//
//  InicioController.swift
//  pyc_cards_ios
//
//  Created by deisy melo on 2/02/16.
//  Copyright © 2016 PinBit. All rights reserved.
//

import Foundation

var resgistroCorrecto:Bool = false

class Inicio: UIViewController{
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    @IBOutlet weak var lblLoader: UILabel!
     var timer = Timer()
    
    override func viewDidLoad() {//antes de cargar el view
        super.viewDidLoad()
        
        self.loader.startAnimating()
        
        let imageBackground = SetBackground(view)
        view.addSubview(imageBackground)
        self.view.sendSubview(toBack: imageBackground)
        
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UserDefaults.standard.set("", forKey: "codigoRef")
        print("device_id: \(UserDefaults.standard.object( forKey: "device_id"))\n")
        print("primera vez: \(UserDefaults.standard.object( forKey: "primera_vez") )\n")
        InternetConnection.isConnectedToNetwork()
        
        
        
        self.loader.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(Inicio.quitarLoader(_:)), name:NSNotification.Name(rawValue: "sinGCM"), object: nil)
        
        lblLoader.text = NSLocalizedString("cargando_datos_inicio", comment: "")
        
        if(UserDefaults.standard.object(forKey: "device_id") != nil){
            if(UserDefaults.standard.object( forKey: "primera_vez") == nil){
               print("Inicio: Registro....")
               self.performSegue(withIdentifier: "registro", sender: nil)
            }
            else{
                print("Inicio : Loggin....")
                self.performSegue(withIdentifier: "loggin", sender: nil)
                self.loader.isHidden = true;
            }
        }
    }
    
    @objc func quitarLoader(_ sender: NSNotification){
        self.loader.isHidden = true;
         lblLoader.text = NSLocalizedString("no_internet", comment: "")
    }
    
}
