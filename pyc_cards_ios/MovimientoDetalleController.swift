//
//  MovimientoDetalleController.swift
//  PYC_mobile
//
//  Created by Sebastian Vivas on 8/04/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class MovimientoDetalleController: UIViewController {
    
    @IBOutlet weak var mapa: GMSMapView!
    var latitud: Double!
    var longitud: Double!
    var latitud_cobro: Double!
    var longitud_cobro: Double!
    var tipo: String!
    var direccion: String!
    var pin:GMSMarker!
    var pin2:GMSMarker!
    
    required override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.mapa.layer.cornerRadius = 5
        self.mapa.layer.shadowOpacity = 0.8
        self.mapa.layer.shadowOffset = CGSize(width: 0.0, height: 0.0 )
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showInView(aView: UIView!, latitud: Double!, longitud: Double!, latitud_cobro: Double!, longitud_cobro: Double!, tipo: String ,animated: Bool)
    {
        self.latitud = latitud
        self.longitud = longitud
        self.latitud_cobro = latitud_cobro
        self.longitud_cobro = longitud_cobro
        self.tipo = tipo
        aView.addSubview(self.view)
        self.view.frame = aView.frame
        //ubica el mapa en el lugar en el que se realiza el pago
        self.reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2DMake(latitud, longitud))
        mapa.camera = GMSCameraPosition.camera(withLatitude: latitud,
                                                           longitude:longitud, zoom:17, bearing:30, viewingAngle: 40)
        
        //crea un pin en la ubicación del usuario....
        pin = GMSMarker(position: CLLocationCoordinate2DMake(latitud, longitud))
        pin.icon = UIImage(named: "icon_location")
        pin.map = mapa
        pin.infoWindowAnchor =  CGPoint(x: 0.3, y: 0.3)
        pin.title = NSLocalizedString("ubicacion_usuario", comment: "")
        
        if animated
        {
            self.showAnimate()
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        mapa.layoutIfNeeded()
        let size = self.mapa.frame.size
        
        let button   = UIButton(type: UIButtonType.system)
        
        button.frame = CGRect(x: (UIScreen.main.bounds.width-80)/2, y: size.height+85, width: 80,height: 30)
        button.addTarget(self, action: #selector(MovimientoDetalleController.btnTouched(_:)), for: UIControlEvents.touchUpInside)
        button.setTitle(NSLocalizedString("cerrar", comment: ""), for: UIControlState.normal)
        button.setBackgroundImage(UIImage(named:"pattern1.png"), for: UIControlState.normal)
        button.setTitleColor(UIColor.white, for: .normal)
        self.view.addSubview(button)
        
    }
    
    func btnTouched(_ sender:UIButton!){
        removeAnimate()
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    //Dirección de la locación
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
        
        // 1
        let geocoder = GMSGeocoder()
        
        // 2
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                
                let lines = address.lines! as [String]
                
                self.direccion = lines.joined(separator: "\n")
                let dir = NSLocalizedString("direccion", comment: "")
                
                self.pin.snippet = "\(dir) \(self.direccion)"
                self.mapa.selectedMarker = self.pin
                
                UIView.animate(withDuration: 0.25) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
}
