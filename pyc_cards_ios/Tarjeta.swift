//
//  Tarjeta.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 25/06/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//


import Foundation


class Tarjeta: NSObject, NSCoding{
    public func encode(with aCoder: NSCoder) {
        //code
    }

    
    var numero: String!
    var id: String!
    var alias: String!
    var tipo: String!
    
    init(num: String, ident: String, alias: String, tipo: String){
        
        self.numero = num
        self.id = ident
        self.alias = alias
        self.tipo = tipo
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encode(numero, forKey:"numero")
        aCoder.encode(id, forKey:"id")
        aCoder.encode(alias, forKey:"alias")
         aCoder.encode(alias, forKey:"tipo")
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init()
        numero = aDecoder.decodeObject(forKey: "numero") as! String
        id = aDecoder.decodeObject(forKey: "id") as! String
        alias = aDecoder.decodeObject(forKey: "alias") as! String
        tipo = aDecoder.decodeObject(forKey: "tipo") as! String
    }
    
    
}
