//
//  PagosController.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 10/06/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit
import Alamofire

class PagosController: UIViewController, UIActionSheetDelegate, CLLocationManagerDelegate {

    var dialogoPagar: DialogoConsultarPinController!
    var dialogoCambiarTarjeta: DialogoCambiarTarjetasController!
    var tarjetasDatos: Bool!
    var dialogoInfo: DialogoSimpleController!
    var dialogoCerrarSesion: DialogoCerrarSesionController!
    
    @IBOutlet weak var pin1: UIButton!
    @IBOutlet weak var pin2: UIButton!
    @IBOutlet weak var pin3: UIButton!
    @IBOutlet weak var pin4: UIButton!
    @IBOutlet weak var pin5: UIButton!
    @IBOutlet weak var pin6: UIButton!
    //Transicion
    
    let manejadorTransiciones = ManejadorTransiciones()

    //Vinculos interfaz
    @IBOutlet weak var lblInput: UILabel!
    @IBOutlet weak var btnPagarQR: UIButton!
    
    @IBOutlet weak var btnBorrar: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var viewHeader: HeaderController!
    
    //Gestos swipe
    var swipeLeft: UISwipeGestureRecognizer!
    var swipeRight: UISwipeGestureRecognizer!
    
    //Mapas
    var locationManager = CLLocationManager()
    var didFindMyLocation = false
    var pin = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("\n pagos")
        
        let imageBackground = SetBackground(view)
        view.addSubview(imageBackground)
        self.view.sendSubview(toBack: imageBackground)
        


        
        self.tabBarController?.delegate = manejadorTransiciones
        
        tarjetasDatos = UserDefaults.standard.bool(forKey: "sinTarjeta")
        
        NotificationCenter.default.addObserver(self, selector: #selector(PagosController.sinTarjetas(_:)), name:NSNotification.Name(rawValue: "sinTarjetas"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PagosController.nuevaTarjeta(_:)), name: NSNotification.Name(rawValue: "conTarjetas"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PagosController.showToast(notification:)), name: NSNotification.Name(rawValue:"pagoPin"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PagosController.falloPago(notification:)), name: NSNotification.Name(rawValue:"falloPago"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PagosController.cerrarApp(notification:)), name: NSNotification.Name(rawValue:"cerrarSesion"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PagosController.abrirPerfil(notification:)), name: NSNotification.Name(rawValue:"verPerfil"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PagosController.cambieTarjeta(notification:)), name: NSNotification.Name(rawValue: "cambieTarjeta"), object: nil)
        
        self.llenarTextos()
        
        self.getPoints();
        
        //Obtener ubicación....
        locationManager.delegate = self
        if((UIDevice.current.systemVersion as NSString).floatValue >= 8)
        {
            print("ios >8", terminator: "")
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.startUpdatingLocation()
    }
    
    
    //Funcion que maneja la ubicación del dispositivo
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("locations")
        UserDefaults.standard.set(locationManager.location!.coordinate.latitude, forKey: "latitud")
        UserDefaults.standard.set(locationManager.location!.coordinate.longitude, forKey: "longitud")
        print("latitud: \(locationManager.location!.coordinate.latitude)")
        locationManager.stopUpdatingLocation()
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        //Pone el fondo de la vista con el degrade azul.....
        //CAGradientLayer().fondoDegrade(self.view, color1: CAGradientLayer().UIColorFromHex(0xfafafa, alpha: 1), color2: CAGradientLayer().UIColorFromHex(0xf0f4f5, alpha: 1))
        self.view.layoutSubviews()
    }
    

    
    override func viewDidAppear(_ animated: Bool) {
        
        pin1.layer.borderWidth = 2
        pin1.layer.borderColor = UIColor.white.cgColor
        pin1.layer.cornerRadius = pin1.layer.bounds.width/2
        
        pin2.layer.borderWidth = 2
        pin2.layer.borderColor = UIColor.white.cgColor
        pin2.layer.cornerRadius = pin1.layer.bounds.width/2
        
        pin3.layer.borderWidth = 2
        pin3.layer.borderColor = UIColor.white.cgColor
        pin3.layer.cornerRadius = pin1.layer.bounds.width/2
        
        pin4.layer.borderWidth = 2
        pin4.layer.borderColor = UIColor.white.cgColor
        pin4.layer.cornerRadius = pin1.layer.bounds.width/2
        
        pin5.layer.borderWidth = 2
        pin5.layer.borderColor = UIColor.white.cgColor
        pin5.layer.cornerRadius = pin1.layer.bounds.width/2
        
        pin6.layer.borderWidth = 2
        pin6.layer.borderColor = UIColor.white.cgColor
        pin6.layer.cornerRadius = pin1.layer.bounds.width/2
        
    }
    
    
    //Metodos que escuchan notificaciones de cambios en las tarjetas
    @objc func sinTarjetas(_ sender: NSNotification){
        
        tarjetasDatos = UserDefaults.standard.bool(forKey: "sinTarjeta")
    }
    
    
    @objc func nuevaTarjeta(_ sender: NSNotification){
        tarjetasDatos = UserDefaults.standard.bool(forKey: "sinTarjeta")
    }
    
    //Boton de pagar por QR
    @IBAction func btnPagarQR(_ sender: AnyObject) {
        print("entra a btnPAgarQR")
        if(InternetConnection.isConnectedToNetwork()){//verifica si el dispositivo tiene conexion a internet
            performSegue(withIdentifier: "scanQR", sender: nil)
        }
        else{
            self.view.makeToast(NSLocalizedString("no_internet", comment: ""))
        }
    }
    
   
    @IBAction func IngresarPin(_ sender: UIButton) {
        print("Ingresando pin \((sender.titleLabel?.text)!)")
        
        
        if((sender.titleLabel?.text)! != "<" &&  pin.characters.count < 6)
        {
            var number = (sender.titleLabel?.text)!
            self.pin = self.pin + number
            print("Pin ingresado: \(self.pin)")
            
            switch self.pin.characters.count {
            case 1:
                pin1.setTitle(number, for: .normal)
                pin1.layer.borderWidth = 0
            case 2:
                pin2.setTitle(number, for: .normal)
                pin2.layer.borderWidth = 0
            case 3:
                pin3.setTitle(number, for: .normal)
                pin3.layer.borderWidth = 0
            case 4:
                pin4.setTitle(number, for: .normal)
                pin4.layer.borderWidth = 0
            case 5:
                pin5.setTitle(number, for: .normal)
                pin5.layer.borderWidth = 0
            case 6:
                pin6.setTitle(number, for: .normal)
                pin6.layer.borderWidth = 0
            default: break
                
            }
        }
        
        if((sender.titleLabel?.text)! == "<"){
            if(self.pin.characters.count > 1){
                self.pin.remove(at: pin.index(before: pin.endIndex))
            }
            else{ self.pin = "" }
            
            switch self.pin.characters.count {
            case 0:
                pin1.setTitle("", for: .normal)
                pin1.layer.borderWidth = 2
            case 1:
                pin2.setTitle("", for: .normal)
                 pin2.layer.borderWidth = 2
            case 2:
                pin3.setTitle("", for: .normal)
                 pin3.layer.borderWidth = 2
            case 3:
                pin4.setTitle("", for: .normal)
                pin4.layer.borderWidth = 2
            case 4:
                pin5.setTitle("", for: .normal)
                    pin5.layer.borderWidth = 2
            case 5:
                pin6.setTitle("", for: .normal)
                pin6.layer.borderWidth = 2
            default: break
            }
        }
        
        if(pin.characters.count == 6){
            consultarPin(pin: self.pin)
            pin1.setTitle("", for: .normal)
            pin1.layer.borderWidth = 2
            pin2.setTitle("", for: .normal)
            pin2.layer.borderWidth = 2
            pin3.setTitle("", for: .normal)
            pin3.layer.borderWidth = 2
            pin4.setTitle("", for: .normal)
            pin4.layer.borderWidth = 2
            pin5.setTitle("", for: .normal)
            pin5.layer.borderWidth = 2
            pin6.setTitle("", for: .normal)
            pin6.layer.borderWidth = 2
            self.pin = ""
        }
    }
    
    func consultarPin(pin: String){
        print("Consultar Pin: \(pin) \n")
        self.view.endEditing(true)
        
        if(InternetConnection.isConnectedToNetwork()){//verifica si el dispositivo tiene conexion a internet
            dialogoPagar = DialogoConsultarPinController(nibName: "DialogoConsultarPin", bundle: nil)
            dialogoPagar.showInView(aView: self.view, codigo: pin, animated: true, controller:  self)
            
        }
        else{
            self.view.makeToast(NSLocalizedString("no_internet", comment: ""))
        }

    }
    
    //Inicializar textos
    func llenarTextos(){
        
        lblInput.text = NSLocalizedString("ingresaCaptura", comment: "")
        
        
        //Boton de chat
        let halfSizeOfView = 20.0
        let insetSize = self.view.bounds.insetBy(dx: CGFloat(Int(2 * halfSizeOfView)), dy: CGFloat(Int(2 * halfSizeOfView))).size
        
        let pointX = CGFloat((30))
        let pointY = CGFloat((50))
        
        var chatView: ChatView!
        chatView = ChatView(frame: CGRect(x: pointX, y: pointY, width: 70, height: 70))
        chatView.view = self
        chatView.chat = "chatPagos"
        self.view.addSubview(chatView)
    }
    
    //Manejador del gesto de swipe
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.left:
                self.tabBarController?.selectedIndex = 2
            case UISwipeGestureRecognizerDirection.right:
                self.tabBarController?.selectedIndex = 0
            default:
                break
            }
        }
    }
    
    //Muestra toast con pago de pin
    @objc func showToast(notification: NSNotification){
        dialogoInfo = DialogoSimpleController(nibName: "DialogoSimple", bundle: nil)
        dialogoInfo.showInView(aView: self.view, titulo: NSLocalizedString("titulo_confirmacion_pago", comment: ""), cuerpo: NSLocalizedString("pago_exitoso", comment: ""), animated: true)
    }
    
    
    
    @objc func falloPago(notification: NSNotification){
        dialogoInfo = DialogoSimpleController(nibName: "DialogoSimple", bundle: nil)
        dialogoInfo.showInView(aView: self.view, titulo: NSLocalizedString("titulo_confirmacion_pago", comment: ""), cuerpo: NSLocalizedString("fallo_pago", comment: ""), animated: true)
    }
    
    
    @IBAction func btnBorrar(_ sender: AnyObject) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "teclaBorrarPin"), object: nil)
    }
    
    
    @IBAction func menu(_ sender: AnyObject) {
       
        let alertController = UIAlertController(
            title: nil,
            message: nil,
            preferredStyle: UIAlertControllerStyle.actionSheet)
        
        
        let movimientosAction = UIAlertAction(
        title: NSLocalizedString("movimientos", comment: ""), style: UIAlertActionStyle.default){
            (action) -> Void in
            print("Historial")
            self.performSegue(withIdentifier: "movimientos", sender: nil)
        }
        
        let perfilAction = UIAlertAction(
        title: NSLocalizedString("perfil", comment: ""), style: UIAlertActionStyle.default){
            (action) -> Void in
            print("Perfil")
            self.performSegue(withIdentifier: "perfil", sender: nil)
        }
        
        
        let cerrarAction = UIAlertAction(
            title: NSLocalizedString("cerrar_sesion", comment: ""), style: UIAlertActionStyle.destructive){
                (action) -> Void in
                print("cerrar sesion.... \n")
                self.dialogoCerrarSesion = DialogoCerrarSesionController(nibName: "DialogoCerrarSesion", bundle: nil)
                self.dialogoCerrarSesion.showInView(aView: self.view, animated: true)
        }
        
        let cancelarAction = UIAlertAction(
            title: NSLocalizedString("cancelar", comment: ""), style: UIAlertActionStyle.default){
                (action) -> Void in
                print("Cancelar")
                alertController.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(cancelarAction)
        alertController.addAction(perfilAction)
        alertController.addAction(movimientosAction)
        alertController.addAction(cerrarAction)
        
    
        let popOver = alertController.popoverPresentationController
        popOver?.sourceView  = sender as? UIView
        popOver?.sourceRect = (sender as! UIView).bounds
        popOver?.permittedArrowDirections = UIPopoverArrowDirection.any
        
        self.present(alertController, animated: true, completion: nil)
       
    }
  
    //recibe la notificación del cierre de sesión en el servidor y cierra la aplicación
    @objc func cerrarApp(notification: NSNotification){
        self.dismiss(animated: true, completion: nil)
        
        let secondPresentingVC = self.presentingViewController?.presentingViewController
        secondPresentingVC?.dismiss(animated: true, completion: {})
    }
    
    //recibe la notificación del cierre de sesión en el servidor y cierra la aplicación
    @objc func abrirPerfil(notification: NSNotification){
        print("abrir perfil \n")
        self.performSegue(withIdentifier: "perfil", sender: nil)
    }
    

    //obtener puntos de referidos
    func getPoints(){
        print("OBTENER PUNTOS \n")
        let url = urlPinbit + "/wallets/check_points.json"
        print("\(url)\n")
        
        request(url, method: .get).responseJSON {
            response in
            print("getPoints status code: \(response.response!.statusCode) \n")
            switch response.result {
            case .success(let data):
                if(response.response!.statusCode == 200)
                {
                    var json: JSON = JSON(data)
                    print("\n Puntos: \(json)")
                    UserDefaults.standard.setValue(json["points"].stringValue, forKey: "points")
                }
                else
                {
                    UserDefaults.standard.setValue("0.0", forKey: "points")
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @objc func cambieTarjeta(notification: NSNotification){
        self.view.makeToast(NSLocalizedString("tarjeta_seleccionada", comment: ""))
    }
    
}
