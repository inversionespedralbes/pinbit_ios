//
//  ViewController.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 22/05/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit
import Alamofire
import CoreData



class Login: UIViewController, UITabBarControllerDelegate, UITextFieldDelegate {
    
    //Variables interfaz
    @IBOutlet weak var lblContrasena: UITextField!
    @IBOutlet weak var txtSubtitulo: UILabel!
    
   
    
    //Variables locales
    let crearCuenta = UITapGestureRecognizer()
    var terminosCondiciones: TerminosController!
    var dialogo: DialogoTarjetasController!
    var tarjetas: [Tarjeta] = []
    var dialogoInicioSesion: DialogoCargaController!
    var dialogoInternet: FallaConexionController!
    var chatView: ChatView!
    var contadorIntentos: Int!
    var dialogoInfo: DialogoSimpleController!
    var passwordLoggin:String = ""
    var otherApp:Bool = false
    
    var pin = ""
    var payment = ""
    var appName = ""
    
    
    
    override func viewDidLoad() {//antes de cargar el view
        super.viewDidLoad()
        
       
        let imageBackground = SetBackground(view)
        view.addSubview(imageBackground)
        self.view.sendSubview(toBack: imageBackground)
        
        self.llenarTextos()
        tarjetas = []
        
        lblContrasena.layer.borderColor = UIColor.white.cgColor
        lblContrasena.layer.cornerRadius = 5
        lblContrasena.layer.borderWidth = 1
        
        lblContrasena.delegate = self
        
              

    }
   
    @IBAction func borrarTexto(_ sender: AnyObject) {
        print("btn borrar \n")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "teclaBorrar"), object: nil)
    }
    
//    override var shouldAutorotate : Bool {
//        return false
//    }
    
    
    @IBAction func btnIniciarSesion(_ sender: Any) {
        
        dialogoInicioSesion = DialogoCargaController(nibName: "DialogoCarga", bundle: nil)
        dialogoInicioSesion.showInView(aView: self.view, titulo: NSLocalizedString("iniciando_sesion", comment: ""), animated: true)
        
        passwordLoggin = lblContrasena.text!
        
        //verifica que los datos sean de otra app
        let data = UserDefaults.standard.object(forKey: "dataApp") as! String
        print("data: \(data)")
        
        
                 //Internet connection
        if(passwordLoggin == ""  || passwordLoggin.characters.count < 4)
        {
            self.view.makeToast("ingrese una contraseña valida")
            dialogoInicioSesion.removeAnimate()
        }
        else{
            if(data != nil && data != ""){
                UserDefaults.standard.set("", forKey: "dataApp")
                
                self.otherApp = true
                var info = data.components(separatedBy: "-")
                self.pin = info[0]
                self.payment = info[1]
                
                print("pin: \(pin)\n")
                print("payment: \(payment)\n")
                print("appName: \(appName)\n")
                
                iniciarSesion()
                
            }else{
                iniciarSesion()
            }

           
        }

    }
    override func viewDidLayoutSubviews() {
        //Pone el fondo de la vista con el degrade azul.....
        //CAGradientLayer().fondoDegrade(self.view, color1: CAGradientLayer().UIColorFromHex(0x0092f1, alpha: 1), color2: CAGradientLayer().UIColorFromHex(0x005eab, alpha: 1))
        self.view.layoutSubviews()
    }
 

    
    
    //Primera conexión para inicio de sesión
    func iniciarSesion() {
        if(InternetConnection.isConnectedToNetwork()){
            print("Intentando iniciar sesion \n")
            let deviceID = UserDefaults.standard.object(forKey: "device_id") as! String
            let url = urlPinbit + "/iniciar_sesion.json"
            print("\(url) \n")
            let parameters = [
                "sesion": [
                    "password": passwordLoggin,
                    "device_id":"\(deviceID)"
                ]] as [String : Any]
            
            //print("parametros inicio sesion: \n \(parameters)")
            
            request(url,method: .post, parameters: parameters).responseJSON {
                response  in
                print("respuesta login: \(response) status code: \(response.response?.statusCode)")
                switch response.result {
                case .success(let data):
                    if(response.response?.statusCode == 201)
                    {
                        let json = JSON(data)
                        print(json)
                        UserDefaults.standard.set(json["token"].stringValue, forKey: "token")
                        UserDefaults.standard.set(true, forKey: "enSesion")
                        self.listaTarjetas()
                    }
                    else{
                        print("No inicio sesion....\n")
                    }
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    self.dialogoInicioSesion.removeAnimate()
                    self.view.makeToast(NSLocalizedString("error_inicio_sesion", comment: ""))
                    
                    //Error de respuesta servidor
                }
            }

        }
        else{
            self.view.makeToast(NSLocalizedString("no_internet", comment: ""))
            dialogoInicioSesion.removeAnimate()
        }
        
    }
    

    
    //Lista las tarjetas de un usuario
    func listaTarjetas(){
        print("\n Listar tarjetas \n")
        let url = urlPinbit + "/tarjetas.json"
        print(url)
        tarjetas.removeAll(keepingCapacity: false)
        
        // llama el objeto AppDelegate para poder utilizar las funciones que allí se encuentran
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        //se obtiene el contexto: Es la interfaz para acceder a la base de datos...
        let context = appDel.managedObjectContext

        
        self.tarjetas.removeAll(keepingCapacity: false)
        
        //traer las tarjetas de la bd...
        let consulta = NSFetchRequest<NSFetchRequestResult>(entityName: "Tarjeta")
        consulta.returnsObjectsAsFaults = false
        
        //recorrer el resultado de la consulta
        let res = try? context.fetch(consulta)
        
        
        if res!.isEmpty {
             print("BUSCANDO TARJETAS ALMACENADAS SERVIDOR....\n")
            
            request(url).responseJSON {
                response in
                print("respuesta login: \(response.response?.statusCode)")
                switch response.result {
                case .success(let data):
                    let json = JSON(data)
                    self.procesarInfoTarjetas(json: json)
                case .failure(let error):
                    print(error)
                }
            }

        }
        else{
            print("BUSCANDO TARJETAS ALMACENADAS EN BD....\n")
            
            var dic = [[String:String]]()
            var jsonTarjetas:JSON
           //recorriendo la consulta
          

            for tarjetaBD:AnyObject in res as! [NSManagedObject]  {
                
                let jsonTar = [
                    "numero_enmascarado" : (tarjetaBD.value(forKey: "mascara")! as? String)!,
                    "id" : (tarjetaBD.value(forKey: "id")! as? String)!,
                    "alias" : (tarjetaBD.value(forKey: "alias")! as? String)!
                    ] as [String : Any]
            
                
                dic.append(jsonTar as! [String : String])
            }
            
           jsonTarjetas = JSON(dic)
           self.procesarInfoTarjetas(json: jsonTarjetas)
        }
        
    }
    
    
    func procesarInfoTarjetas(json: JSON){
        let lista = json.arrayValue
        print("Lista de tarjetas Loggin: \(lista)")
        
         let id_ultimaTarjeta = UserDefaults.standard.object(forKey: "ultimaTarjeta") as? String
        
        print("tarjeta guardada: \(id_ultimaTarjeta)")
        
        if(id_ultimaTarjeta != nil){
            print("ultima tarjeta seleccionada...")
            
            // llama el objeto AppDelegate para poder utilizar las funciones que allí se encuentran
            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            
            //se obtiene el contexto: Es la interfaz para acceder a la base de datos...
            let context = appDel.managedObjectContext

            //consultar un dato especifico...
            let dato = NSFetchRequest<NSFetchRequestResult>(entityName: "Tarjeta")
            dato.predicate = NSPredicate(format: "id = %@", id_ultimaTarjeta!)
            
            
            //recorrer el resultado de la consulta....
            let resultado2 = try? context.fetch(dato)
            
            var mascara:String!
            var alias:String!
            var tipo:String!
            
            for res2 in resultado2 as! [NSManagedObject]  {
                mascara = res2.value(forKey: "mascara") as! String!
                alias = res2.value(forKey:"alias") as! String!
                tipo = res2.value(forKey: "tipo")as! String
            }
            
            for res2 in resultado2 as! [NSManagedObject] {
                mascara = res2.value(forKey: "mascara")  as! String!
                alias = res2.value(forKey: "mascara")  as! String!
                tipo = res2.value(forKey: "tipo")as! String
            }
            
            
//            for res2:AnyObject in resultado2 as AnyObject {
//                mascara = (res2.valueForKey("mascara")! as? String)!
//                alias = (res2.valueForKey( "alias")! as? String)!
//            }
            
            UserDefaults.standard.setValue(mascara,forKey: "numero_enmascarado")
            self.tarjetas.append(Tarjeta(num: mascara, ident: id_ultimaTarjeta!, alias: alias, tipo: tipo))
            let guardarTarjetas = NSKeyedArchiver.archivedData(withRootObject: self.tarjetas)
            UserDefaults.standard.setValue(guardarTarjetas, forKey: "tarjetas")
            self.postTarjeta(idTarjeta: id_ultimaTarjeta!)
            UserDefaults.standard.setValue(false, forKey: "sinTarjeta")

        }
        else{
            //Si no hay una tarjeta recientemente seleccionada
            if(lista.count == 0)
            {
                if(!otherApp){
                    print("No hay tarjetas \n")
                    self.dialogoInicioSesion.removeAnimate()
                    UserDefaults.standard.setValue(true, forKey: "sinTarjeta")
                    //self.performSegue(withIdentifier: "LoginPerfil", sender: nil)
                    self.performSegue(withIdentifier: "pagos", sender: nil)
                }
                else{
                    self.otherAppFail()
                }
                
                
            }
            else if(otherApp){//selecciona una tarjeta //InApp
                UserDefaults.standard.setValue(lista[0]["numero_enmascarado"].stringValue,forKey: "numero_enmascarado")
                self.tarjetas.append(Tarjeta(num: lista[0]["numero_enmascarado"].stringValue, ident: lista[0]["id"].stringValue, alias: lista[0]["alias"].stringValue, tipo: lista[0]["pon"].stringValue))
                let guardarTarjetas = NSKeyedArchiver.archivedData(withRootObject: self.tarjetas)
                UserDefaults.standard.setValue(guardarTarjetas, forKey: "tarjetas")
                self.postTarjeta(idTarjeta: lista[0]["id"].stringValue)
                UserDefaults.standard.setValue(false, forKey: "sinTarjeta")
                
            }else if(lista.count == 1)
            {
                //Una sola tarjeta
                UserDefaults.standard.setValue(lista[0]["numero_enmascarado"].stringValue,forKey: "numero_enmascarado")
                self.tarjetas.append(Tarjeta(num: lista[0]["numero_enmascarado"].stringValue, ident: lista[0]["id"].stringValue, alias: lista[0]["alias"].stringValue, tipo: lista[0]["pon"].stringValue))
                let guardarTarjetas = NSKeyedArchiver.archivedData(withRootObject: self.tarjetas)
                UserDefaults.standard.setValue(guardarTarjetas, forKey: "tarjetas")
                self.postTarjeta(idTarjeta: lista[0]["id"].stringValue)
                UserDefaults.standard.setValue(false, forKey: "sinTarjeta")
            }
            else
            {
                //Multiples tarjetas
                for aux in lista
                {
                    self.tarjetas.append(Tarjeta(num: aux["numero_enmascarado"].stringValue, ident: aux["id"].stringValue, alias: aux["alias"].stringValue, tipo: aux["pon"].stringValue))
                }
                let guardarTarjetas = NSKeyedArchiver.archivedData(withRootObject: self.tarjetas)
                UserDefaults.standard.setValue(guardarTarjetas, forKey: "tarjetas")
                self.dialogo = DialogoTarjetasController(nibName: "DialogoTarjetasController",bundle: nil)
                self.dialogo.showInView(aView: self.view, animated: true, tarjetas: self.tarjetas, dialogo: self.dialogoInicioSesion)
            }
        }        
    }
    
    
    //Selecciona una tarjeta para usar
    func postTarjeta(idTarjeta: String){
        print("\n Seleccionar Tarjetas Inicio sesion ")
        let url = urlPinbit + "/seleccionar_tarjetas.json"
        print(url)
        let token = UserDefaults.standard.object(forKey: "token") as! String
        let parameters = [
            "tarjeta": [
                "id":idTarjeta
        ], "authenticity_token":token ] as [String : Any]
        
        request(url,method: .post, parameters: parameters as? [String : AnyObject])
            .responseJSON { response in
                print("respuesta: \(response.description) status code: \(response.response!.statusCode) \n")
                
                switch response.result{
                case .success:
                    if(response.response!.statusCode == 200)
                    {
                        //se almacena la ultima tarjeta seleccionada
                        UserDefaults.standard.setValue(idTarjeta, forKey: "ultimaTarjeta")
                        self.dialogoInicioSesion.removeAnimate()
                        if(self.otherApp){
                            self.pay(tarjeta: idTarjeta)
                        }else{
                            self.dialogoInicioSesion.removeAnimate()
                            self.performSegue(withIdentifier: "pagos", sender: nil)
                        }
                    }
                    else
                    {
                        //Error de respuesta servidor
                        print("error de respuesta de servidor")
                    }
                case .failure(let error):
                    print("Error: \(error)\n")
                    if(self.otherApp){
                        self.otherAppFail()
                    }
                }
        }        
    }
    
    //Inicia el registro de una nueva cuenta
    func nuevaCuenta(){
        NotificationCenter.default.addObserver(self, selector: Selector("mostrarDialogoScan:"), name: NSNotification.Name(rawValue: "cerrarTerminos"), object: nil)
        
        self.terminosCondiciones = TerminosController(nibName: "TerminosCondiciones", bundle: nil)
        self.terminosCondiciones.showInView(aView: self.view, animated: true)
    }
    
    //Inicializar textos
    func llenarTextos(){
        self.txtSubtitulo.text = NSLocalizedString("contrasena", comment: "")
        
        //Boton de chat
        let halfSizeOfView = 20.0
        let insetSize = self.view.bounds.insetBy(dx: CGFloat(Int(2 * halfSizeOfView)), dy: CGFloat(Int(2 * halfSizeOfView))).size
        
        let pointX = CGFloat((30))
        let pointY = CGFloat((50))
        
        chatView = ChatView(frame: CGRect(x: pointX, y: pointY, width: 70, height: 70))
        chatView.view = self
        chatView.chat = "chatLogin"
        self.view.addSubview(chatView)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func otherAppFail(){
        let urlPinBitSdk = "sdkpinbit://#fail"
        
        print("url: \(urlPinBitSdk)\n")
        
        
        let secondPresentingVC = self.presentingViewController?.presentingViewController
        secondPresentingVC?.dismiss(animated: true, completion: {})
        self.lblContrasena.text = ""
        UserDefaults.standard.set(false, forKey: "enSesion")

        self.dialogoInicioSesion.removeAnimate()
        
        let customURL = NSURL(string: urlPinBitSdk)!
        
        if UIApplication.shared.canOpenURL(customURL as URL) {
            print("Intenta abrir el app")
            UIApplication.shared.openURL(customURL as URL)
        }
        else{
            print("No funciona")
        }

    }
    
    func otherAppOk(){
        let urlPinBitSdk = "sdkpinbit://#ok"
        
        print("url: \(urlPinBitSdk)\n")
        
        
        let secondPresentingVC = self.presentingViewController?.presentingViewController
        secondPresentingVC?.dismiss(animated: true, completion: {})
        self.lblContrasena.text = ""
        UserDefaults.standard.set(false, forKey: "enSesion")
        
        self.dialogoInicioSesion.removeAnimate()
        
        let customURL = NSURL(string: urlPinBitSdk)!
        
        if UIApplication.shared.canOpenURL(customURL as URL) {
            print("Intenta abrir el app")
            UIApplication.shared.openURL(customURL as URL)
        }
        else{
            print("No funciona")
        }

    }
    
    func pay(tarjeta: String){
        self.otherApp = false
        print("\n POST PAGAR PIN \n")
        
        let token = UserDefaults.standard.object(forKey: "token") as! String
        let deviceID = UserDefaults.standard.object(forKey: "device_id") as! String
        
        
        let parameters = [
            "sistema_operativo": "iOS",
            "latitud":"0",
            "longitud":"0",
            "cantidad_cuotas": self.payment,
            "authenticity_token": token,
            "pin":"\(self.pin)",
            "device_id":"\(deviceID)"] as [String : Any]
        let linkPago = "/pagos.json"
        let url = urlPinbit + linkPago
        
        print("\n \(url)")
        print("params: \(parameters)")
        
        request(url,method: .post, parameters: parameters).responseJSON {
            response in
            print("Respuesta del Pago: \(response.response!.statusCode)")
            
            switch response.result {
            case .success(let data):
                print("Datos del pago: \n \(data)")
                if(response.response!.statusCode == 201){
                    
                    let json = JSON(data)
                    print("JSON pagar pin: \(json)")
                    
                    if(linkPago.contains("/wallets/pay")){
                        let points = json["points"].stringValue
                        UserDefaults.standard.setValue(points, forKey: "points")
                    }
                    else{
                        self.savePay(json: json, tarjeta: tarjeta)
                        self.otherAppOk()
                    }
                    
                   
                }
                else{
                    //Error de respuesta servidor
                    if(response.response!.statusCode == 422){
                        print("Falló el pago....")
                        
                    }
                    else{
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "falloPago"), object: nil)
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            case .failure(let error):
                print("Fallo pago: \(error) \n")
            
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "falloPago"), object: nil)
                self.dismiss(animated: true, completion: nil)
            }
            
        }
        
    }
    
    
    
    func savePay(json:JSON, tarjeta:String) {
         var appDel = UIApplication.shared.delegate as! AppDelegate
        //se obtiene el contexto: Es la interfaz para acceder a la base de datos...
        var context = appDel.managedObjectContext
        
        let id_tarjeta = tarjeta
        
        
        //traer los pagos de la bd...
        let consulta = NSFetchRequest<NSFetchRequestResult>(entityName: "Tarjeta")
        
        let predicate = NSPredicate(format: "id == %@", id_tarjeta)
        
        // Set the predicate on the fetch request
        consulta.predicate = predicate
        
        //recorrer el resultado de la consulta
        let res1 = try? context.fetch(consulta)
        var alias = ""
        var mascara = ""
        
        //recorriendo la consulta
        for info:AnyObject in res1 as! [NSManagedObject]  {
            alias = (info.value(forKey: "alias") as? String!)!
            mascara = (info.value(forKey:  "mascara") as? String!)!
        }

        
        let movimientoNuevo: AnyObject = NSEntityDescription.insertNewObject(forEntityName: "Movimiento", into: context)
        
        var date = json["fecha"].stringValue
        
        movimientoNuevo.setValue(json["establecimiento"].stringValue, forKey: "establecimiento")
        movimientoNuevo.setValue(self.formatDate(fecha: date), forKey: "fecha_pago")
        //movimientoNuevo.setValue(formatDate(date), forKey: "fecha_pago")
        movimientoNuevo.setValue(json["latitud_cobro"].stringValue, forKey: "latitud_cobro")
        movimientoNuevo.setValue(json["longitud_cobro"].stringValue, forKey: "longitud_cobro")
        movimientoNuevo.setValue(json["latitud_pago"].stringValue, forKey: "latitud_pago")
        movimientoNuevo.setValue(json["longitud_pago"].stringValue, forKey: "longitud_pago")
        movimientoNuevo.setValue(json["valor"].stringValue, forKey: "valor")
        
        print("moviento_valor: \(json["valor"].stringValue)\n")
        //datos de la tarjeta
        
        movimientoNuevo.setValue(tarjeta, forKey: "id_tarjeta")
        movimientoNuevo.setValue(alias, forKey: "alias_tarjeta")
        movimientoNuevo.setValue(mascara, forKey: "mascara_tarjeta")
        //guardar tarjeta en la bd
        try! context.save()
    }
    
    
    //Formato para fechas
    func formatDate(fecha: String) -> String {
        let parte1 = fecha.substring(to: fecha.index((fecha.characters.indices.min())!, offsetBy: 10)) + " "
        //let parte2 = fecha[fecha.startIndex.advancedBy(11)...fecha.startIndex.advancedBy(18)]
        let parte2 = fecha[fecha.index(fecha.startIndex, offsetBy: 11)...fecha.index(fecha.startIndex, offsetBy: 18)]
        return parte1 + parte2
        
    }

    

}

