//
//  InternetConnection.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 27/07/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import Foundation
public class InternetConnection {
    
    class func isConnectedToNetwork()->Bool{
        
        var Status:Bool = false
        let url = NSURL(string: "http://google.com/")
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "HEAD"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
        request.timeoutInterval = 8.0
        
        var response: URLResponse?
        
        _ = (try? NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)) as NSData?
        
        print("comprobacion Internet: \(response?.description) \n")
        
        if let httpResponse = response as? HTTPURLResponse {
            if httpResponse.statusCode == 200 {
                Status = true
            }
        }
        
        print("Status Internet: \(Status) \n")
        
        return Status
    }
    
}
