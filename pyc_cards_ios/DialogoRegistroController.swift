//
//  DialogoRegistroController.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 5/06/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit
import Alamofire

class DialogoRegistroController: UIViewController, UITextFieldDelegate {

    //Interfaz
    @IBOutlet weak var viewDialogo: UIView!
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var txtCodigo: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var btnRegistro: UIButton!
    @IBOutlet weak var btnCancelar: UIButton!
    
    //Variables
    var codigoUsuario: String!
    var token: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0)
        self.viewDialogo.layer.cornerRadius = 5
        self.viewDialogo.layer.shadowOpacity = 0.8
        self.viewDialogo.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        txtCodigo.delegate = self
        self.spinner.isHidden = true
        self.view.isUserInteractionEnabled = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    required override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showInView(aView: UIView!, animated: Bool, codigoUsuario: String!, token: String!)
    {
        
        aView.addSubview(self.view)
        self.view.frame = aView.frame
        self.token = token
        self.codigoUsuario = codigoUsuario
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        txtCodigo.resignFirstResponder()
        return true
    }
    
    @IBAction func btnRegistrar(_ sender: AnyObject) {
        self.putConfirmar()
    }
    
    @IBAction func btnCancelar(_ sender: AnyObject) {
        self.removeAnimate()
    }
    
    func putConfirmar( ) {
        
        self.spinner.isHidden = false
        self.spinner.startAnimating()
        self.view.isUserInteractionEnabled = false
        
        let url = urlPinbit + "/activacion_usuarios/\(self.codigoUsuario).json"
        
        let parameters = ["sistema_operativo":"iOS",
                          "codigo": txtCodigo.text!,
                          "device_id": UIDevice.current.identifierForVendor!.uuidString,
                          "authenticity_token":self.token]

        
        request(url, method: .put,  parameters: parameters).responseJSON {
            response in
            switch response.result {
            case .success(_):
                if(response.response!.statusCode == 201)
                {
                    print("Registro exitoso: \(response)")
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "Loggin")
                    self.present(vc, animated: true, completion: nil)
                    self.view.makeToast(NSLocalizedString("cuenta_confirmada", comment: ""))
                }
                else
                {
                    print(response.result)
                    self.view.makeToast(NSLocalizedString("cuenta_no_confirmada", comment: ""))
                    //Error de respuesta servidor
                }
            case .failure(let error):
                print(error)
            }
        }
        
        self.spinner.stopAnimating()
        self.spinner.isHidden = true
        self.view.isUserInteractionEnabled = true
        
    }
    
    func llenarCampos(){
        txtCodigo.placeholder = NSLocalizedString("codigo_confirmacion", comment: "")
        btnCancelar.setTitle(NSLocalizedString("cancelar", comment: ""), for: UIControlState())
        btnRegistro.setTitle(NSLocalizedString("registrar", comment: ""), for: UIControlState())
        lblTitulo.text = NSLocalizedString("envio_codigo", comment: "")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
