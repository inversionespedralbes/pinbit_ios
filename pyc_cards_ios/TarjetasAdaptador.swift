//
//  TarjetasAdaptador.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 30/06/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class TarjetasAdaptador: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewTarjetas: UIView!
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var tabla: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak var btnAddTarj: UIButton!
    
    
    var dialogoInicioSesion: DialogoCargaController!
    var dialogo: DialogoTarjetasController!
    
    var tarjetas = [Tarjeta]()
    var tarjetaActual: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitulo.text = NSLocalizedString("tus_tarjetas",comment: "")
        
        
//        let plus1 = UIImage(named: "plus.png")
//        let tintedPlus = plus1?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
//        btnAddTarj.setImage(tintedPlus, for: .normal)
//        btnAddTarj.tintColor = UIColor.init(cgColor: CAGradientLayer().UIColorFromHex(rgbValue: 0x00c4ee, alpha: 1.0).cgColor)
        
        
        tabla.delegate = self
        tabla.dataSource = self
        spinner.isHidden = false
        spinner.startAnimating()
        self.listaTarjetas()
        NotificationCenter.default.addObserver(self, selector: #selector(TarjetasAdaptador.refreshTarjetas(sender:)), name: NSNotification.Name(rawValue: "nuevaTarjeta"), object: nil)
        
        //NotificationCenter.default.addObserver(self, selector: #selector(TarjetasAdaptador.refreshTarjetas(_:)), name:"nuevaTarjeta", object: nil)
        // let id_ultimaTarjeta = NSUserDefaults.standardUserDefaults().objectForKey("ultimaTarjeta") as? String
        
        if let aux = UserDefaults.standard.object(forKey: "ultimaTarjeta") as? String{
            self.tarjetaActual = aux
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //Funciones de tabla de tarjetas
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath) as! ListaTarjetasAdapatador
    
        cell.lblNumeroEnmascarado.text = tarjetas[indexPath.row].numero
        
        var imageTarejta = UIImage(named: "credit.png")
        
        switch tarjetas[indexPath.row].tipo {
        case "Amex":
            imageTarejta = UIImage(named: "logoamex.png")
            break
        case "VISA":
            imageTarejta = UIImage(named: "logovisa.jpg")
            break
        case "MasterCard":
            imageTarejta = UIImage(named: "logomastercard.png")
            break
        case "DinersClub":
            imageTarejta = UIImage(named: "logodiners.png")
            break
        case "PinBit":
            imageTarejta = UIImage(named: "tarjeta.jpg")
            break
        default:
            break
        }
        
        
        cell.btnCheck.setImage(imageTarejta, for: UIControlState.normal)
        
        cell.accessoryType = UITableViewCellAccessoryType.none
        let image = UIImage(named: "remove.png")
        //image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        cell.imgDelete.setImage(image, for: UIControlState.normal)
        
        
        cell.indice = String(indexPath.row)
        var alias = tarjetas[indexPath.row].alias!
        if (alias == ""){ alias = tarjetas[indexPath.row].tipo!}
        if let _ = tarjetaActual {
            if(tarjetas[indexPath.row].id == tarjetaActual)
            {
                let origImage = UIImage(named: "remove_naranja.png");
                cell.imgDelete.setImage(origImage, for: .normal)
                //cell.btnEliminar.tintColor = UIColor.init(cgColor: CAGradientLayer().UIColorFromHex(rgbValue: 0x00c4ee, alpha: 1.0).cgColor)
                cell.lblAlias.text = alias
            }
            else
            {
                cell.lblAlias.text = alias
            }
        }
        else {
            cell.lblAlias.text = alias
        }
        
        cell.alias = alias
        cell.id = tarjetas[indexPath.row].id
        cell.numero = tarjetas[indexPath.row].numero!
        //cell.backgroundColor = .whiteColor()
        
        return cell
      
    }
    
    
    
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tarjetas.count
    }
    
    
    //Lista tarjetas de el usuario
    func listaTarjetas(){
        
        // llama el objeto AppDelegate para poder utilizar las funciones que allí se encuentran
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        //se obtiene el contexto: Es la interfaz para acceder a la base de datos...
        let context = appDel.managedObjectContext
        
        
        self.tarjetas.removeAll(keepingCapacity: false)
        
        //traer las tarjetas de la bd...
        let consulta = NSFetchRequest<NSFetchRequestResult>(entityName: "Tarjeta")
        consulta.returnsObjectsAsFaults = false
        
        //recorrer el resultado de la consulta
        let res = try? context.fetch(consulta)
        
        
        print("BUSCANDO TARJETAS ALMACENADAS EN BD ADAPTADOR....\n")
        
        var dic = [[String:String]]()
        
        
        //(tarjetaBD.valueForKey("mascara")! as? String)!
        
        for tarjetaBD:AnyObject in res! as! [NSManagedObject]  {
            
            let jsonTar = [
                
                "numero_enmascarado" : (tarjetaBD.value!(forKey: "mascara")! as? String)!,
                "id" : (tarjetaBD.value(forKey: "id")! as? String)!,
                "alias" : (tarjetaBD.value!(forKey: "alias") as? String)!,
                "tipo" : (tarjetaBD.value!(forKey: "tipo") as? String)!
                ]
            
            dic.append(jsonTar)
        }

        let json = JSON(dic)
        print("lista tarjetas adapter: \(json) \n")
        
        let tarjetas = json.arrayValue
        
        UserDefaults.standard.set(false, forKey: "sinTarjeta")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "conTarjetas"), object: nil)
        for tar in tarjetas
        {
            self.tarjetas.append(Tarjeta(num:  tar["numero_enmascarado"].stringValue, ident: tar["id"].stringValue, alias:  tar["alias"].stringValue, tipo: tar["tipo"].stringValue))
        }
        
        let guardarTarjetas = NSKeyedArchiver.archivedData(withRootObject: self.tarjetas)
        UserDefaults.standard.set(guardarTarjetas, forKey: "tarjetas")
        
        if(tarjetas.count == 0)
        {
            print("Adaptador: Sin tarjetas....\n")
            self.tarjetas.removeAll(keepingCapacity: false)
            UserDefaults.standard.set(true, forKey: "sinTarjeta")
            let codigoRef = UserDefaults.standard.object(forKey: "codigoRef") as! String
            if(codigoRef.isEmpty){
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "sinTarjetas"), object: nil)
                //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addTarjeta"), object: nil)
            }
        }
        else
        {
            print("Adaptador: tarjeta seleccionada: \((UserDefaults.standard.object(forKey: "ultimaTarjeta") as! String))\n")
            
            if((UserDefaults.standard.object(forKey: "ultimaTarjeta") as? String) == nil){
                if(tarjetas.count == 1){
                    print("Seleccionar única tarjeta... \n")
                    
                    //Una sola tarjeta
                    UserDefaults.standard.set(tarjetas[0]["numero_enmascarado"].stringValue,forKey: "numero_enmascarado")
                    let guardarTarjetas = NSKeyedArchiver.archivedData(withRootObject: self.tarjetas)
                    UserDefaults.standard.set(guardarTarjetas, forKey: "tarjetas")
                    self.postTarjeta(idTarjeta: tarjetas[0]["id"].stringValue, mascara: tarjetas[0]["numero_enmascarado"].stringValue)
                    UserDefaults.standard.set(false, forKey: "sinTarjeta")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cerrarPerfil"), object: nil)
                }
                else{
                    print("mostrar dialogo de tarjetas \n")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "verDialogTarjetas"), object: nil)
                    
                }
            }
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cambieTarjeta"), object: nil)
        }
        
        
        //        let guardarTarjetas = NSKeyedArchiver.archivedDataWithRootObject(self.tarjetas)
        //        NSUserDefaults.standardUserDefaults().setObject(guardarTarjetas, forKey: "tarjetas")
        self.tabla.reloadData()
        self.spinner.stopAnimating()
        self.spinner.isHidden = true
        
        print("TArjetas adapter Cargado!!! \n")
        
    }
    
    
    //Selecciona una tarjeta para usar
    func postTarjeta(idTarjeta: String, mascara: String)
    {
        
        print("ADAPTER: SELECCIONAR TARJETA  \n")
        let url = urlPinbit + "/seleccionar_tarjetas.json"
        print("\(url)\n")
        let token = UserDefaults.standard.object(forKey: "token") as! String
        let parameters = [
            "tarjeta": [
                "id":idTarjeta
            ], "authenticity_token":token ] as [String : Any]
        
        
        request(url,method: .post, parameters: parameters as? [String : AnyObject]).responseJSON {
            response in
            print("Respuesta del servidor: \(response.response?.statusCode)\n")
            switch response.result {
            case .success(_):
                if(response.response?.statusCode == 200)
                {
                    print("adapter: tarjeta seleccionada  \n")
                    //se almacena la ultima tarjeta seleccionada
                    UserDefaults.standard.setValue(idTarjeta, forKey: "ultimaTarjeta")
                    
                    UserDefaults.standard.set(mascara, forKey: "numero_enmascarado")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cambieTarjeta"), object: nil)
                }
                else
                {
                    //Error de respuesta servidor
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    //Acción para adicionar una tarjeta
    @IBAction func btnAddTarjeta(_ sender: AnyObject) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addTarjeta"), object: nil)
    }
    
    //Refrescar las tarjetas del ususario
    @objc func refreshTarjetas(sender: AnyObject){
        self.listaTarjetas()
        
        
    }
}
