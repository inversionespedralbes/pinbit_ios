//
//  DialogoSimpleController.swift
//  PYC_mobile
//
//  Created by Sebastian Vivas on 4/05/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class DialogoSimpleController: UIViewController {

    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var lblContenido: UILabel!
    @IBOutlet weak var vistaDialogo: UIView!
    @IBOutlet weak var btnAceptar: UIButton!
    
    required override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.vistaDialogo.layer.cornerRadius = 5
        self.vistaDialogo.layer.shadowOpacity = 0.8
        self.vistaDialogo.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        btnAceptar.layer.masksToBounds = true
        btnAceptar.layer.cornerRadius = 3
        btnAceptar.setTitle(NSLocalizedString("aceptar", comment: ""), for: UIControlState())
        
    }
    
    func showInView(aView: UIView!, titulo: String!, cuerpo: String!, animated: Bool)
    {
        aView.addSubview(self.view)
        self.view.frame = aView.frame
        lblTitulo.text = titulo
        lblContenido.text = cuerpo
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }

    @IBAction func btnAction(_ ender: AnyObject) {
        let codigoRef = UserDefaults.standard.object(forKey: "codigoRef") as! String
        
        if(!codigoRef.isEmpty){
            UserDefaults.standard.setValue("", forKey: "codigoRef")
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addTarjeta"), object: nil)
        }
         removeAnimate()
    }
}
