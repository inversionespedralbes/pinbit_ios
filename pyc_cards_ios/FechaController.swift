//
//  FechaController.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 9/06/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

protocol DataPickerViewControllerDelegate : class {
    
    func datePickerVCDismissed(date : NSDate?)
}

class FechaController: UIViewController {
    
    
    @IBOutlet weak var vistaContenedor: UIView!
    @IBOutlet weak var fechaPicker: UIDatePicker!
    @IBOutlet weak var btnSeleccionar: UIButton!
    weak var delegate : DataPickerViewControllerDelegate?
    
    var currentDate : NSDate? {
        didSet {
            updatePickerCurrentDate()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updatePickerCurrentDate()
        btnSeleccionar.layer.masksToBounds = true
        btnSeleccionar.layer.cornerRadius = 3.0
        print("Estoy en el dialogo", terminator: "")
        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.delegate?.datePickerVCDismissed(date: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func updatePickerCurrentDate() {
        
        if let _currentDate = self.currentDate {
            if let _datePicker = self.fechaPicker {
                _datePicker.date = _currentDate as Date
            }
        }
    }
    
    
    @IBAction func betnSeleccionar(_ sender: AnyObject) {
        self.dismiss(animated: true) {
            
            let nsdate = self.fechaPicker.date
            self.delegate?.datePickerVCDismissed(date: nsdate as NSDate?)
        }
    }
    
    convenience init() {
        self.init(nibName: "FechaPicker", bundle: nil)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
