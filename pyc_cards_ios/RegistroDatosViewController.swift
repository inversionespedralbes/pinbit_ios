//
//  RegistroDatosViewController.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 29/05/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit
import Alamofire

class RegistroDatosViewController: UIViewController, UITextFieldDelegate{
    
//    var nombre: String!
//    var cedula: String!
    
    //Variables interfaz
    @IBOutlet weak var txtPassC: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var btnSiguiente: UIButton!
    @IBOutlet weak var toolbar: UINavigationBar!
    @IBOutlet weak var lblHeLeidoTerminos: UILabel!
    @IBOutlet weak var btnTerminos: UIButton!
    @IBOutlet weak var lblBienvenida: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    
    @IBOutlet weak var LbConfirmPass: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var lblerror: UILabel!
    @IBOutlet weak var lblerror2: UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    var check = false
    
    var nombre:String!
    var pass:String!
    var passConfir:String!
    var movil:String!
    
    var dialogoSimple: DialogoSimpleController!
    
    var dialogo: DialogoRegistroController!
    var dialogoInformacion: DialogoSimpleController!
    var loader: DialogoCargaController!
    var aceptaTerminos = false
    
    //para reconocer cuando tocan el link de terminos y condiciones
    var tocarTerminos = UITapGestureRecognizer()
    var dialogTerminos: TerminosController!

    
    var mostrandoTeclado: Bool!
    
    var scrollGestureRecognizer: UITapGestureRecognizer!
    var keyboardHeight:CGFloat!
    var textFields: [UITextField]!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let imageBackground = SetBackground(view)
        view.addSubview(imageBackground)
        self.view.sendSubview(toBack: imageBackground)
        

        txtPass.delegate = self
        txtPassC.delegate = self
        txtPass.layer.borderColor = UIColor.white.cgColor
        txtPass.layer.borderWidth = 1
        txtPass.layer.cornerRadius = 5
        txtPassC.layer.borderColor = UIColor.white.cgColor
        txtPassC.layer.borderWidth = 1
        txtPassC.layer.cornerRadius = 5
        
        
        //txtUsuario.resignFirstResponder()
        self.llenarCampos()
        // Do any additional setup after loading the view.
        //self.createCheckboxes()
        
        addDoneButtonOnKeyboard()
        
        textFields = [ self.txtPass, self.txtPassC]

        
        
        NotificationCenter.default.addObserver(self, selector: #selector(RegistroDatosViewController.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RegistroDatosViewController.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        scrollGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegistroDatosViewController.hideKeyBoard))
        scrollView.addGestureRecognizer(scrollGestureRecognizer)
    }
    
     @objc func keyboardWillShow(notification: NSNotification){
        let info:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardHeight = keyboardSize.height
        
        setScrollViewPosition()
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        bottomConstraint.constant = 40
        self.view.layoutIfNeeded()
    }
    
    func hideKeyBoard(){
        // Hide the keyboard
        view.endEditing(true)
    }
    
    func setScrollViewPosition(){
        // Modificamos el valor de la constante del constraint inferior, le damos la altura del teclado más 20 de marge. De este modo estamos agrandando el Scroll View lo suficiente para poder hacer scroll hacia arriba y trasladar el UITextField hasta que quede a la vista del usuario. Ejecutamos el cambio en el constraint con la función layoutIfNeeded().
        bottomConstraint.constant = keyboardHeight + 20
        self.view.layoutIfNeeded()
        
        // Calculamos la altura de la pantalla
        let screenSize: CGRect = UIScreen.main.bounds
        let screenHeight: CGFloat = screenSize.height
        
        // Recorremos el array de textFields en busca de quien tiene el foco
        for textField in textFields {
            if textField.isFirstResponder {
                // Guardamos la posición 'Y' del UITextField
                let yPositionField = textField.frame.origin.y
                // Guardamos la altura del UITextField
                let heightField = textField.frame.size.height
                // Calculamos la 'Y' máxima del UITextField
                let yPositionMaxField = yPositionField + heightField
                // Calculamos la 'Y' máxima del View que no queda oculta por el teclado
                let Ymax = screenHeight - keyboardHeight
                // Comprobamos si nuestra 'Y' máxima del UITextField es superior a la Ymax
                if Ymax < yPositionMaxField {
                    // Comprobar si la 'Ymax' el UITextField es más grande que el tamaño de la pantalla
                    if yPositionMaxField > screenHeight {
                        let diff = yPositionMaxField - screenHeight
                        print("El UITextField se sale por debajo \(diff) unidades")
                        // Hay que añadir la distancia a la que está por debajo el UITextField ya que se sale del screen height
                        scrollView.setContentOffset(CGPoint(x: 0, y: 40), animated: true)
                    }else{
                        // El UITextField queda oculto por el teclado, entonces movemos el Scroll View
                        scrollView.setContentOffset(CGPoint(x: 0, y: self.keyboardHeight - 100), animated: true)
                        
                    }
                }else{print("NO MUEVO EL SCROLL")}
            }
        }
    }


    
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("siguiente", comment: ""), style: UIBarButtonItemStyle.done, target: self, action: #selector(RegistroDatosViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.txtPass.inputAccessoryView = doneToolbar
        
    }
    
    func doneButtonAction()
    {
        print("txtPass")
        txtPassC.becomeFirstResponder()
    }
    
    
    
    @IBAction func verTerminos(_ sender: AnyObject) {
        print("\n Mostrar EULA \n")
        self.view.endEditing(true)
        self.dialogTerminos = TerminosController(nibName: "TerminosCondiciones", bundle: nil)
        self.dialogTerminos.showInView(aView: self.view, animated: true)
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("Acabé de mostrar datos del nuevo registro", terminator: "")
        if(UserDefaults.standard.object( forKey: "primera_vez") != nil){
            print("cerrar Registro...\n")
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewDidLayoutSubviews() {
        //CAGradientLayer().fondoDegrade(self.view, color1: CAGradientLayer().UIColorFromHex(0x0092f1, alpha: 1), color2: CAGradientLayer().UIColorFromHex(0x005eab, alpha: 1))
        self.view.layoutSubviews()
    }


    
    @IBAction func btnSiguiente(_ sender: AnyObject) {
        
        if(txtPassC.text != txtPass.text)
        {
            dialogoSimple = DialogoSimpleController(nibName: "DialogoSimple", bundle: nil)
            dialogoSimple.showInView(aView: self.view, titulo: NSLocalizedString("titulo_resgistro", comment: ""), cuerpo: NSLocalizedString("error_password_registro", comment: ""), animated: true)
        }
        else if(txtPass.text!.characters.count < 4)
        {
            dialogoSimple = DialogoSimpleController(nibName: "DialogoSimple", bundle: nil)
            dialogoSimple.showInView(aView: self.view, titulo: NSLocalizedString("titulo_resgistro", comment: ""), cuerpo: NSLocalizedString("validacion_password", comment: ""), animated: true)
        }
        else if(!aceptaTerminos){
            dialogoSimple = DialogoSimpleController(nibName: "DialogoSimple", bundle: nil)
            dialogoSimple.showInView(aView: self.view, titulo: NSLocalizedString("titulo_resgistro", comment: ""), cuerpo: NSLocalizedString("error_eula", comment: ""), animated: true)
        }
        else
        {
            if(InternetConnection.isConnectedToNetwork()){//verifica si el dispositivo tiene conexion a internet
                if(UserDefaults.standard.object(forKey: "device_id") != nil){
                     registrarse()
                }
                else{
                    print("Error no hay device_id")
                    self.view.makeToast(NSLocalizedString("error_registro", comment: ""))
                }
            }
            else{
                self.view.makeToast(NSLocalizedString("no_internet", comment: ""))
            }
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        if(textField == txtPass)
        {
            print("txtPass")
            txtPassC.becomeFirstResponder()
        }
        else if(textField == txtPassC)
        {
            print("PassC")
            //txtNombre.becomeFirstResponder()
            self.view.endEditing(true)
        }
       
        return true
    }
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        
//        
//        /*nav.nombre = nombre
//        nav.documento = cedula
//        nav.pass = txtPass.text
//        nav.passC = txtPassC.text
//        nav.movil = txtMovil.text
//        nav.usuario = txtUsuario.text*/
//    }
    

    
    @IBAction func btnCancel(_ sender: AnyObject) {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Loggin")
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func llenarCampos(){
       
        lblBienvenida.text = NSLocalizedString("ingresa_pass", comment: "")
        LbConfirmPass.text = NSLocalizedString("confirma_pass", comment: "")
        lblerror.text = NSLocalizedString("min_pass", comment: "")
        lblerror2.text = NSLocalizedString("min_pass", comment: "")
        lblHeLeidoTerminos.text = NSLocalizedString("he_leido", comment: "")
        
        //Pone el texto subrayado
        let textEula = NSLocalizedString("link_eula", comment: "")
        let attrib = [NSForegroundColorAttributeName: UIColor.white,
                      NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue] as [String : Any]
        let linkEula = NSAttributedString(string: textEula, attributes: attrib)
        btnTerminos.setAttributedTitle(linkEula, for: UIControlState())

    }
    
    
    
    
    //Post para registrar ususario
    func registrarse() {
        self.view.endEditing(true)//Oculta el teclado....
        
        loader = DialogoCargaController(nibName: "DialogoCarga", bundle: nil)
        loader.showInView(aView: self.view, titulo: NSLocalizedString("procesando", comment: ""), animated: true)
        
        self.view.isUserInteractionEnabled = false
        
        let url = urlPinbit + "/wallets.json"
        print("URL registro: \(url)\n")
        
               self.pass = txtPass.text
        self.passConfir = txtPassC.text
        
        let deviceID = UserDefaults.standard.object(forKey: "device_id") as! String

        
        let parameters = [
            "wallet": [
                "password": self.pass!,
                "password_confirmation" : self.passConfir!,
                "device_id": UserDefaults.standard.object(forKey: "device_id") as! String]]
        
        print("datos enviados registro: \(parameters)")
        
        request(url,method: .post, parameters: parameters).responseJSON {
           response in
            print("Respuesta del servidor registro: \(response.response!.statusCode)\n")
            print("Respuesta del servidor registro: \(response.response!.description)\n")
            switch response.result {
            case .success(let data):
                if(response.response?.statusCode != 201)
                {
                    self.loader.removeAnimate()
                    let error = "\(data)"
                    self.dialogoInformacion = DialogoSimpleController(nibName: "DialogoSimple", bundle: nil)
                    self.dialogoInformacion.showInView(aView: self.view, titulo:  NSLocalizedString("registro_fallido", comment: ""), cuerpo: "\(self.manejarError(errorP: error))", animated: true)
                    
                    self.view.isUserInteractionEnabled = true
                    self.view.endEditing(true)
                    print("Resp: \(data)")
                }
            case .failure(let error):
                print(error)
                self.loader.removeAnimate()
                self.view.makeToast(NSLocalizedString("error_conexion", comment: ""))
            }
        }
        
        self.view.isUserInteractionEnabled = true
        
    }
  
    
    //Arreglo del string de error para una mejor presentación
    func manejarError(errorP: String!) -> String{
        var error = errorP
        error = error?.replacingOccurrences(of: "{", with: "")
        error = error?.replacingOccurrences(of: "}", with: "")
        error = error?.replacingOccurrences(of: "_", with: " ")
        error = error?.replacingOccurrences(of: "(", with: "")
        error = error?.replacingOccurrences(of: ")", with: "")
        error = error?.replacingOccurrences(of: ":", with: "")
        error = error?.replacingOccurrences(of: "\n", with: "")
        error = error?.replacingOccurrences(of: ";", with: "\n")
        error = error?.replacingOccurrences(of: "\"", with: "")
        error = error?.replacingOccurrences(of: "=", with: "")
        error = error?.replacingOccurrences(of: " ", with: "")
        return error!
    }


    @IBAction func CheckBoxAction(_ sender: AnyObject) {
        if(self.check){
            self.btnCheck.setImage(UIImage(named: "unchecked_checkbox"), for: .normal)
            self.btnCheck.tintColor = UIColor.white
            self.check = false
            self.aceptaTerminos = false
        }
        else{
            self.btnCheck.setImage(UIImage(named: "checked_checkbox"), for: .normal)
             self.btnCheck.tintColor = UIColor.white
            self.check = true
            self.aceptaTerminos = true
        }
        
        print("checkbox has state \(self.check)");
    }
    

}
