//
//  ChatBitrixController.swift
//  pyc_cards_ios
//
//  Created by deisy melo on 18/11/16.
//  Copyright © 2016 PinBit. All rights reserved.
//

import Foundation

class ChatBitrixController:UIViewController {
    
    
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var webView: UIWebView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = NSURL (string: urlPinbit + "chat-button");
        let requestObj = NSURLRequest(url: url! as URL);
        webView.loadRequest(requestObj as URLRequest);
    }
    
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true)
    }
}
