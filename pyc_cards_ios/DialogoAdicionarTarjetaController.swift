//
//  DialogoAdicionarTarjetaController.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 7/07/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class DialogoAdicionarTarjetaController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var vistaDialogo: UIView!
    @IBOutlet weak var txtTarjeta: UITextField!
    @IBOutlet weak var txtMes: UITextField!
    @IBOutlet weak var txtCVV: UITextField!
    @IBOutlet weak var txtAno: UITextField!
    @IBOutlet weak var txtAlias: UITextField!
    @IBOutlet weak var btnRegistro: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var btnCancelar: UIButton!
    
    
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var lblExppiracion: UILabel!
    @IBOutlet weak var lblAlias: UILabel!
    @IBOutlet weak var lblCVV: UILabel!
    var dialogoLoader:DialogoCargaController!
    
    
    var idTarjeta: String!
    var numeroTarjeta: String!
    var keyPinBit:String!
    
    var tipo: String!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fillTexts()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.vistaDialogo.layer.cornerRadius = 5
        self.vistaDialogo.layer.shadowOpacity = 0.8
        self.vistaDialogo.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.btnRegistro.layer.masksToBounds = true
        self.btnRegistro.layer.cornerRadius = 3.0
        self.btnCancelar.layer.masksToBounds = true
        self.btnCancelar.layer.cornerRadius = 3.0
        spinner.isHidden = true
        self.txtAlias.delegate = self
        print("REGISTRAR TARJETA \n")
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Animaciones de dialogo
    func showInView(_ aView: UIView!, numero: String!, mes: String!, anio: String!, cvv: String!,tipo: String!, animated: Bool)
    {
        aView.addSubview(self.view)
        //self.keyPinBit = key
        self.view.frame = aView.frame
        self.txtTarjeta.text = numero
        self.txtAno.text = anio
        self.txtMes.text = mes
        self.txtCVV.text = cvv
        self.tipo = tipo
        if animated
        {
            self.showAnimate()
        }
        
        //print("KEY PINBIT: \(self.keyPinBit!)\n")
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform( scaleX: 1.3,y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate( withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform( scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    //Boton para agregar tarjeta
    @IBAction func btnRegistrar(_ sender: AnyObject) {
        spinner.startAnimating()
        spinner.isHidden = false
        self.vistaDialogo.isUserInteractionEnabled = false
        
        if(InternetConnection.isConnectedToNetwork()){//verifica si el dispositivo tiene conexion a internet
            
            self.getKeyPinBit()
        }
        else {
            self.view.makeToast(NSLocalizedString("no_internet", comment: ""))
            spinner.isHidden = true
            self.vistaDialogo.isUserInteractionEnabled = true
        }
        
        
    }
    
    
    @IBAction func btnCancelar(_ sender: AnyObject) {
        self.removeAnimate()
    }
    
   
    func getKeyPinBit(){
        
        print("-- getKeyPinBit -- \n")
        
     
        if(InternetConnection.isConnectedToNetwork()){//verifica si el dispositivo tiene conexion a internet
            //trae el KEY de PinBit para poder hacer la captura de la tarjeta
            //y comunicarse con la bóveda
            
            let url = urlPinbit + "/gateway_sessions.json"
            print("Obtención de KEY para bóveda: \n")
            print(url)
            let token = UserDefaults.standard.object(forKey: "token") as! String
            let parametros = [
                "authenticity_token":token ]
            
            request(url,method: .post, parameters: parametros).responseJSON {
                response in
                print("Respuesta del servidor...: \(response.response!.statusCode)")
                
                switch response.result {
                case .success(let data):
                    if(response.response!.statusCode == 201){
                        print("key PinBit: \(data)")
                        
                        let json = JSON(data)
                        self.keyPinBit = json["key"].stringValue
                        self.postAdicionarTarjetaBoveda()
                    
                    }
                    else{
                        print("falló Key")
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "errorRegistroTarjeta"), object: nil)
                        self.removeAnimate()
                    }
                case .failure(let error):
                    print(error)
                    print("falló Key")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "errorRegistroTarjeta"), object: nil)
                    self.removeAnimate()
                }
                
            }
            
        }
        else{
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "errorRegistroTarjeta"), object: nil)
        }
        
    }
    
    //Adicionar una tarjeta en la boveda
    func postAdicionarTarjetaBoveda() {
        print("-- postAdicionarTarjetaBoveda -- \n")
        let url = urlBoveda + "/tokens.json"
        //let token = NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        //let deviceID = NSUserDefaults.standardUserDefaults().objectForKey("device_id") as! String
        let parameters = [
            "card": [
                "number": self.txtTarjeta.text!,
                "exp_month": self.txtMes.text!,
                "exp_year": self.txtAno.text!,
                "cvc": self.txtCVV.text!,
                "alias": self.txtAlias.text!,
            ],
            "key":self.keyPinBit] as [String : Any]
        
        print("url registro tarjeta: \(url) \n")
        print("paraemtros: \(parameters)\n")
        
        request(url,method: .post, parameters: parameters).responseJSON {
            response in
            
            print("RESPUESTA DE BOVEDA: \(response.response?.statusCode), \(response.response?.description) \n")
            switch response.result {
            case .success(let data):
                if(response.response?.statusCode == 200)
                {
                    //capturar datos recibidos de la boveda y realizar el registro en PinBit
                    let json = JSON(data)
                    self.postAdicionarTarjetaPinbit(tokenBoveda: json["card"]["token"].stringValue, mascara: json["card"]["masked_number"].stringValue, alias: self.txtAlias.text!)
                    
                }
                else
                {
                    //Error de respuesta servidor
                    print("Error BOVEDA \n")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "errorRegistroTarjeta"), object: nil)
                    self.removeAnimate()
                    
                    
                }
            case .failure(let error):
                print(error)
                print("2: Error BOVEDA \n")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "errorRegistroTarjeta"), object: nil)
                
                self.removeAnimate()
            }
        }
    }

    
    //se agrega a PinBiT la tarjeta registrada en la Boveda
    func postAdicionarTarjetaPinbit(tokenBoveda: String!, mascara: String!, alias: String!) {
        print("-- postAdicionarTarjetaPinbit -- \n")
        let url = urlPinbit + "/tarjetas.json"
         print("REGISTRO TARJETA PINBIT: url \(url)\n")
         print("tipo tarjtea: \(self.tipo!)\n")
        
        let token = UserDefaults.standard.object(forKey: "token") as! String
        let parameters = [
            "card": [
                "token": tokenBoveda!,
                "numero_enmascarado": mascara!,
                "alias": alias!,
                "pon": self.tipo!],
            "authenticity_token":token] as [String : Any]
        
       
        print("parametros: \(parameters)\n")
        request(url,method: .post, parameters: parameters).responseJSON {
            response in
            
            print("RESPUESTA DE PINBIT: \(response.response!.statusCode), \(response.description) \n")
            switch response.result {
            case .success(let data):
                if(response.response!.statusCode == 201)
                {
                    print("datos recibidos: \(data) \n")
                    let json = JSON(data)
                    self.almacenarTarjetaBD(json["card"]["id"].stringValue, mascara: json["card"]["masked_number"].stringValue, alias: json["card"]["alias"].stringValue)
                    
                }
                else
                {
                    //Error de respuesta servidor
                }

            case .failure(let error):
                print(error)
            }
            
        }
    }
    
    func almacenarTarjetaBD(_ id:String, mascara: String, alias:String){
        // llama el objeto AppDelegate para poder utilizar las funciones que allí se encuentran
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        //se obtiene el contexto: Es la interfaz para acceder a la base de datos...
        let context = appDel.managedObjectContext
        
        //se crea una nueva tarjeta 
        let tarjetaNueva: AnyObject = NSEntityDescription.insertNewObject(forEntityName: "Tarjeta", into: context)
        //Insertar Datos
        tarjetaNueva.setValue(id, forKey: "id")
        tarjetaNueva.setValue(mascara, forKey: "mascara")
        tarjetaNueva.setValue(alias, forKey: "alias")
        tarjetaNueva.setValue(self.tipo, forKey: "tipo")
        //guardar arjeta en la bd
        try! context.save()
        
        //Terminar el proceso de registro... cerrar el dialog, seleccionar la tarjeta registrada....
        
        self.postTarjeta(idTarjeta: id, numeroTarjeta: mascara)
        //NSNotificationCenter.defaultCenter().postNotificationName("nuevaTarjeta", object: nil)
        

    }
    

    //Seleccionar tarjeta en el servidor
    func postTarjeta(idTarjeta: String, numeroTarjeta: String){
        print("SELECCIONAR TARJETA.... REGISTRO \n")
        
        
        let url = urlPinbit + "/seleccionar_tarjetas.json"
        print("\(url) \n")
        let token = UserDefaults.standard.object(forKey: "token") as! String
        let parameters = [
            "tarjeta": [
                "id":idTarjeta
            ], "authenticity_token":token ] as [String : Any] 
        
        
        request(url,method: .post, parameters: parameters).responseJSON {
            response in
            print("Respuesta del servidor: \(response.response!.statusCode) \n")
            
            switch response.result {
            case .success(_):
                if(response.response!.statusCode == 200)
                {
                    print("tarjeta seleccionada....")
                    //se almacena la ultima tarjeta seleccionada
                    UserDefaults.standard.setValue(idTarjeta, forKey: "ultimaTarjeta")
                    UserDefaults.standard.setValue(numeroTarjeta, forKey: "numero_enmascarado")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "nuevaTarjeta"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tarjetaRegistrada"), object: nil)
                }
                else
                {
                    //Error de respuesta servidor
                }

            case .failure(let error):
                print(error)
            }
            
            self.spinner.stopAnimating()
            self.spinner.isHidden = true
            self.removeAnimate()
        }

    }
    
    //Maneja eventos del teclado
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //Init textos
    func fillTexts(){
        self.lblTitulo.text = NSLocalizedString("tu_tarjeta", comment: "")
        self.lblExppiracion.text = NSLocalizedString("fecha_expiracion", comment: "")
        self.lblCVV.text = NSLocalizedString("codigo_seguridad", comment: "")
        self.lblAlias.text = NSLocalizedString("alias", comment: "")
        self.txtAlias.placeholder = NSLocalizedString("alias_tarjeta", comment: "")
        self.btnRegistro.setTitle(NSLocalizedString("registrar_tarjeta", comment: ""), for: UIControlState())
        self.btnCancelar.setTitle(NSLocalizedString("cancelar", comment: ""), for: UIControlState())
    }

}
