//
//  SetBackGround.swift
//  pyc_cards_ios
//
//  Created by deisy melo on 5/08/16.
//  Copyright © 2016 PinBit. All rights reserved.
//

import Foundation

func SetBackground(_ view:UIView, fondo:String = "bg_app.png") -> UIImageView
{
    let background = UIImage(named: fondo )
    
    let imageView = UIImageView(frame: view.bounds)
    imageView.contentMode =  UIViewContentMode.scaleAspectFill
    imageView.clipsToBounds = true
    imageView.image = background
    imageView.center = view.center
    //view.addSubview(imageView)
    
    return imageView
    
}

