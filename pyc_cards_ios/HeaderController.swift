//
//  HeaderController.swift
//  PYC_mobile
//
//  Created by Sebastian Vivas on 28/04/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class HeaderController: UIView {

    @IBOutlet var view: HeaderController!
    @IBOutlet weak var lblSaldo: UILabel!
    //@IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var btnSaldo: UIButton!
    //@IBOutlet weak var lblSaludo: UILabel!
    @IBOutlet weak var lblTuTarjeta: UILabel!
    //@IBOutlet weak var btnCambiarTarjeta: UIButton!
    @IBOutlet weak var lblTitulo: UILabel!
    
    var actualizo: Bool! = false
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("Header", owner: self, options: nil)
        self.addSubview(self.view)
        
        self.llenarTextos()
        //self.backgroundColor = UIColor.clearColor()
        //CAGradientLayer().fondoAzulDegrade(self)
        
        //        let fullName = (NSUserDefaults.standardUserDefaults().objectForKey("usuario_nombre") as? String)?.capitalizedString as String!
        //        var fullNameArr = fullName.characters.split {$0 == " "}.map { String($0) }
        //        let firstName: String = fullNameArr[0]
        
        //lblNombre.text = firstName
        
        //print("MIRANDO LA MASCARA HEADER: \(NSUserDefaults.standardUserDefaults().objectForKey("numero_enmascarado") as! NSString)")
        
        if ((UserDefaults.standard.object(forKey: "numero_enmascarado")) != nil){
            let tarjeta = (UserDefaults.standard.object(forKey: "numero_enmascarado") as! NSString)
            if(tarjeta != ""){
                lblSaldo.text = tarjeta.substring(with: NSRange(location: tarjeta.length-6, length: 6)).capitalized //capitalizedString
                lblSaldo.layoutIfNeeded()
                
            }
            
        }
        
    }
    

    //Inicializar textos
    func llenarTextos(){
        //lblSaludo.text = NSLocalizedString("hola", comment: "")
        //lblTuTarjeta.text = NSLocalizedString("tu_tarjeta_header", comment: "")
    }
    
}
    

