//
//  ManejadorTransiciones.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 21/07/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class ManejadorTransiciones: NSObject, UITabBarControllerDelegate, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    
    private var reverse = true
    var tarjetasDatos: Bool!
    var toolBar: UITabBarController!
    var mayor = true
    private var interactive = false
    
    
    //Cargar la barra de tabs
    func cargarTabBar(tab:UITabBarController)
    {
        self.toolBar = tab
    }
    
    //Funcion animar la transición de tabs
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let container = transitionContext.containerView
        //let fromView = transitionContext.viewForKey(UITransitionContextFromViewKey)!
        //let toView = transitionContext.viewForKey(UITransitionContextToViewKey)!
        let fromView = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!.view // iOS 7
        let toView = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!.view // iOS 7
        let from = Int((transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)?.restorationIdentifier!)!)
        let to = Int((transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)?.restorationIdentifier!)!)
        
        //Mirando si hace transición a la derecha o izquierda
        if(from! < to!)
        {
            mayor = true
        }
        else
        {
            mayor = false
        }
        container.addSubview(toView!)
        
        self.mayor ? (toView?.frame = CGRect(x: toView!.frame.size.width-20, y:  -toView!.frame.origin.y, width: toView!.frame.size.width, height: toView!.frame.size.height)) : (toView?.frame = (CGRect(x:(toView?.frame.size.width)!+10,y: (toView?.frame.origin.y)!, width: (toView?.frame.size.width)!, height: (toView?.frame.size.height)!)))
        
        self.reverse ? container.sendSubview(toBack: toView!) : container.bringSubview(toFront: toView!)
        
        //Animación
        let duration:TimeInterval = self.transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.8, options: [], animations: {
            self.mayor ? (fromView?.frame = CGRect(x: -(toView?.frame.size.width)!-20, y: (toView?.frame.origin.y)!, width: (toView?.frame.size.width)!, height: (toView?.frame.size.height)!)) : (fromView?.frame = CGRect(x:(toView?.frame.size.width)!+10, y: (toView?.frame.origin.y)!, width :(toView?.frame.size.width)!, height: (toView?.frame.size.height)!))
            
            toView?.frame = CGRect(x: 0, y: (toView?.frame.origin.y)!, width: (toView?.frame.size.width)!, height: (toView?.frame.size.height)!)//CGRectMake(0, toView.frame.origin.y, toView.frame.size.width, toView?.frame.size.height);
            
            }, completion: { finished in
                if(transitionContext.transitionWasCancelled)
                {
                    toView?.frame = CGRect(x: 0, y: (toView?.frame.origin.y)!,width: (toView?.frame.size.width)!, height: (toView?.frame.size.height)!);
                    fromView?.frame = CGRect(x: 0, y: fromView!.frame.origin.y,width: fromView!.frame.size.width, height: (fromView?.frame.size.height)!);
                }
                else
                {
                    fromView?.removeFromSuperview()
                    
                    
                    self.mayor ? (fromView?.frame = CGRect(x: -toView!.frame.size.width-20, y: fromView!.frame.origin.y, width: fromView!.frame.size.width, height: fromView!.frame.size.height)) : (fromView?.frame = CGRect(x: (toView?.frame.size.width)!+10, y: (fromView?.frame.origin.y)!, width: (fromView?.frame.size.width)!, height: (fromView?.frame.size.height)!))
                    
                    toView?.frame = CGRect(x: 0, y: (toView?.frame.origin.y)!, width: (toView?.frame.size.width)!, height: (toView?.frame.size.height)!)//CGRectMake(0, toView.frame.origin.y, toView.frame.size.width, toView?.frame.size.height);
                }
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
        
    }
    
    //Tiempo que dura la animación
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    //Animacion para el controlador que se esta mostrando
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.reverse = true
        return self
    }
    
    //Función que hacer cuando la animación sea rechazada
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.reverse = false
        return self
    }
    
    //Asigna la animación al tabbar
    func tabBarController(tabBarController: UITabBarController, animationControllerForTransitionFromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    //Bloquea tabs cuando el usuario no tiene tarjetas
    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        let tarjeta = UserDefaults.standard.bool(forKey: "sinTarjeta")
        if(tarjeta == true)
        {
            self.toolBar.selectedIndex = 2
            return false
        }
        return true
    }
    
}
