//
//  Movimiento.swift
//  PYC_mobile
//
//  Created by Sebastian Vivas on 6/04/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import Foundation

class Movimiento {
    
    var valor: String
    var longitud_pago: Double
    var latitud_pago: Double
    var longitud_cobro: Double
    var latitud_cobro: Double
    var descripcion: String
    var fecha: String
    
    init (valor: String, longitud_pago: String, latitud_pago: String,longitud_cobro: String, latitud_cobro: String, descripcion: String, fecha: String )
    {
        self.valor = valor
        self.longitud_pago =  (longitud_pago as NSString).doubleValue
        self.latitud_pago = (latitud_pago as NSString).doubleValue
        self.longitud_cobro =  (longitud_cobro as NSString).doubleValue
        self.latitud_cobro = (latitud_cobro as NSString).doubleValue
        self.descripcion = descripcion
        self.fecha = fecha
    }
    
}
