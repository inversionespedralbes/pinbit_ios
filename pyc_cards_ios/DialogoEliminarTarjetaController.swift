//
//  DialogoRegistroController.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 5/06/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class DialogoEliminarTarjetaController: UIViewController {
    
    //Interfaz
    
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var viewDialogo: UIView!
    @IBOutlet weak var lblAlias: UILabel!
    @IBOutlet weak var lblNumero: UILabel!
    @IBOutlet weak var btnEliminar: UIButton!
    @IBOutlet weak var btnCancelar: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    //Variables
    var indiceEliminar: String!
    var idEliminar: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fillTexts()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.viewDialogo.layer.cornerRadius = 5
        self.viewDialogo.layer.shadowOpacity = 0.8
        self.viewDialogo.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        btnEliminar.layer.masksToBounds = true
        btnEliminar.layer.cornerRadius = 3.0
        btnCancelar.layer.masksToBounds = true
        btnCancelar.layer.cornerRadius = 3.0
        
        spinner.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //Se encarga de mostrar el dialogo
    func showInView(aView: UIView!, animated: Bool, numero: String!, alias: String!, indice: String!, id: String!)
    {
        aView.addSubview(self.view)
        lblAlias.text = alias
        lblNumero.text = numero
        self.indiceEliminar = indice
        self.idEliminar = id
        self.view.frame = aView.frame
        if animated
        {
            self.showAnimate()
        }
    }
    
    
    //Animación del dialogo
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    //Animación de desaparición del dialogo
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    //Acción boton eliminar
    
    @IBAction func btnEliminar(_ sender: AnyObject) {
        
        spinner.startAnimating()
        spinner.isHidden = false
        self.viewDialogo.isUserInteractionEnabled = false
        self.deleteTarjeta()
    }
    
    //Acción para cancelar
    @IBAction func btnCancelar(_ sender: AnyObject) {
        self.removeAnimate()
    }
    
    //Funcion que elimina la tarjeta
    func deleteTarjeta()
    {
        print("ELIMINAR TARJETA: \n")
        
        let token = UserDefaults.standard.object(forKey: "token") as! String
        let customAllowedSet =  NSCharacterSet(charactersIn:"=\"#%/<>?@\\^`{|}+").inverted
        let escapedString = token.addingPercentEncoding(withAllowedCharacters: customAllowedSet) //stringByAddingPercentEncodingWithAllowedCharacters(customAllowedSet)
       
        var url = "/tarjetas/\(idEliminar!).json?authenticity_token=" + escapedString!
        url = urlPinbit + url
        
        print("\(url) \n")
        
        
        
        request(url, method: .delete).responseJSON { response in
            
            print("Respuesta del servidor ELIMINAR TARJETA: \(response.response?.statusCode)\n \(response.description)\n ")
            
            if(response.response?.statusCode==204){
                print("Tarjeta eliminada \n")
                
                // llama el objeto AppDelegate para poder utilizar las funciones que allí se encuentran
                let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                
                //se obtiene el contexto: Es la interfaz para acceder a la base de datos...
                let context = appDel.managedObjectContext
                
                //consultar un dato especifico...
                let dato = NSFetchRequest<NSFetchRequestResult>(entityName: "Tarjeta")
                
                //recorrer el resultado de la consulta....
                let tarjeta = try? context.fetch(dato)
                
                for res in tarjeta as! [NSManagedObject] {
                    if let id = res.value(forKey: "id")! as? String{
                        if id == self.idEliminar {
                            //borrar Usuario
                            context.delete(res)
                            print("Se eliminó la tarjeta de la BD")
                        }
                    }
                }
                try! context.save()
                
                if(self.idEliminar == (UserDefaults.standard.value(forKey: "ultimaTarjeta") as! String)){
                    print("ES LA TARJETA ACTUAL \n")
                    UserDefaults.standard.setValue(nil, forKey: "ultimaTarjeta")
                    UserDefaults.standard.set("",forKey: "numero_enmascarado")
                }
                
                self.spinner.stopAnimating()
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "nuevaTarjeta"), object: nil)
                
                self.removeAnimate()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cambieTarjeta"), object: nil)
            }
            else{
                print("Error: la tarjeta no fue borrada")
            }
        }
        
        
    }
    
    
    func fillTexts(){
        lblTitulo.text = NSLocalizedString("eliminar_tarjeta", comment: "")
        btnCancelar.setTitle(NSLocalizedString("cancelar",comment: ""), for: .normal)
        btnEliminar.setTitle(NSLocalizedString("eliminar",comment: ""), for: .normal)
    }
    //----FIN
}
