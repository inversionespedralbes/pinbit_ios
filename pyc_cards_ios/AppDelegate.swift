
//
//  AppDelegate.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 22/05/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GGLInstanceIDDelegate, GCMReceiverDelegate{
    
    var window: UIWindow?
    var shouldSelectViewController: Bool?
    //variables GCM
    var connectedToGCM = false
    var subscribedToTopic = false
    var gcmSenderID: String?
    var registrationToken: String?
    var registrationOptions = [String: AnyObject]()
    let registrationKey = "onRegistrationCompleted"
    let messageKey = "onMessageReceived"
    let subscriptionTopic = "/topics/global"
    var abrirRegistro = false
    var hayNotificacion = false
    
    let googleMapsApiKey = "AIzaSyBYcy-sVXtB17M3SAcVv5IGezjPGW7vrUU"
    
    var app:UIApplication?
    var timer = Timer()
    
    func applicationDidFinishLaunching(_ application: UIApplication) {
        // Override point for customization after application launch.
        self.app = application
         
        
        //Google api
        GMSServices.provideAPIKey(googleMapsApiKey)
        
        //Integración con zopim (Chat)
        //ZDCLog.enable(true)
        //ZDCLog.setLogLevel(ZDCLogLevel.warn)
        
        
        //Configuracion del GCM
        /// [START_EXCLUDE]
        // Configure the Google context: parses the GoogleService-Info.plist, and initializes
        // the services that have entries in the file
        var configureError:NSError?
//        GMRConfiguration.sharedInstance().setIsEnabled(false)
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        gcmSenderID = GGLContext.sharedInstance().configuration.gcmSenderID
        // [END_EXCLUDE]
        configGCM()
        //return true
    }
  
    
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        print("entra a open url\n")
    
        print("URL Fragment ===> \(url.fragment!)")
        
        UserDefaults.standard.set(url.fragment!, forKey: "dataApp")
        
        return true
    }
    //........
    //......
    //      CONFIGURACIÓN Y CONEXION CON GCM
    //....
    //......
    
    func configGCM(){
        print("configGCM\n")
        if(UserDefaults.standard.object(forKey: "primera_vez")==nil){
            // Register for remote notifications
            
            
            if #available(iOS 8.0, *) {
                                    let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                                    self.app!.registerUserNotificationSettings(settings)
                                    self.app!.registerForRemoteNotifications()
                                } else {
                                    // Fallback
                                    let types: UIRemoteNotificationType = [.alert, .badge, .sound]
                                    self.app!.registerForRemoteNotifications(matching: types)
                                }

            // [END register_for_remote_notifications]
            // [START start_gcm_service]
            let gcmConfig = GCMConfig.default()
            gcmConfig?.receiverDelegate = self
            GCMService.sharedInstance().start(with: gcmConfig)
            // [END start_gcm_service]
            
        }
    }
    
    //Cuando el self.app!.registerForRemoteNotifications() es correcto llega a esta funcion
    // [START receive_apns_token]
    func application( _ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken
        deviceToken: Data ) {
        print("didRegisterForRemoteNotificationsWithDeviceToken \n")
        // [END receive_apns_token]
        // [START get_gcm_reg_token]
        // Create a config and set a delegate that implements the GGLInstaceIDDelegate protocol.
        let instanceIDConfig = GGLInstanceIDConfig.default()
        instanceIDConfig?.delegate = self
        // Start the GGLInstanceID shared instance with that config and request a registration
        // token to enable reception of notifications
        GGLInstanceID.sharedInstance().start(with: instanceIDConfig)
        registrationOptions = [kGGLInstanceIDRegisterAPNSOption:deviceToken as AnyObject,
                               kGGLInstanceIDAPNSServerTypeSandboxOption:true as AnyObject]
    
        GGLInstanceID.sharedInstance().token(withAuthorizedEntity: gcmSenderID,
                                                                 scope: kGGLInstanceIDScopeGCM, options: registrationOptions, handler: registrationHandler)
        // [END get_gcm_reg_token]
        
    }
    
    //Cuando self.app!.registerForRemoteNotifications() es error muestra un error...
    // [ERROR receive_apns_token]
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print("Error al registrar en APN: \(error.code)\n")
        print("Error APN: \(error.description)\n")
    }
    
    
    func registrationHandler(_ registrationToken: String?, error: Error?) {
        if (registrationToken != nil) {
            self.registrationToken = registrationToken
            print("Registration GCM Token: \(registrationToken)")
            
            let userInfo = ["registrationToken": registrationToken]  as [String: Any]
            //guarda en variables de usuario el Device_id.....
            UserDefaults.standard.set(registrationToken, forKey: "device_id")
            
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: self.registrationKey), object: nil, userInfo: userInfo)
            
            //NotificationCenter.default.postNotificationName(
                //self.registrationKey, object: nil, userInfo: userInfo)
            
            //envia al usuario al loggin....
            print("\n guarda el idGCM..")
            
            
            GCMService.sharedInstance().connect(handler: {
                (error) -> Void in
                if error != nil {
                    print("Could not connect to GCM: descripcion: \(error?.localizedDescription) \n")
                    
                }
                
            })
            
            let viewController = self.window!.rootViewController!
            //viewController.performSegueWithIdentifier("loggin", sender: nil)
            
            if(UserDefaults.standard.object(forKey: "primera_vez") == nil){
                if(!abrirRegistro){
                    abrirRegistro = true
                    print("\n primera vez : mostrar registro....")
                    viewController.performSegue(withIdentifier: "registro", sender: nil)
                }
                else{
                    print("intenta abrir otra pantalla de registro")
                }
                
            }
            else{
                print("\n LLama al Loggin....")
                viewController.performSegue(withIdentifier: "loggin", sender: nil)
            }
            
            
        } else {
            print("\n Registration to GCM failed with error: \(error?.localizedDescription) \n ")
            let userInfo = ["error": error?.localizedDescription] as [String: Any]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: self.registrationKey), object: nil, userInfo: userInfo)
//            NotificationCenter.default.postNotificationName(
//                self.registrationKey, object: nil, userInfo: userInfo)
            //intenta iniciar todo el proceso de configuracion y registro del GCM
            self.configGCM()
        }
    }
    
    // [START on_token_refresh] necesario para cumplir protocolo GCM
    func onTokenRefresh() {
        // A rotation of the registration tokens is happening, so the app needs to request a new token.
        print("The GCM registration token needs to be changed.")
        GGLInstanceID.sharedInstance().token(withAuthorizedEntity: gcmSenderID,
                                                                 scope: kGGLInstanceIDScopeGCM, options: registrationOptions, handler: registrationHandler)
    }
    // [END on_token_refresh]
    
    
    //........
    //......
    //      PROCESANDO RECEPCION DE NOTIFICACIONES
    //....
    //......
    
    //recibir notificaciones.....



    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        NSLog("Notification received 55: %@", userInfo)
        // This works only if the app started the GCM service
        GCMService.sharedInstance().appDidReceiveMessage(userInfo);
                let json: JSON = JSON(userInfo)
                procesandoNotificacion(datosGCM: json)

    }
    
    func application(_ application: UIApplication,
                              didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                              fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Notification received: \(userInfo)")
                        // This works only if the app started the GCM service
                        GCMService.sharedInstance().appDidReceiveMessage(userInfo);
                        // Handle the received messag
                        // ...
        
                        let json: JSON = JSON(userInfo)
                        procesandoNotificacion(datosGCM: json)
        
        completionHandler(UIBackgroundFetchResult.noData)
    }
    
    

    
    func procesandoNotificacion(datosGCM: JSON){
        
        if(!self.hayNotificacion){
            self.hayNotificacion = true
            let view = self.window!.rootViewController?.presentedViewController // traer el view actual...
            print("view actual: \(self.window!.rootViewController)")
            
            print("datos notificación GCM: \(datosGCM)")
            
            let tipo = datosGCM["tipo"].stringValue
            
            if(tipo == "preregistro"){
                let deviceID = UserDefaults.standard.object(forKey: "device_id") as! String
                //let token = datosGCM["token"].stringValue
                let wallet_id = datosGCM["wallet_id"].stringValue
                let codigo = datosGCM["codigo"].stringValue
                let ref = datosGCM["pin_para_referente"].stringValue
                print("codigo ref GCM \(ref)\n")
                UserDefaults.standard.set(ref, forKey: "codigoRef")
                
                let url = urlPinbit + "/activacion_usuarios/\(wallet_id).json"
                print("url activacion_usuarios: \(url)")
                
                let parameters = [
                    "codigo": codigo,
                    "device_id": deviceID,
                    "sistema_operativo": "IOS"]
                
                request(url, method: .put, parameters: parameters).responseJSON{
                    response in
                    print("Respuesta del servidor activacion_usuarios: \(response.description)\n")
                    switch response.result {
                    case .success(let data):
                        if(response.response?.statusCode == 201)
                        {
                            print("token: \(data) \n")
                            let json = JSON(data)
                            
                            UserDefaults.standard.set(false, forKey: "primera_vez")
                            resgistroCorrecto = true
                            UserDefaults.standard.set(json["token"].stringValue, forKey: "token")
                            
                            UserDefaults.standard.set(true, forKey: "sinTarjeta")
                            //view!.dismiss(animated: true, completion: nil)
                            view!.performSegue(withIdentifier: "perfilRegistro", sender: nil)
                            
                            //cerrarRegistro
                            
                            
                        }
                        else
                        {
                            print(response.result)
                            //Error de respuesta servidor
                            view!.dismiss(animated: true, completion: nil)
                            view!.view.makeToast(NSLocalizedString("cuenta_no_confirmada", comment: ""))
                            
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
                
            }

        }
        
        
    }
    
    //........
    //......
    //          FUNCIONES NECESARIAS PARA ENVIAR MENSAJES...
    //       se necesitan para cumplir con el protocolo de GCM....
    //....
    //......
    
    // [START upstream_callbacks]
    func willSendDataMessageWithID(messageID: String!, error: NSError!) {
        if (error != nil) {
            // Failed to send the message.
        } else {
            // Will send message, you can save the messageID to track the message
        }
    }
    
    func didSendDataMessageWithID(messageID: String!) {
        // Did successfully send message identified by messageID
    }
    // [END upstream_callbacks]
    
    func didDeleteMessagesOnServer() {
        // Some messages sent to this device were deleted on the GCM server before reception, likely
        // because the TTL expired. The client should notify the app server of this, so that the app
        // server can resend those messages.
    }
    
    
    
    //........
    //......
    //     ESTADOS DE LA APP
    //....
    //......
    func applicationDidBecomeActive( _ application: UIApplication) {
        
        print("App activa...!!!")
        let enSesion = UserDefaults.standard.bool(forKey: "enSesion")
        if(enSesion){
            print("reiniciar tiempo")
            self.timer.invalidate()
        }
        
        // [START connect_gcm_service]
        // Connect to the GCM server to receive non-APNS notifications
        GCMService.sharedInstance().connect(handler: {
            (error) -> Void in
            if error != nil {
                print("Could not connect to GCM: descripcion: \(error?.localizedDescription) \n")
                
                //                if(error==-1009){
                //                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "sinGCM"), object: nil)
                //                }
                //
            }
        })

        
        
        // [END connect_gcm_service]
    }
    

    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("applicationDidEnterBackground \n")
        // [START disconnect_gcm_service]
        GCMService.sharedInstance().disconnect()
        // [START_EXCLUDE]
        self.connectedToGCM = false
        // [END_EXCLUDE]
        // [END disconnect_gcm_service]
        
        
        if(UserDefaults.standard.object(forKey: "device_id") == nil){
            print("cierra app...")
            exit(0)
        }
        
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        //detecta cuando la aplicaión pasa de esta activo a inactivo.....
        print("salio")
        let enSesion = UserDefaults.standard.bool(forKey: "enSesion")
        if(enSesion){
            print("guardar tiempo")
            self.timer = Timer.scheduledTimer(
                timeInterval: 180, target: self, selector: #selector(AppDelegate.cerrarAplicacion), userInfo: nil, repeats: false)
        }
    }
    
    
    //Funciones para cerrar sesion en el servidor
    func cerrarAplicacion(){
        print("después de 180 seg cerrarAplicacion  \(self.timer) \n")
        //exit(0)
        cerrarSesion()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.window!.rootViewController = storyboard.instantiateInitialViewController()
        
    }
    
    func cerrarSesion(){
        let token = UserDefaults.standard.object(forKey: "token") as! String
        let customAllowedSet =  NSCharacterSet(charactersIn:"=\"#%/<>?@\\^`{|}+").inverted
        
        let escapedString = token.addingPercentEncoding(withAllowedCharacters: customAllowedSet)
        let url = urlPinbit + "/cerrar_sesion.json?authenticity_token=" + escapedString!
        request(url, method: .delete)
            .responseJSON {  response in
                print( "Respuesta cerrar sesion: \(response.response?.statusCode)\n")
                switch response.result {
                case .success(_):
                    if(response.response?.statusCode == 200)
                    {
                        print( "sesión eliminada")
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cerrarSesion"), object: nil)
                        UserDefaults.standard.set(false, forKey: "enSesion")
                    }
                    else
                    {
                        //Error de respuesta servidor
                    }
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        print("App entra en foreground\n")
    }
    
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        print("App finalizada\n")
    }
    
    
    //........
    //......
    //      ADMINISTRACION BASE DE DATOS
    //....
    //......
    
    // MARK: - Core Data stack NECESARIO PARA EL USO DE BASE D DATOS......
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "PinBit.Core_Data_2" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "BD-PinBit", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as! NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
}
