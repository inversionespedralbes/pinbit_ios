//
//  PagosAdapatador.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 10/06/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class PagosAdapatador: UITableViewCell {
    
    
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var imagen: UIImageView!
    @IBOutlet weak var separador: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
