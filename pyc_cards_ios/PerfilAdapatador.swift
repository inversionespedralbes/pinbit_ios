//
//  PerfilAdapatador.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 30/06/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class PerfilAdapatador: UITableViewCell {

    @IBOutlet weak var vistaPerfil: UIView!
    @IBOutlet weak var lblPass: UILabel!
    @IBOutlet weak var lblContraseña: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        vistaPerfil.layer.masksToBounds = false
        vistaPerfil.layer.cornerRadius = 3
        
        lblContraseña.text = NSLocalizedString("contrasena", comment: "")
        lblPass.text = "••••••••••"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //Notifica a la aplicacion que el ususario quiere cambiar pass
    @IBAction func btnCambiarPass(_ sender: AnyObject) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cambiarPass"), object: nil)
        //NotificationCenter.default.post(name: Notification.Name(rawValue: "cambiarPass"), object: nil)
    }
    
  

}
