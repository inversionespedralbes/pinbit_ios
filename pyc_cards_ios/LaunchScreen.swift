//
//  LaunchScreen.swift
//  pyc_cards_ios
//
//  Created by deisy melo on 6/10/15.
//  Copyright © 2015 PinBit. All rights reserved.
//

import UIKit

class LaunchScreen: UIView {

    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        CAGradientLayer().fondoDegrade(self.view, color1: CAGradientLayer().UIColorFromHex(0x0092f1, alpha: 1), color2: CAGradientLayer().UIColorFromHex(0x005eab, alpha: 1))
        self.view.layoutSubviews()
    }


}
