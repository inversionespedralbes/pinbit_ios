        //
//  MovimientosController.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 9/06/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit
import Alamofire
import CoreData



class MovimientosController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIDocumentInteractionControllerDelegate {
    
    //Variables de interfaz
    @IBOutlet weak var txtFecha: UITextField!
    @IBOutlet weak var tablaMovimientos: UITableView!
    @IBOutlet weak var btnCalendar: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var viewHeader: HeaderController!
    
    @IBOutlet weak var lblTitulo: UILabel!
    //Variables logicas locales
    var listaMovimientos: [Movimiento] = []
    var fechaMovimientos: NSDate!
    var dia: String!
    var mes: String!
    var anio: String!
    var listaVacia: UILabel!
    var popDatePicker : FechaPicker!
    var mapa: MovimientoDetalleController!
    var menu: UIDocumentInteractionController!
    var dialogoCambiarTarjeta: DialogoCambiarTarjetasController!
    var dialogoInfo: DialogoSimpleController!
    
    //Gestos swipe
    var swipeLeft: UISwipeGestureRecognizer!
    var swipeRight: UISwipeGestureRecognizer!
    
    //variables necesarias para la utilización de CoreData
    var appDel:AppDelegate!
    var context:NSManagedObjectContext!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // llama el objeto AppDelegate para poder utilizar las funciones que allí se encuentran
        appDel = UIApplication.shared.delegate as! AppDelegate
        //se obtiene el contexto: Es la interfaz para acceder a la base de datos...
        context = appDel.managedObjectContext
        
        spinner.startAnimating()
        spinner.isHidden = false
        txtFecha.delegate = self
        self.fechaMovimientos = NSDate()
        txtFecha.text = fechaMovimientos.ToDateMediumString() as String!
        fechaMov()
        popDatePicker = FechaPicker(forTextField: txtFecha)
        self.initTexts()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tablaMovimientos.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //Funciones para el manejo de la tabla
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaMovimientos.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MovimientosAdapatador
        
        cell.lblMovimiento.text = self.listaMovimientos[indexPath.row].descripcion
        cell.lblFecha.text = self.listaMovimientos[indexPath.row].fecha
        cell.lblValor.text = "$ " +  (self.listaMovimientos[indexPath.row].valor)
        
        if(listaMovimientos[indexPath.row].latitud_pago == 0 && listaMovimientos[indexPath.row].longitud_pago == 0)
            
        {
            cell.imagen.image = (UIImage(named: "LocationG.png"))
        }
        else
        {
            cell.imagen.image = (UIImage(named: "LocationB.png"))
        }
        cell.layer.insertSublayer(CAGradientLayer().fondoGris(cell: cell), at:0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(listaMovimientos[indexPath.row].latitud_pago != 0 && listaMovimientos[indexPath.row].longitud_pago != 0)
        {
            print("intenta mostrar el mapa...\n")
            self.mapa = MovimientoDetalleController(nibName: "Mapa", bundle: nil)
            self.mapa.showInView(aView: self.view, latitud: listaMovimientos[indexPath.row].latitud_pago  , longitud: listaMovimientos[indexPath.row].longitud_pago, latitud_cobro:listaMovimientos[indexPath.row].latitud_cobro  , longitud_cobro: listaMovimientos[indexPath.row].longitud_cobro, tipo: listaMovimientos[indexPath.row].descripcion, animated: true)
        }
    }
    
    //Carga los movimientos de una fecha especifica
    func cargarMovimientos(){
        
        print("\n FILTRO POR FECHA... \n")
        
        self.listaMovimientos.removeAll(keepingCapacity: false)
        self.tablaMovimientos.isHidden = true
        self.spinner.isHidden = false
        self.spinner.startAnimating()
        self.listaMovimientos.removeAll(keepingCapacity: false)
        
        var fecha = "\(self.anio!)-\(self.mes!)-\(self.dia!)"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd"
        let fechaFormat = dateFormatter.date(from: fecha)
        
        fecha = dateFormatter.string(from: fechaFormat!)
        
        print("fecha: \(fecha)")
                //traer los datos de la tarjeta desde la bd para verlos en la lista de pagos...
        let lista = NSFetchRequest<NSFetchRequestResult>(entityName: "Movimiento")
        
        //Se consulta especificamente la fecha que queremos
        
        lista.predicate = NSPredicate(format: "fecha_pago CONTAINS[cd] %@", fecha)
        
        //recorrer el resultado de la consulta....
        let consultaPagosFecha = try? context.fetch(lista)
        
        print("consultaPagosFecha: \(consultaPagosFecha!)")
        
        var dic = [[String:String]]()
        var jsonPagos:JSON
        //recorriendo la consulta
        
        
        for movimientoBD in consultaPagosFecha as! [NSManagedObject] {
            let jsonP = [
                "establecimiento" : (movimientoBD.value(forKey: "establecimiento")as! String),
                "fecha" : (movimientoBD.value(forKey: "fecha_pago")as! String),
                "latitud_cobro" : (movimientoBD.value(forKey: "latitud_cobro")as! String),
                "longitud_cobro" : (movimientoBD.value(forKey: "longitud_cobro")as! String),
                "longitud_pago" : (movimientoBD.value(forKey: "longitud_pago")as! String),
                "latitud_pago" : (movimientoBD.value(forKey: "latitud_pago")as! String),
                "valor" : (movimientoBD.value(forKey: "valor") as! String)
            ] as [String : Any]
            
            dic.append(jsonP as! [String : String])
        }
        
        jsonPagos = JSON(dic)
        print("carga movimientos pagos: \(jsonPagos)\n")
        self.procesarListaPagos(json: jsonPagos, TAG: "base_datos")
        
    }
    
    //Carga los 10 ultimos movimientos
    func cargarMovimientosUltimos(){
        print("\n ULTIMOS 10 MOVIMIENTOS... \n")
        
        self.listaMovimientos.removeAll(keepingCapacity: false)
        
        //traer los pagos de la bd...
        let consulta = NSFetchRequest<NSFetchRequestResult>(entityName: "Movimiento")
        
        let sortDescriptor = NSSortDescriptor(key: "fecha_pago", ascending: false)
        consulta.sortDescriptors = [sortDescriptor]
        
        //recorrer el resultado de la consulta
        let res = try? context.fetch(consulta)
        
        if res!.isEmpty {
            
            let deviceID = UserDefaults.standard.object(forKey: "device_id") as! String
                var url = "/pagos.json?device_id=\(deviceID)&sistema_operativo=iOS"
                    url = urlPinbit + url
            
            print("Url : \(url) \n")
            
            request(url, method: .get).responseJSON{
                response in
                print("Consulta server lista movimientos: \(response.response?.statusCode)\n")
                switch response.result {
                case .success(let data):
                    print("Lista movientos: \(data)\n")
                    if(response.response?.statusCode == 200)
                    {
                        let json = JSON(data)
                        print("pagos web: \(JSON(json["pagos"].arrayValue))\n")
                        self.procesarListaPagos(json: JSON(json["pagos"].arrayValue), TAG: "servidor")
                        
                    }
                    else
                    {
                        print("error listar pagos... \n")//Error de respuesta servidor
                        self.addEmptyLabel()
                    }
                    
                case .failure(let error):
                    print("fallo consulta loista pagos: \(error)\n")
                    self.addEmptyLabel()
                }

            }
            
        
        }
        else{
            print("\n BUSCAR PAGOS EN BD \n")
            
            var dic = [[String:String]]()
            var jsonPagos:JSON
            //recorriendo la consulta
            
            for movimientoBD:AnyObject in res as! [NSManagedObject] {
                let jsonP = [
                    "establecimiento" : (movimientoBD.value(forKey: "establecimiento")! as? String)!,
                    "fecha" : (movimientoBD.value(forKey: "fecha_pago")! as? String)!,
                    "latitud_cobro" : (movimientoBD.value(forKey: "latitud_cobro")! as? String)!,
                    "longitud_cobro" : (movimientoBD.value(forKey: "longitud_cobro")! as? String)!,
                    "longitud_pago" : (movimientoBD.value(forKey: "longitud_pago")! as? String)!,
                    "latitud_pago" : (movimientoBD.value(forKey: "latitud_pago")! as? String)!,
                     "valor" : (movimientoBD.value(forKey: "valor") as! String)
                ] as [String : Any]
                
                dic.append(jsonP as! [String : String])
            }
            
            jsonPagos = JSON(dic)
            print("Pagos BD: \(jsonPagos)\n")
            self.procesarListaPagos(json: jsonPagos, TAG: "base_datos")
        }
        
        
    }
    
    func procesarListaPagos(json: JSON, TAG: String){
        print("procesarListaPagos \n")
        let movimientos:[JSON] = json.arrayValue
        print("moviemientos: \(movimientos) \n")
        var mov: Movimiento
        for movimiento in movimientos{
            var lat_pago = "0"
            var long_pago = "0"
            var lat_cobro = "0"
            var long_cobro = "0"
            
            if movimiento["latitud_pago"] != "0.0" && movimiento["longitud_pago"] != "0.0"
            {
                lat_pago = movimiento["latitud_pago"].stringValue
                long_pago = movimiento["longitud_pago"].stringValue
                lat_cobro = movimiento["latitud_cobro"].stringValue
                long_cobro = movimiento["longitud_cobro"].stringValue
            }
            
            
            var tarjetaPago = ""
            
            if(!movimiento["alias"].stringValue.isEmpty){
                tarjetaPago = movimiento["alias"].stringValue
            }
            else{
                tarjetaPago = movimiento["mascara_tarjeta"].stringValue
            }
            
            print("valor mov: \(self.formatValor(saldo: movimiento["valor"].stringValue))")
            
            mov = Movimiento(valor: self.formatValor(saldo: movimiento["valor"].stringValue),
                             longitud_pago: long_pago, latitud_pago: lat_pago, longitud_cobro: long_cobro,
                             latitud_cobro: lat_cobro, descripcion: movimiento["establecimiento"].stringValue,
                             fecha: self.formatDate(fecha: movimiento["fecha"].stringValue))
            
           
            
            self.listaMovimientos.append(mov)
            
            if(TAG == "servidor"){
                //se crea una nueva tarjeta
                let formatDate = self.formatDate(fecha: movimiento["fecha"].stringValue)
                
                let movimientoNuevo: AnyObject = NSEntityDescription.insertNewObject(forEntityName: "Movimiento", into: context)
                //Insertar Datos
                movimientoNuevo.setValue(movimiento["establecimiento"].stringValue, forKey: "establecimiento")
                movimientoNuevo.setValue(formatDate, forKey: "fecha_pago")
                movimientoNuevo.setValue(movimiento["latitud_cobro"].stringValue, forKey: "latitud_cobro")
                movimientoNuevo.setValue(movimiento["longitud_cobro"].stringValue, forKey: "longitud_cobro")
                movimientoNuevo.setValue(movimiento["latitud_pago"].stringValue, forKey: "latitud_pago")
                movimientoNuevo.setValue(movimiento["longitud_pago"].stringValue, forKey: "longitud_pago")
                movimientoNuevo.setValue(movimiento["valor"].stringValue, forKey: "valor")
                
                //datos tarjeta...
                //movimientoNuevo.setValue(movimiento["tarjeta"].stringValue, forKey: "id_tarjeta")
                //movimientoNuevo.setValue(movimiento["alias"].stringValue, forKey: "alias_tarjeta")
                //movimientoNuevo.setValue(movimiento["numero_enmascarado"].stringValue, forKey: "mascara_tarjeta")
                
                //guardar arjeta en la bd
                try! context.save()
            }
        }
        
        self.addEmptyLabel()
        DispatchQueue.main.async(execute: {
            self.animateTable()
            self.tablaMovimientos.isHidden = false
            self.spinner.stopAnimating()
            self.spinner.isHidden = true
            return
        })
        
    }
    
    //Poner mensaje lista vacia
    func addEmptyLabel()
    {
        print("lista moviemintos: \(self.listaMovimientos)\n")
        if(self.listaMovimientos.count == 0)
        {
            print("sin movimientos")
            self.listaVacia = UILabel()
            self.listaVacia.frame = CGRect(x:0, y:0, width:self.view.bounds.size.width, height: self.view.bounds.size.height)
            self.listaVacia.text = NSLocalizedString("no_hay_movimientos", comment: "")
            self.listaVacia.textAlignment = .center
            //            self.listaVacia.font = UIFont(name: "Palatino-Italic", size: 20)
            //            self.listaVacia.sizeToFit()
            self.tablaMovimientos.backgroundView = self.listaVacia
            self.tablaMovimientos.separatorStyle = UITableViewCellSeparatorStyle.singleLine
            self.tablaMovimientos.separatorColor = UIColor.white
        }
        else
        {
            self.tablaMovimientos.backgroundView = nil
        }
        
        self.tablaMovimientos.isHidden = false
        self.spinner.stopAnimating()
        self.spinner.isHidden = true
    }
    
    //Decimales para los valores de los movimientos
    func formatValor(saldo: String) -> String{
        let sal = (saldo as NSString).doubleValue
        let fmt = NumberFormatter()
        fmt.numberStyle = .decimal
        
        return fmt.string(from: NSNumber(value: sal))! //fmt.stringFromNumber(NSNumber(sal))!
    }
    
    //Formato para fechas
    func formatDate(fecha: String) -> String {
        let parte1 = fecha.substring(to: fecha.index((fecha.characters.indices.min())!, offsetBy: 10)) + " "
        //let parte2 = fecha[fecha.startIndex.advancedBy(11)...fecha.startIndex.advancedBy(18)]
        let parte2 = fecha[fecha.index(fecha.startIndex, offsetBy: 11)...fecha.index(fecha.startIndex, offsetBy: 18)]
        return parte1 + parte2
    }
    
    //Fecha inicial
    func fechaMov()
    {
        _ = UserDefaults.standard.object(forKey: "device_id") as! String
        var calendar = NSCalendar.current
        let unitFlags = Set<Calendar.Component>([.hour, .minute, .month, .year, .day])
        let components = calendar.dateComponents(unitFlags, from: fechaMovimientos as Date)
        //let components = calendar.components([.Hour, .Minute, .Month, .Year, .Day], fromDate: fechaMovimientos)
        self.mes = String(describing: components.month!)
        self.anio = String(describing: components.year!)
        self.dia = String(describing: components.day!)
        
    }
    
    //Pop up de seleccion de fechas
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField === txtFecha) {
            txtFecha.resignFirstResponder()
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            formatter.timeStyle = .none
            let initDate = formatter.date(from: txtFecha.text!)
            
            popDatePicker!.pick(inViewController: self, initDate:initDate as NSDate?, dataChanged: { (newDate : NSDate, forTextField : UITextField) -> () in
                
                // here we don't use self (no retain cycle)
                forTextField.text = newDate.ToDateMediumString() as String!
                self.fechaMovimientos = newDate
                self.fechaMov()
                self.cargarMovimientos()
                
            })
            return false
        }
        else{
            return true
        }
    }
    
    //Refresh de movimientos cuando aparece la vista
    override func viewDidAppear(_ animated: Bool) {
        self.cargarMovimientosUltimos()
        
    }
    
    //Función que anima la tabla
    func animateTable() {
        tablaMovimientos.reloadData()
        
        let cells = tablaMovimientos.visibleCells
        let tableHeight: CGFloat = tablaMovimientos.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a
            UIView.animate(withDuration: 0.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
                }, completion: nil)
            
            index += 1
        }
    }
    
    //Funciones delegado de PDF viewer y sharing (UIDocumentInteractionControllerDelegate)
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerViewForPreview(_ controller: UIDocumentInteractionController) -> UIView? {
        return self.view
    }
    
    func documentInteractionControllerRectForPreview(_ controller: UIDocumentInteractionController) -> CGRect {
        return self.view.frame
    }
    
    func documentInteractionControllerDidDismissOptionsMenu(_ controller: UIDocumentInteractionController) {
    }
    
    //Manejador del gesto de swipe
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.left:
                print("Left")
                self.tabBarController?.selectedIndex = 1
            default:
                break
            }
        }
    }
    
    
    //Funcion iniciar los textos
    func initTexts(){
        //Crear boton chat
        //Mostrar botón chat
//        let halfSizeOfView = 20.0
//        let insetSize = self.view.bounds.insetBy(dx: CGFloat(Int(2 * halfSizeOfView)), dy: CGFloat(Int(2 * halfSizeOfView))).size
//        
//        let pointX = CGFloat((30))
//        let pointY = CGFloat((insetSize.height-20))
//        
//        let newView = ChatView(frame: CGRect(x: pointX, y: pointY, width: 40, height: 40))
//        self.view.addSubview(newView)
        
        lblTitulo.text = NSLocalizedString("movimientos", comment: "")
        
        //Textos
        //self.viewHeader.lblTitulo.text = NSLocalizedString("movimientos", comment: "")
    }
    
    @IBAction func btnAtras(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
}
