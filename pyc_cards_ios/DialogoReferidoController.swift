//
//  DialogoReferidoController.swift
//  pyc_cards_ios
//
//  Created by deisy melo on 1/09/16.
//  Copyright © 2016 PinBit. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class DialogoReferidoController: UIViewController, UITextFieldDelegate {
    
    
    //@IBOutlet var view: UIView!
    
    @IBOutlet weak var btnCancelar: UIButton!
    @IBOutlet weak var btnAceptar: UIButton!
    @IBOutlet weak var txtCodigo: UITextField!
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet var viewRef: UIView!
    
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.viewRef.layer.cornerRadius = 5
        self.viewRef.layer.shadowOpacity = 0.8
        self.viewRef.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        btnAceptar.layer.masksToBounds = true
        btnAceptar.layer.cornerRadius = 3
        btnAceptar.setTitle(NSLocalizedString("aceptar", comment: ""), for: UIControlState())
        txtCodigo.delegate = self
        load.isHidden = true
        
    }

    
    required override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func showInView(aView: UIView!, animated: Bool)
    {
        aView.addSubview(self.view)
        self.view.frame = aView.frame
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate( withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform( scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    @IBAction func agregar(_ sender: AnyObject) {
        load.isHidden = false
        load.startAnimating()
        
        let codigo = self.txtCodigo.text
        
        if(codigo != nil && codigo != "" ){
            let url = urlPinbit + "/referrals"
            let token = UserDefaults.standard.object(forKey: "token") as! String
            
            
            let parameters = [
                "authenticity_token": token,
                "referral": [
                    "pin": codigo!
                ]
            ] as [String : Any]
            
            print("datos enviados registro: \(parameters)")
            
            request( url,method: .post, parameters: parameters).responseJSON {
                response in
                print("Add Referido: \(response.response!.statusCode)\n")
                print("respuesta add Referidos: \(response.result) \n")
                
                switch response.result {
                case .success:
                    if(response.response!.statusCode == 200)
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addRefOK"), object: nil)
                        
                        self.removeAnimate()
                    }
                    else{
                        self.removeAnimate()
                    }
                case .failure(let error):
                    print(error)
                }
                
            }
        }
        else{
            load.isHidden = true
            load.stopAnimating()
            
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: txtCodigo.center.x - 5, y: txtCodigo.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: txtCodigo.center.x + 5, y: txtCodigo.center.y))
            txtCodigo.layer.add(animation, forKey: "position")
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func cancelar(_ sender: AnyObject) {
        self.removeAnimate()
    }
    
}
