//
//  DialogoSimpleController.swift
//  PYC_mobile
//
//  Created by Sebastian Vivas on 4/05/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class FallaConexionController: UIView {
    
    @IBOutlet var view: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("FallaConexionView", owner: self, options: nil)
        self.addSubview(self.view)
    }
    
    func showInView(aView: UIView!, animated: Bool)
    {
        self.view.frame.origin.y = aView.bounds.height - 49
        aView.addSubview(self.view)
        self.view.frame = aView.frame
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    @IBAction func btnAction(_ sender: AnyObject) {
        removeAnimate()
    }
}
