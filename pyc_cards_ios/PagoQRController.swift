//
//  PagoQRController.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 16/06/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit
import AVFoundation

class PagoQRController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    
    var dialogoPagar: DialogoConsultarPinController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicializarLector()
    }
    
    
    func inicializarLector(){
        
        print("Inicia scan QR \n")
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
        // as the media type parameter.
        
        var captureDevice:AVCaptureDevice! = nil
                captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        do{
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            // Set the input device on the capture session.
            captureSession?.addInput(input as AVCaptureInput)
            
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            view.layer.addSublayer(videoPreviewLayer!)
            
            // Start video capture.
            captureSession?.startRunning()
            print("abre camara\n")
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            qrCodeFrameView?.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView?.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView!)
            view.bringSubview(toFront: qrCodeFrameView!)
            print("Scan QR View\n")
            //agrega el boton de cancelar a la camara...
            let btnCancel = UIButton(frame: CGRect(x: 0,y: 0,width: 100,height: 100))
            btnCancel.setTitle("Cancel", for: .normal)
            btnCancel.addTarget(self, action: #selector(PagoQRController.cerrarCamara(sender:)), for:.touchUpInside)
            self.view.addSubview(btnCancel)
            
        }
        catch {
            print(error)
            captureSession?.stopRunning()//detiene el scan del QR
            self.dismiss(animated: true, completion: nil)
            return
        }
    }
    
    //Decifra el codigo QR capturado...
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect(x: 0, y: 0, width: 0, height: 0)//CGRectZero
            print("No QR code is detected")
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObjectTypeQRCode {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
            qrCodeFrameView?.frame = barCodeObject.bounds;
            
            if metadataObj.stringValue != nil {
                print("CODIGO QR?: \(metadataObj.stringValue)")
                mostrarDialogoPagar(mensaje: metadataObj.stringValue)
                captureSession?.stopRunning()//detiene el scan del QR
            }
            
        }

    }
    
    
    func mostrarDialogoPagar(mensaje: String)
    {
        print("Voy a mostrar dialogo pagar", terminator: "")
        dialogoPagar = DialogoConsultarPinController(nibName: "DialogoConsultarPin", bundle: nil)
        dialogoPagar.showInViewQR(aView: self.view,codigo: mensaje, animated: true, controller: self)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(PagoQRController.cerroQR),
            name: NSNotification.Name(rawValue: "notificacionCierre"),
            object: nil)
    }
    
    @objc func cerroQR() {
        self.dismiss(animated: true, completion: { () -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        print("notificacion cerrarQR", terminator: "")
    }
    
    func cerrarCamara(sender: UIButton!){
        print("Cerrar Camara")
        self.dismiss(animated: true, completion: { () -> Void in
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.dismiss(animated: true, completion: nil)
    }
}
