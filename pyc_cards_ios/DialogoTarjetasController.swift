//
//  DialogoTarjetasController.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 25/06/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//


import UIKit
import Alamofire

class DialogoTarjetasController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var viewDialogo: UIView!
    @IBOutlet weak var tabla: UITableView!
    @IBOutlet weak var btnSeleccionar: UIButton!
    @IBOutlet weak var lblTitulo: UILabel!
    
    
    var selectedCell: NSIndexPath!
    var tarjetas: [Tarjeta] = []
    
    var dialogoCarga: DialogoCargaController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabla.register(UINib(nibName: "CellTarjeta", bundle: nil), forCellReuseIdentifier: "Cell")
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.viewDialogo.layer.cornerRadius = 5
        self.viewDialogo.layer.shadowOpacity = 0.8
        self.viewDialogo.layer.shadowOffset = CGSize(width: 0.0, height: 0.0) //CGSizeMake(0.0, 0.0)
        btnSeleccionar.layer.masksToBounds = true
        btnSeleccionar.layer.cornerRadius = 3.0
        
        self.fillTexts()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tarjetas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath) as! TarjetaAdaptador
        
        cell.layer.insertSublayer(CAGradientLayer().fondoGris(cell: cell), at:0)
        cell.lblTarjeta.text = tarjetas[indexPath.row].numero
        cell.accessoryType = UITableViewCellAccessoryType.none
        
        let imageView = UIImageView(image: UIImage(named: "checkW.png")!)
        imageView.frame = CGRect(x: 0, y: 0, width: 32, height: 32)//CGRectMake(0, 0, 32, 32)
        cell.accessoryView = imageView
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if(selectedCell != indexPath && selectedCell != nil)
        {
            tabla.cellForRow(at: selectedCell as IndexPath)!.accessoryType = UITableViewCellAccessoryType.none
            
            let imageView = UIImageView(image: UIImage(named: "checkW.png")!)
            imageView.frame = CGRect(x: 0,y:  0,width: 32,height: 32)
            tabla.cellForRow(at: selectedCell as IndexPath)!.accessoryView = imageView
            selectedCell = indexPath
        }
        
        selectedCell = indexPath
        tabla.cellForRow(at: indexPath as IndexPath)!.accessoryType = UITableViewCellAccessoryType.none
        
        let imageView = UIImageView(image: UIImage(named: "checkB.png")!)
        imageView.frame = CGRect(x: 0,y: 0,width: 32,height: 32)
        tabla.cellForRow(at: indexPath as IndexPath)!.accessoryView = imageView
    }
    
    required override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showInView(aView: UIView!, animated: Bool, tarjetas: [Tarjeta], dialogo: DialogoCargaController!)
    {
        self.tarjetas = tarjetas
        aView.addSubview(self.view)
        self.view.frame = aView.frame
        self.dialogoCarga = dialogo
        if animated
        {
            self.showAnimate()
        }
        tabla.reloadData()
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    @IBAction func btnSeleccionar(_ sender: AnyObject) {
        if(tabla.indexPathForSelectedRow?.row == nil)
        {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: self.viewDialogo.center.x - 5,y: viewDialogo.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: viewDialogo.center.x + 5,y: viewDialogo.center.y))
            viewDialogo.layer.add(animation, forKey: "position")
        }
        else
        {
            UserDefaults.standard.set(tarjetas[tabla.indexPathForSelectedRow!.row].numero, forKey: "numero_enmascarado")
            self.postTarjeta(idTarjeta: tarjetas[tabla.indexPathForSelectedRow!.row].id)
        }
    }
    
    func postTarjeta(idTarjeta: String)
    {
        
        let url = urlPinbit + "/seleccionar_tarjetas.json"
        print(url)
        print("\n idTarjeta: \(idTarjeta)")
        let token = UserDefaults.standard.object(forKey: "token") as! String
        let parameters = [
            "tarjeta": [
                "id":idTarjeta
            ], "authenticity_token":token ] as [String : Any]
        
        request(url,method: .post, parameters: parameters).responseJSON {
            response in
            print("Respuesta del servidor: \(response.response?.statusCode)")
            switch response.result {
            case .success(_):
                if(response.response?.statusCode == 200)
                {
                    self.removeAnimate()
                    
                    //se almacena la ultima tarjeta seleccionada
                    UserDefaults.standard.setValue(idTarjeta, forKey: "ultimaTarjeta")
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cerrarPerfil"), object: nil)
                    
                    
                    //                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    //                    let vc = storyboard.instantiateViewControllerWithIdentifier("Principal")
                    //                    self.presentViewController(vc, animated: true, completion: nil)
                    //vc.tabBarController?.selectedIndex = 0
                }
                else
                {
                    //Error de respuesta servidor
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func fillTexts(){
        self.btnSeleccionar.setTitle(NSLocalizedString("seleccionar", comment: ""), for: UIControlState())
        lblTitulo.text = NSLocalizedString("seleccionar_tarjeta", comment: "")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
