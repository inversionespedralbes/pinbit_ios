//
//  DialogoRegistroController.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 5/06/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit
import Alamofire

class DialogoCerrarSesionController: UIViewController {

    //Interfaz
    @IBOutlet weak var txtCodigo: UITextField!
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var viewDialogo: UIView!
    @IBOutlet weak var btnAceptar: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak var btnCancel: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.isHidden = true
        lblTitulo.text = NSLocalizedString("cerrar_sesion_titulo" , comment: "")
        btnAceptar.setTitle(NSLocalizedString("cerrar_sesion", comment: ""), for: UIControlState())
        btnCancel.setTitle(NSLocalizedString("cancelar", comment: ""), for: UIControlState())
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.viewDialogo.layer.cornerRadius = 5
        self.viewDialogo.layer.shadowOpacity = 0.8
        self.viewDialogo.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    required override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //Se encarga de mostrar el dialogo
    func showInView(aView: UIView!, animated: Bool)
    {
        aView.addSubview(self.view)
        self.view.frame = aView.frame
        if animated
        {
            self.showAnimate()
        }
    }
    
    //Animación del dialogo
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    //Animación de desaparición del dialogo
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    //Detecta los toques en la pantalla
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    //Mueve el teclado de la vista
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        txtCodigo.resignFirstResponder()
        return true
    }
    
    //Muestra el dialogo para pagar un pin

    
    @IBAction func btnAceptar(_ sender: AnyObject) {
        spinner.startAnimating()
        spinner.isHidden = false
        self.cerrarSesion()
    }

    
    //Funcion para cerrar sesion en el servidor
    func cerrarSesion(){
        if(InternetConnection.isConnectedToNetwork()){
            
            let token = UserDefaults.standard.object(forKey: "token")as! String
            let customAllowedSet =  NSCharacterSet(charactersIn:"=\"#%/<>?@\\^`{|}+").inverted
            let escapedString = token.addingPercentEncoding(withAllowedCharacters: customAllowedSet)
            let url = urlPinbit + "/cerrar_sesion.json?authenticity_token=" + escapedString!
            request(url, method: .delete)
                .responseJSON { response in
                    print( "Respuesta cerrar sesion: \(response.response?.statusCode)")
                    switch response.result {
                    case .success(_):
                        if(response.response?.statusCode == 200)
                        {
                            print( "sesión eliminada")
                            self.spinner.stopAnimating()
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cerrarSesion"), object: nil)
                            UserDefaults.standard.set(false, forKey: "enSesion")
                            self.removeAnimate()
                        }
                        else
                        {
                            //Error de respuesta servidor
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
        else{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cerrarSesion"), object: nil)
            //NotificationCenter.default.post(name: Notification.Name(rawValue: "cerrarSesion"), object: nil)
            UserDefaults.standard.set(false, forKey: "enSesion")
            //UserDefaults.standard.set(false, forKey: "enSesion")
            
        }
        
    }
    
    
    //Función de cancelar el dialogo
    @IBAction func btnCancelar(_ sender: AnyObject) {
        self.tabBarController?.selectedIndex = 0
        print("Hola", terminator: "")
        print("\(self.parent)", terminator: "")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "noSalir"), object: nil)
        //NotificationCenter.default.post(name: Notification.Name(rawValue: "noSalir"), object: nil)
        self.removeAnimate()
    }
    
    
    //----FIN
}
