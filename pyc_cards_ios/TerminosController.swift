//
//  TerminosController.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 22/05/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class TerminosController: UIViewController {
    
    @IBOutlet weak var scroller: UIScrollView!
    @IBOutlet weak var lblTerminos: UILabel!
    @IBOutlet weak var vistaDialogo: UIView!
    @IBOutlet weak var lblTitulo: UILabel!

    @IBOutlet weak var btnCerrar: UIButton!
    
    
    required override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        scroller.isScrollEnabled = true
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.vistaDialogo.layer.cornerRadius = 5
        self.vistaDialogo.layer.shadowOpacity = 0.8
        self.vistaDialogo.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.terminosCondiciones()
        
        self.vistaDialogo.isUserInteractionEnabled = true

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showInView(aView: UIView!, animated: Bool)
    {
        aView.addSubview(self.view)
        self.view.frame = aView.frame
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    //Init de textos
    func terminosCondiciones() {
        self.lblTitulo.text = NSLocalizedString("terminos_condiciones", comment: "")
        self.btnCerrar.setTitle(NSLocalizedString("cerrar",  comment: ""), for: UIControlState())
        //self.btnCancelar.setTitle(NSLocalizedString("cancelar",  comment: ""), forState: .Normal)
        self.lblTerminos.text = "Según lo dispuesto en la Ley de Comercio Electrónico de Colombia, mediante el diligenciamiento de las solicitudes de servicio y la aceptación electrónica de los presentes términos y condiciones, el USUARIO manifiesta su consentimiento y aceptación del contrato de prestación de servicios (en adelante, “el Contrato”) entre CENTRAL COMERCIALIZADORA DE INTERNET S.A.S., (en adelante, “CENTRAL”) y EL USUARIO para la prestación de los servicios ofrecidos (en adelante “los Servicios” o el “Servicio”) en la página web www.pinbit.co o en cualquier página o aplicación relacionada con el producto PINBIT, por sus aliados, proveedores o socios de negocios en las que aparezcan estos términos y condiciones (en adelante, “el Portal”)." +
        "\nLa aceptación electrónica del USUARIO implica que este ha leído, entiende, reconoce y está de acuerdo con los siguientes Términos y Condiciones:" +
        "\n1. TÉRMINOS Y CONDICIONES GENERALES DE LOS SERVICIOS" +
        "1.1. INFORMACIÓN DE CONTACTO DE CENTRAL. En cumplimiento de la Ley 1480 de 2011, CENTRAL COMERCIALIZADORA DE INTERNET S.A.S. informa que es una sociedad constituida bajo la República de Colombia, con número de identificación tributaria – NIT. 900.293.637-2, con domicilio en la ciudad de Bogotá, dirección de notificación judicial en la Calle 93b No 11a -84, Local 409, teléfono 6230622 y correo electrónico servicioalcliente@pinbit.co." +
        "\n1.2. CAPACIDAD. El USUARIO declara que es mayor de dieciocho (18) años de edad y que está plenamente facultado para obligarse en los términos de este Contrato. En caso de que quien solicite los Servicios de CENTRAL lo haga a nombre de una persona jurídica o de un tercero, dicha persona garantiza que está plenamente facultado para obligar a la persona jurídica o tercero según los términos y condiciones de este Contrato. En caso de que por solicitud del USUARIO se realicen cambios en la información de contacto asociada a algún servicio, el USUARIO declara que está facultado para obligar a quien resulte en la información de contacto según estos términos y condiciones. En caso de no estar facultado, el solicitante responderá personalmente por las obligaciones contraídas en este Contrato. CENTRAL se reserva el derecho de solicitar documentos o pruebas que considere necesarios para identificar a quien realiza las solicitudes, ante cualquier solicitud de parte de un USUARIO, tercero o por su propia iniciativa. El USUARIO se obliga a entregar a CENTRAL los documentos requeridos para comprobar su identidad, y el incumplimiento a este requerimiento será causal suficiente para que CENTRAL rechace cualquier solicitud de Servicio, modificación o alteración sobre los mismos." +
        "\n1.3. VERACIDAD DE LA INFORMACIÓN SUMINISTRADA POR EL USUARIO. EL USUARIO garantiza que toda la información que entregue a CENTRAL a través de cualquiera de los medios dispuestos para el envío de información, o cualquier información que se mantenga asociada a un Servicio prestado por CENTRAL es correcta y que los datos suministrados son ciertos. Así mismo, se compromete a actualizar la información que se encuentre en las bases de datos de CENTRAL a través de los medios dispuestos para ello, a más tardar 7 días después de que la misma se actualice y/o modifique. EL USUARIO acepta que los Servicios adquiridos podrán ser cancelados, terminados, transferidos, suspendidos, limitados o eliminados si CENTRAL determina que se ha suministrado información incorrecta, falsa o imprecisa en el proceso de solicitud del servicio o en cualquier otra comunicación relacionada con la prestación de los mismos." +
        "\n1.4. LEGITIMIDAD DE USO DE LOS SERVICIOS. El USUARIO garantiza que el uso de los servicios no infringe derechos de terceros y no viola ninguna ley local, nacional o internacional. En particular, el USUARIO declara y garantiza que el uso de los servicios no infringe ningún derecho de propiedad intelectual, derechos de imagen, contrato, acuerdo o convenio de terceros, y que los Servicios no serán usados en conexión con ninguna actividad ilegal." +
        "\n1.5. DURACIÓN: El Contrato que surge de la aceptación de estos Términos y Condiciones estará vigente desde el momento en que el USUARIO emita su aceptación electrónica y permanecerá vigente mientras exista cualquier obligación imputable al USUARIO sobre los Servicios contratados con CENTRAL." +
        "\n1.6. PETICIONES, QUEJAS Y RECLAMOS. En caso de tener cualquier petición, queja o reclamo en relación con los Servicios ofrecidos, EL USUARIO podrá radicarlas mediante correo electrónico enviado a la dirección servicioalcliente@pinbit.co, al cual también podrá acudir para saber el estado de su petición, queja o reclamo." +
        "\n1.7. LEGISLACIÓN APLICABLE. Salvo que se especifique lo contrario en alguna parte de los Términos y Condiciones, los Servicios ofrecidos por CENTRAL se encuentran regulados por las leyes de la República de Colombia y cualquier diferencia será resuelta por la jurisdicción ordinaria colombiana." +
        "\n1.8. SOLUCIÓN DE CONTROVERSIAS. Cualquier diferencia, disputa, reclamación o controversia relacionada con el Contrato o uso de los servicios serán regidas por las leyes de la República de Colombia, y dichas diferencias, disputas reclamaciones o controversias, serán tratadas y resueltas en la jurisdicción ordinaria de Colombia." +
        "\n1.9. RESPONSABILIDAD POR USO INDEBIDO DE DATOS DE ACCESO: CENTRAL no será responsable bajo ninguna circunstancia por el uso que den el USUARIO o terceros a los datos de acceso del USUARIO a cualquiera de los Servicios prestados bajo este Contrato. Es responsabilidad absoluta del USUARIO mantener los datos de acceso a su cuenta protegidos de terceros. Si bien CENTRAL hará sus mejores esfuerzos por ofrecer un sistema seguro de acceso a los Servicios, bajo ninguna circunstancia será responsable por cualquier modificación que realice el USUARIO o terceros que obtuvieren los datos de acceso con o sin autorización del USUARIO. EL USUARIO declara entender que quien cuenta con acceso a los datos de ingreso a los servicios, puede llegar a modificar los Servicios y los datos asociados a los mismos. Por tanto, el USUARIO se compromete a proteger con sus máximos esfuerzos los datos y la información de su(s) cuenta(s) con CENTRAL. El USUARIO se obliga, además, a responder por cualquier acción realizada sobre los Servicios o sus Cuentas como si dicho uso hubiere sido realizado directamente por el USUARIO, incluyendo, pero sin limitarse a la Aceptación de todos los términos y condiciones que puedan aplicar a los servicios utilizados. El USUARIO declara además tener la capacidad jurídica para realizar la anterior liberación de responsabilidades en nombre de quien represente." +
        "\n1.10. MODIFICACIONES A LOS TÉRMINOS Y CONDICIONES: EL USUARIO acepta que en cualquier momento del tiempo su relación con CENTRAL estará regida por los Términos y Condiciones que se encuentren publicados en el Portal o en las aplicaciones relacionadas al servicio. CENTRAL se reserva el derecho a modificar en cualquier momento los Términos y Condiciones. Si bien CENTRAL realizará sus mejores esfuerzos por mantener al USUARIO informado sobre cambios importantes sobre los Términos y Condiciones, es responsabilidad del USUARIO revisar periódicamente el Portal y estar enterado de los mismos. En caso de que el USUARIO guarde silencio sobre las nuevas modificaciones, se entenderá que acepta los cambios realizados. En caso de que un USUARIO no esté de acuerdo con modificaciones a los Términos y Condiciones podrá solicitar la cancelación de sus cuentas, siempre y cuando se pueda comprobar la identidad de quien realiza la cancelación. CENTRAL podrá notificar a los USUARIOS de las modificaciones a los Términos y Condiciones mediante correo electrónico para solicitar su consentimiento a los nuevos Términos y Condiciones en los casos en que se creen nuevas cargas u obligaciones para el USUARIO, o en cualquier caso que estime conveniente." +
        "\n1.11. CONDICIONES DE TERMINACIÓN. CENTRAL expresamente se reserva el derecho de denegar el servicio, terminar el Contrato, bloquear, suspender o modificar el acceso a los Servicios o transferir la titularidad de los Servicios por cualquiera las siguientes causales: a) Cumplir con órdenes y decisiones de entidades judiciales o administrativas nacionales e internacionales, de entidades reguladoras internacionales, páneles arbitrales o tribunales de resolución de conflictos de cualquier naturaleza b) Realizar o colaborar con investigaciones para determinar o prevenir fraude o abusos en contra de terceros c) Evitar responsabilidad civil o penal que se pudiera derivar para CENTRAL, sus funcionarios, socios, administradores, agentes o colaboradores por el contenido o uso de los Servicios d) Para corregir cualquier error o equivocación cometida por CENTRAL en la oferta o prestación de los Servicios e) En caso de que CENTRAL o cualquiera de sus Proveedores de Servicios, sus proveedores generales, o cualquier ente gubernamental así lo determinen. d) por incumplimiento de cualquiera de los Términos y Condiciones por parte del USUARIO. En caso de que cualquiera de estas situaciones se presentara, CENTRAL informará al Titular en los siguientes treinta (30) días del motivo de la denegación del servicio, terminación del Contrato, bloqueo, suspensión, modificación o transferencia de titularidad del Servicio." +
        "\n1.12. Se entienden integradas a este Contrato y aceptadas por el USUARIO todas las políticas gubernamentales que sean aplicables al uso de los servicios de CENTRAL y a las modificaciones que se hagan a éstas en el futuro." +
        "\n1.13. LIBERACIÓN DE RESPONSABILIDAD. Bajo ninguna circunstancia CENTRAL será responsable por la imposibilidad de procesar una transacción o por la suspensión, cancelación, supresión, interrupción, transferencia o no aceptación de cualquier solicitud realizada por EL USUARIO. El USUARIO acepta que CENTRAL no tendrá responsabilidad alguna por la imposibilidad del USUARIO de realizar un proceso a través de los sistemas de CENTRAL. Así mismo, acepta que los servicios ofrecidos por CENTRAL son de medio y no de resultado y por lo tanto absuelve de cualquier responsabilidad a CENTRAL y a sus Proveedores de Servicio por la imposibilidad de uso de los servicios, por las eventuales fallas en los sistemas de información, por cualquier eventualidad en la que CENTRAL o sus Proveedores de Servicio tengan o no responsabilidad, y por cualquier otra situación que pueda perjudicar al USUARIO." +
        "\n1.14. El USUARIO acepta toda la responsabilidad y exigibilidad que surja o sea asignada al USUARIO por el uso de los servicios de CENTRAL." +
        "\n2. INDEMNIDAD." +
        "\n2.1. EL USUARIO mantendrá indemne, defenderá, y mantendrá libre de toda responsabilidad a CENTRAL, a sus Proveedores de Servicios, a los Administradores de las extensiones, a los entes gubernamentales o relacionados y sus afiliados y subsidiarias, también como a sus propietarios, directivos, funcionarios, contratistas de y contra cualquier exigencia, daño, responsabilidad, costo o cualquier tipo de gasto incluyendo cargos legales razonables y otros (incluyendo apelaciones) que surjan de o se encuentren relacionados al uso de los servicios de CENTRAL." +
        "\n2.2. El USUARIO libera a Central de cualquier responsabilidad sobre eventos relacionados con la no disponibilidad de servicios, pérdidas de información, ataques de hackers, daños en el hardware o software asociado al servicio, errores en las aplicaciones, errores en la manipulación de los servicios de parte de CENTRAL o de cualquier otra persona, y en general de cualquier eventualidad relacionada con el servicio prestado." +
        "\n2.3. ACEPTACIÓN DE LOS TÉRMINOS Y CONDICIONES DE USO DE LOS TERCEROS O DE SERVICIOS CONTRATADOS. El USUARIO declara conocer y aceptar que si utiliza los servicios de CENTRAL para adquirir un producto o servicio, deberá revisar con el proveedor del producto y/o servicio y aceptar los Términos de Uso publicados y exigidos por cada uno de los terceros y/o proveedores." +
        "\n2.4. LIBERACION DE RESPONSABILIDAD SOBRE LOS SERVICIOS O PRODUCTOS. EL USUARIO declara entender que cualquier servicio o producto adquirido mediante los medios dispuestos por CENTRAL no tiene relación alguna con las obligaciones de CENTRAL. CENTRAL no será responsable por servicios o productos contratados a través de los servicios de CENTRAL" +
        "\n2.5.  El USUARIO entiende que si utiliza los servicios de CENTRAL para realizar cobros o recaudos, está sujeto a que dichos cobros o recaudos sean reversados por parte de instituciones financieras, pagadores, entidades gubernamentales, etc. El USUARIO acepta que en dado caso, cualquier valor reversado será imputable al USUARIO y autoriza a CENTRAL para realizar los trámites que sean necesarios para dicha reversión." +
        "\n3. POLÍTICAS DE PRIVACIDAD Y TRATAMIENTO DE DATOS Entrada en vigencia de la última versión: 9 de Enero de 2014. En cumplimiento de lo dispuesto en la Ley 1581 de 2012 y el Decreto 1377 de 2013, ENTRAL declara que se compromete a la siguiente Política de Privacidad, y el USUARIO autoriza y se obliga a la misma según corresponda:" +
        "\n3.1. FINALIDAD DE LA BASE DE DATOS. La base de datos tiene como finalidad principal lograr la satisfactoria prestación de los servicios que se ofrecen y la garantía del cumplimiento de las normas vigentes. Central se compromete a recolectar la información del USUARIO: a) para tramitar ante los diferentes sistemas, EL uso de los servicios provistos. B) para cumplir con los requerimientos de publicación de información de acuerdo a las políticas determinadas entes gubernamentales. c) para comunicar y notificar eventos relacionados con los servicios. d) para mejorar los servicios ofrecidos por Central a los USUARIOS." +
        "\n3.2. RESPONSABLE DEL TRATAMIENTO DE DATOS. Salvo que se indique lo contrario en algún aparte de los Términos y Condiciones, el responsable del tratamiento de datos personales y otra información del USUARIO es la sociedad CENTRAL COMERCIALIZADORA DE INTERNET S.A.S., identificada con NIT 900.293.637-2, domiciliada en la ciudad de Bogotá en la Cl 93B 11ª 84 LC 409, teléfono 6230622, correo electrónico servicioalcliente@pinbit.co Los receptores de la información serán Central, sus proveedores y aliados en la prestación de los servicios ofrecidos al USUARIO." +
        "\n3.3. Central entregará la información entregada por los Titulares: a) a las entidades relacionas con el procesamiento de los servicios, b) a sus aliados de negocios para el único fin de mejorar los servicios prestados a los Titulares, c) a cualquier autoridad que así lo demande según la jurisdicción que sea aplicable a los servicios de CENTRAL." +
        "\n3.4. RECOLECCIÓN DE DATOS PERSONALES Y OTRA INFORMACIÓN: El USUARIO expresamente autoriza a CENTRAL para recolectar datos personales y otra información del USUARIO en el momento en que EL USUARIO entrega explícita o implícitamente información a CENTRAL a través de los medios dispuestos para recibir solicitudes y pago de Servicios, durante el uso de los mismos Servicios. Así mismo, cuando el USUARIO envía mensajes, publica contenido, envía datos, transmite información o correos hacia CENTRAL o a través de los servicios ofrecidos por CENTRAL o los de sus aliados comerciales, el USUARIO autoriza a CENTRAL para que retenga la información necesaria para procesar sus solicitudes, responder sus dudas y mejorar sus servicios. De forma explícita, CENTRAL podrá pedir al USUARIO datos personales tales como su nombre, razón social, número de identificación, tarjetas de crédito y otras formas de pago, fecha de nacimiento, dirección de correspondencia, teléfono de contacto y correo electrónico entre otras. Adicionalmente, CENTRAL podrá obtener información sobre el computador del USUARIO y su navegador de Internet, tales como la dirección IP, datos sobre “cookies”, atributos de software y hardware, páginas solicitadas y otros parámetros del navegador. El USUARIO declara que ninguno de los datos que suministrará a CENTRAL califican como Datos Sensibles según lo dispuesto en la Ley 1581 de 2012." +
        "\n3.5. TRATAMIENTO DE LOS DATOS PERSONALES: CENTRAL solo usará, procesará y circulará los datos personales y otra información del USUARIO para los propósitos descritos y autorizados en esta Política de Privacidad o en las leyes vigentes. En adición a lo mencionado en otras cláusulas, el USUARIO expresamente autoriza a CENTRAL para la recolección, uso y circulación de sus datos personales y otra información para los siguientes propósitos y en las siguientes circunstancias: i) Comunicación entre CENTRAL y los USUARIOS. ii) Facilitar los medios de oferta y aceptación de Servicios, evitando que los USUARIOS deban entregar la misma información en repetidas ocasiones. iii) Proveer los servicios. iv) Auditar, estudiar y analizar la información recibida para mantener, proteger, aumentar y mejorar los Servicios ofrecidos por CENTRAL. v) Asegurar el funcionamiento de la plataforma y la red utilizada por CENTRAL en la prestación de los Servicios. vi) Proteger los derechos de propiedad intelectual de CENTRAL, los de terceros y los de los USUARIOS. vii) Desarrollar nuevos servicios viii) Combinar los datos personales con la información que se obtenga de otros aliados o compañías o enviarla a los mismos con el exclusivo propósito de proveer a nuestros clientes una mejor experiencia o mejorar la calidad de los Servicios. ix) Cuando se cuente autorización expresa y previa del USUARIO para revelar la información. x) Revelación de la información y datos personales a las sociedades subsidiarias, filiales o afiliadas a CENTRAL, o a sociedades o personas en las que CENTRAL confíe para que realicen el procesamiento de la información para el mejoramiento de los servicios o los procesos involucrados en el aprovisionamiento de los mismos. xi) Cuando la información deba ser revelada para cumplir con leyes, regulaciones o procesos legales, asegurar el cumplimiento de los términos y condiciones, para detener o prevenir fraudes, ataques a la seguridad de CENTRAL o de otros, prevenir problemas técnicos o proteger los derechos de otros como lo requieran los términos y condiciones o la ley. xii) Los demás descritos en los Términos y Condiciones. xiii) los demás que sean razonablemente necesarios para cumplir con las finalidades de la base de datos. Central acuerda no procesará los Datos Personales de una manera incompatible con los propósitos y demás limitaciones acerca de las cuales haya notificado en virtud de estos Términos y Condiciones." +
        "\n3.6. ALMACENAMIENTO Y PROTECCIÓN DE LOS DATOS PERSONALES Y OTRA INFORMACIÓN. El USUARIO autoriza expresamente a CENTRAL para almacenar sus datos personales y demás información de la forma que estime más conveniente y segura, la cual actualmente consiste en servidores en múltiples centros de datos que son propiedad y se encuentran bajo la administración de compañías aliadas a CENTRAL. Toda la información recolectada residirá en dicha red de servidores. Como medidas de protección de los datos personales y demás información del USUARIO, CENTRAL tomará las medidas que estén a su razonable alcance para evitar el acceso de personal no autorizado a la información de los USUARIOS. Para tales efectos, restringirá razonablemente el acceso a dicha información por parte de sus empleados, contratistas o agentes que necesiten acceso a la información y en caso de que sea necesario revelarla, cada persona que tenga acceso a la información estará obligada contractualmente a respetar la privacidad y confidencialidad de la información. El USUARIO acepta expresamente esta forma de protección y declara que la considera conveniente y suficiente para todos los propósitos." +
        "\n3.6. DERECHOS DE LOS USUARIOS EN RELACIÓN CON SUS DATOS PERSONALES: Son derechos del USUARIO conocer, consultar actualizar y rectificar sus datos personales. Para tales efectos, podrá utilizar los sistemas en línea dispuestos por CENTRAL para la actualización y modificación de datos. En particular, son derechos de los titulares según se establece en el artículo 8 de la Ley 1581 de 2012: a) Conocer, actualizar y rectificar sus datos personales b) Solicitar prueba de la autorización otorgada c) Ser informado, previa solicitud, respecto del uso que le ha dado a sus datos personales; d) Presentar ante la Superintendencia de Industria y Comercio quejas por infracciones a lo dispuesto en la ley e) Revocar la autorización y/o solicitar la supresión del dato f) Acceder en forma gratuita a sus datos personales que hayan sido objeto de Tratamiento." +
        "\n3.6.1.. PROCEDIMIENTO PARA EJERCER SUS DERECHOS. En caso de que desee ejercer sus derechos frente a CENTRAL, EL USUARIO deberá enviar un correo electrónico o físico a las direcciones de contacto establecidas en la presente Política de Privacidad. 3.6.2 ÁREA ENCARGADA DE PETICIONES, CONSULTAS Y RECLAMOS SOBRE DATOS PERSONALES. El área encargada de atender las peticiones, consultas y reclamos de los titulares para ejercer sus derechos a conocer, actualizar, rectificar y suprimir sus datos y revocar su autorización es el Área de Servicio al Cliente. 3.6.3. PETICIONES Y CONSULTAS SOBRE DATOS PERSONALES. Para el caso de las consultas, CENTRAL responderá la solicitud en plazo de máximo diez (10) días. En cumplimiento a lo dispuesto en la Ley 1581 de 2012, cuando no fuere posible atender la consulta dentro de dicho término, se informará al USUARIO, se le expresará los motivos de la demora y se le señalará la fecha en que se atenderá su consulta, la cual no podrá superar los cinco (5) días hábiles siguientes al vencimiento del primer término. 3.6.4. REVOCACIÓN DE AUTORIZACIÓN, RETIRO O SUPRESIÓN Y RECLAMOS SOBRE DATOS PERSONALES. El USUARIO que considere que la información contenida en las bases de datos de CENTRAL debe ser objeto de corrección, revocación, actualización o supresión, o cuando adviertan el presunto incumplimiento de cualquiera de los deberes contenidos en la Ley 1581 de 2012, podrán presentar un reclamo ante CENTRAL, el cual será tramitado bajo las siguientes reglas: 1. El reclamo se formulará mediante solicitud dirigida a CENTRAL con la identificación del USUARIO, el titular de los datos, la descripción de los hechos que dan lugar al reclamo, la dirección, y se anexarán los documentos que se quieran hacer valer. Si el reclamo resulta incompleto, CENTRAL podrá requerir al interesado dentro de los cinco (5) días siguientes a la recepción del reclamo para que subsane las fallas. Transcurridos dos (2) meses desde la fecha del requerimiento, sin que el solicitante presente la información requerida, se entenderá que ha desistido del reclamo. En caso de que CENTRAL no sea competente para resolver el reclamo, dará traslado a quien corresponda en un término máximo de dos (2) días hábiles e informará de la situación al USUARIO, con lo cual quedará relevada de cualquier reclamación o responsabilidad por el uso, rectificación o supresión de los datos. 2. Una vez recibido el reclamo completo, se incluirá en la base de datos una leyenda que diga \"reclamo en trámite\" y el motivo del mismo, en un término no mayor a dos (2) días hábiles. Dicha leyenda deberá mantenerse hasta que el reclamo sea decidido. 3. El término máximo para atender el reclamo será de quince (15) días hábiles contados a partir del día siguiente a la fecha de su recibo. Cuando no fuere posible atender el reclamo dentro de dicho término, se informará al USUARIO los motivos de la demora y la fecha en que se atenderá su reclamo, la cual en ningún caso podrá superar los ocho (8) días hábiles siguientes al vencimiento del primer término. El procedimiento descrito en esta cláusula junto con las demás condiciones establecidas en estos Términos y Condiciones constituyen el Manual Interno a que hace referencia el artículo 18 de la Ley 1581 de 2012. La solicitud de revocatoria o supresión no operarán respecto de datos sobre los cuales exista un deber contractual o legal de permanecer en la base de datos o registros de CENTRAL o de sus aliados." +
        "\n3.7. PERÍODO DE VIGENCIA DE LA BASE DE DATOS. Los datos personales incorporados en la Base de Datos estarán vigentes durante el plazo necesario para cumplir sus finalidades." +
        "\n3.8. CAMBIOS EN LA POLÍTICA DE PRIVACIDAD. Cualquier cambio sustancial en las políticas de Tratamiento, será comunicado oportunamente a los USUARIOS mediante la publicación en nuestros portales web. Para el caso de cambios sustanciales o que afecten a nuestros USUARIOS, comunicaremos dichos cambios a través de correo electrónico, en caso que CENTRAL cuente con este dato personal. " +
        "\n3.9. Las presentes políticas de privacidad junto con la Ley 1581 de 2012 y el Decreto 1377 de 2013 constituyen el Manual Interno de Políticas y Procedimientos de CENTRAL COMERCIALIZADORA DE INTERNET S.A.S."
    }
    
    
//    //Función botón aceptar
//    @IBAction func btnAcepto(sender: AnyObject) {
//        NSNotificationCenter.defaultCenter().postNotificationName("cerrarTerminos", object: nil)
//        self.removeAnimate()
//    }
    
    //Función botón cancelar
       
  
    @IBAction func CerrarAction(_ sender: AnyObject) {
        self.removeAnimate()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
