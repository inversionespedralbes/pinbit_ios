//
//  Global.swift
//  pyc_cards_ios
//
//  Created by deisy melo on 9/02/17.
//  Copyright © 2017 PinBit. All rights reserved.
//

import Foundation

private var  ip_Pinbit = "https://www.pinbit.co"
private var  ip_test = "https://test.pinbit.co"
private var  ip_boveda = "https://www.pin.com.co"
private var  ip_boveda_test = "https://test.pin.com.co"

public var urlPinbit = ip_Pinbit
public var urlBoveda = ip_boveda

