//
//  ListaTarjetasAdapatador.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 1/07/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class ListaTarjetasAdapatador: UITableViewCell {

    @IBOutlet weak var lblAlias: UILabel!
    @IBOutlet weak var lblNumeroEnmascarado: UILabel!
    @IBOutlet weak var btnEliminar: UIButton!
    @IBOutlet weak var btnCheck: UIButton!
    

    @IBOutlet weak var imgDelete: UIButton!
    
    var indice: String!
    var id: String!
    var alias: String!
    var numero: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func seleccionarTarjeta(_ sender: AnyObject) {
        print("btn seleccionarTarjeta \n")
        let selectedDateDictionary = ["indice" : self.indice, "id" : self.id, "alias":self.alias, "numero": self.numero]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "seleccionarTarjeta"), object: nil, userInfo:selectedDateDictionary)
    }
    
    @IBAction func btnEliminarTarjeta(_ sender: AnyObject) {
        print("btn eliminar Tarjeta adap\n")
        let selectedDateDictionary = ["indice" : self.indice, "id" : self.id, "alias":self.alias, "numero": self.numero]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "eliminarTarjeta"), object: nil, userInfo:selectedDateDictionary)
    }
    
}
