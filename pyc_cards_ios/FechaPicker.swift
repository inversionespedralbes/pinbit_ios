//
//  FechaPicker.swift
//  PYC_mobile
//
//  Created by Sebastian Vivas on 7/04/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

@available(iOS 8.0, *)
public class FechaPicker: NSObject , UIPopoverPresentationControllerDelegate, DataPickerViewControllerDelegate {
    
    public typealias PopDatePickerCallback = (_ newDate : NSDate, _ forTextField : UITextField)->()
    
    var datePickerVC : FechaController
    var popover : UIPopoverPresentationController?
    var textField : UITextField!
    var dataChanged : PopDatePickerCallback?
    var presented = false
    var offset : CGFloat = 8.0
    
    public init(forTextField: UITextField) {
        
        datePickerVC = FechaController()
        self.textField = forTextField
        super.init()
    }
    
    public func pick(inViewController : UIViewController, initDate : NSDate?, dataChanged : @escaping PopDatePickerCallback) {
        
        if presented {
            return  // we are busy
        }
        
        datePickerVC.delegate = self
        if datePickerVC.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
            // iOS 8+
            datePickerVC.modalPresentationStyle = UIModalPresentationStyle.popover
            datePickerVC.preferredContentSize = CGSize(width: 500, height: 320)// CGSizeMake(500,320)
            datePickerVC.currentDate = initDate
            popover = datePickerVC.popoverPresentationController
            if let _popover = popover {
                
                _popover.sourceView = textField
                _popover.sourceRect = CGRect(x: self.offset, y: textField.bounds.size.height, width: 0, height: 0)//CGRectMake(self.offset,textField.bounds.size.height,0,0)
                _popover.delegate = self
                self.dataChanged = dataChanged
                
                inViewController.present(datePickerVC, animated: true, completion: nil)
                presented = true
            }
        }
            
        else
        {
            datePickerVC.modalTransitionStyle = UIModalTransitionStyle.coverVertical
            datePickerVC.modalPresentationStyle = UIModalPresentationStyle.formSheet
            datePickerVC.currentDate = initDate
            datePickerVC.vistaContenedor.layer.cornerRadius = 5
            datePickerVC.vistaContenedor.layer.shadowOpacity = 0.8
            datePickerVC.vistaContenedor.layer.shadowOffset = CGSize(width: 0.0, height: 0.0) //CGSizeMake(0.0, 0.0)
            self.dataChanged = dataChanged
            inViewController.present(datePickerVC, animated: true, completion: nil)
            presented = true
        }
        
        
    }
    
    public func adaptivePresentationStyleForPresentationController(PC: UIPresentationController) -> UIModalPresentationStyle {
        
        return .none
    }
    
    func datePickerVCDismissed(date : NSDate?) {
        
        if let _dataChanged = dataChanged {
            
            if let _date = date {
                
                _dataChanged(_date, textField)
                
            }
        }
        presented = false
    }
}
