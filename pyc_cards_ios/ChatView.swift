//
//  topCircle.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 15/07/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit

class ChatView: UIView {
    

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    var imageView = UIImageView();
    
    var lastLocation:CGPoint = CGPoint(x: 0, y: 0)
    var view = UIViewController()
    var chat = ""
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        // Initialization code
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.layer.bounds.width/2
        let panRecognizer = UIPanGestureRecognizer(target:self, action:#selector(ChatView.detectPan(_:)))
        self.gestureRecognizers = [panRecognizer]
       // self.backgroundColor = CAGradientLayer().UIColorFromHex(rgbValue: 0x12b8f3,alpha: 0.6)
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ChatView.tapChat(_:))))
        
        //Adicionando imagen
        imageView = UIImageView(frame: CGRect(x: (self.layer.bounds.width/2)-12, y: (self.layer.bounds.height/2)-10, width: 40, height: 40));
        let image = UIImage(named: "chat.png");
        imageView.image = image;
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.addSubview(imageView);
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func detectPan(_ recognizer:UIPanGestureRecognizer) {
        let translation  = recognizer.translation(in: self.superview!)
        self.center = CGPoint(x: lastLocation.x + translation.x, y: lastLocation.y + translation.y)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // Promote the touched view
        
        self.superview?.bringSubview(toFront: self)
        // Remember original location
        lastLocation = self.center
    }
    
    func tapChat(_ gestureRecognizer: UITapGestureRecognizer){
        print("TAPS \n")
//        ZDCChat.initialize(withAccountKey: "4BBXUIlJlBXdcKmIUNpcz3qgpvUJN8H0")
//        ZDCChat.start{options in
//            options?.preChatDataRequirements.name = .required
//            options?.preChatDataRequirements.email = .required
//            options?.preChatDataRequirements.phone = .required
//            options?.preChatDataRequirements.department = .required
//            options?.preChatDataRequirements.message = .required
//        }
        
        print("view actual: \(self.view.description)")
         self.view.performSegue(withIdentifier: chat, sender: nil)
        
       
        
    }
    
    func changeColor(){
       self.backgroundColor = CAGradientLayer().UIColorFromHex(rgbValue: 0xffffff,alpha: 0.6)        
        self.tintColor = UIColor.init(cgColor: CAGradientLayer().UIColorFromHex(rgbValue: 0x7ed8d2, alpha: 1.0).cgColor)
        
    }
}
