//
//  LaunchScreenController.swift
//  pyc_cards_ios
//
//  Created by deisy melo on 27/01/16.
//  Copyright © 2016 PinBit. All rights reserved.
//

import Foundation

class LaunchScreenController: UIViewController {
    
    
    override func viewDidLoad() {//antes de cargar el view
        super.viewDidLoad()
        
        let imageBackground = SetBackground(view)
        view.addSubview(imageBackground)
        self.view.sendSubview(toBack: imageBackground)

        
       print("LaunchScreen")
        
    }
    
}

