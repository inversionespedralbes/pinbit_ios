//
//  DialogoSimpleController.swift
//  PYC_mobile
//
//  Created by Sebastian Vivas on 4/05/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit
import Alamofire

class DialogoCambioPassController: UIViewController, UITextFieldDelegate {

    //Variables de interfaz
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var vistaDialogo: UIView!
    @IBOutlet weak var txtActual: UITextField!
    @IBOutlet weak var txtNueva: UITextField!
    @IBOutlet weak var txtConfirmar: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var btnCancelar: UIButton!
    @IBOutlet weak var btnCambiar: UIButton!
    
    required override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.spinner.isHidden = true
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.vistaDialogo.layer.cornerRadius = 5
        self.vistaDialogo.layer.shadowOpacity = 0.8
        self.vistaDialogo.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        txtActual.delegate = self
        txtNueva.delegate = self
        txtConfirmar.delegate = self
        self.fillTexts()
    }
    
    func showInView(aView: UIView!, animated: Bool)
    {
        aView.addSubview(self.view)
        self.view.frame = aView.frame
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate( withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate( withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    //Acción para cambiar contraseña
    @IBAction func btnCambiar(_ sender: AnyObject) {
        if(txtActual.text!.characters.count < 4)
        {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: txtActual.center.x - 5, y: txtActual.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: txtActual.center.x + 5, y: txtActual.center.y))
            txtActual.layer.add(animation, forKey: "position")
        }
        
        else if(txtNueva.text!.characters.count < 4)
        {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: txtNueva.center.x - 5, y: txtNueva.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: txtNueva.center.x + 5, y: txtNueva.center.y))
            txtNueva.layer.add(animation, forKey: "position")
        }
        
        else if(txtNueva.text != txtConfirmar.text)
        {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: txtNueva.center.x - 5, y: txtNueva.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: txtNueva.center.x + 5, y: txtNueva.center.y))
            txtNueva.layer.add(animation, forKey: "position")
            animation.fromValue = NSValue(cgPoint: CGPoint(x: txtConfirmar.center.x - 5, y: txtConfirmar.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: txtConfirmar.center.x + 5, y: txtConfirmar.center.y))
            txtConfirmar.layer.add(animation, forKey: "position")
        }
        
        else
        {
            spinner.isHidden = false
            spinner.startAnimating()
            if(InternetConnection.isConnectedToNetwork()){//verifica si el dispositivo tiene conexion a internet
                
                self.cambioPass()
                
            }
            else{
                spinner.isHidden = true
                spinner.stopAnimating()
                self.view.makeToast(NSLocalizedString("no_internet", comment: ""))
            }
            
        }
    }
    
    //Acción para cancelar
    @IBAction func btnCancelar(_ sender: AnyObject) {
        self.removeAnimate()
    }
    
    
    //Patch para cambiar pass
    func cambioPass(){
        print("Cambiar password...: \n")
        let token = UserDefaults.standard.object(forKey: "token") as! String
       
        let url = urlPinbit + "/actualizar_contrasena.json"
        print("\(url)\n")
        let parameters = [
            "usuario": [
                "actual_password":self.txtActual.text!,
                "password":self.txtNueva.text!,
                "password_confirmation":self.txtConfirmar.text! ],
            "authenticity_token":token] as [String : Any]
        
        request(url,method: .patch, parameters: parameters as? [String : AnyObject])
            .responseJSON { response in
                
                print("respuesta: \(response.response?.statusCode) \n \(response.description)\n")
                switch response.result {
                case .success(_):
                    if(response.response?.statusCode == 200)
                    {
                        self.spinner.stopAnimating()
                        print("Cambié pass")
                        self.removeAnimate()
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cambioPassword"), object: nil)
                    }
                    else
                    {
                        self.removeAnimate()
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "errorPassword"), object: nil)
                        print("Error:  no fue posible cambiar la contraseña...")
                        
                    }
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func fillTexts(){
        self.lblTitulo.text = NSLocalizedString("cambiar_contrasena", comment: "")
        txtActual.placeholder = NSLocalizedString("contrasena_actual", comment: "")
        txtConfirmar.placeholder = NSLocalizedString("confirma_pass", comment: "")
        txtNueva.placeholder = NSLocalizedString("nueva_contrasena", comment: "")
        btnCancelar.setTitle(NSLocalizedString("cancelar", comment: ""), for: UIControlState())
        btnCambiar.setTitle(NSLocalizedString("guardar", comment: ""), for: UIControlState())
        btnCambiar.layer.masksToBounds = true
        btnCancelar.layer.masksToBounds = true
        btnCambiar.layer.cornerRadius = 3
        btnCancelar.layer.cornerRadius = 3
        
    }
    
    
    
    
}
