//
//  ScanQRPerfil.swift
//  pyc_cards_ios
//
//  Created by deisy melo on 14/12/16.
//  Copyright © 2016 PinBit. All rights reserved.
//

import UIKit
import Alamofire
import CoreData


class ScanQRPerfilController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    
    var dialogoPagar: DialogoConsultarPinController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicializarLector()
    }
    
    
    func inicializarLector(){
        
        print("Inicia scan QR \n")
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
        // as the media type parameter.
        
        var captureDevice:AVCaptureDevice! = nil
        captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        // Get an instance of the AVCaptureDeviceInput class using the previous device object.
        
        
        
        //        let error:NSError? = nil
        //
        //        let input: AVCaptureDeviceInput?
        //
        //        do {
        //            input = try AVCaptureDeviceInput(device: captureDevice)
        //        } catch {
        //            return
        //        }
        //
        //
        //
        //        if (error != nil) {
        //            // If any error occurs, simply log the description of it and don't continue any more.
        //            print("\(error?.localizedDescription)")
        //            return
        //        }
        
        do{
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            // Set the input device on the capture session.
            captureSession?.addInput(input as AVCaptureInput)
            
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            view.layer.addSublayer(videoPreviewLayer!)
            
            // Start video capture.
            captureSession?.startRunning()
            print("abre camara\n")
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            qrCodeFrameView?.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView?.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView!)
            view.bringSubview(toFront: qrCodeFrameView!)
            print("Scan QR View\n")
            //agrega el boton de cancelar a la camara...
            let btnCancel = UIButton(frame: CGRect(x: 0,y: 0,width: 100,height: 100))
            btnCancel.setTitle("Cancel", for: .normal)
            btnCancel.addTarget(self, action: #selector(ScanQRPerfilController.cerrarCamara(sender:)), for:.touchUpInside)
            self.view.addSubview(btnCancel)
            
            
            //        btnCambioTarjeta.addTarget(self, action: "pushCambiar:", forControlEvents:.TouchUpInside)
            //        self.view.addSubview(btnCambioTarjeta)
            
        }
        catch {
            print(error)
            captureSession?.stopRunning()//detiene el scan del QR
            self.dismiss(animated: true, completion: nil)
            return
        }
    }
    
    //Decifra el codigo QR capturado...
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect(x: 0, y: 0, width: 0, height: 0)//CGRectZero
            print("No QR code is detected")
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObjectTypeQRCode {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
            qrCodeFrameView?.frame = barCodeObject.bounds;
            
            if metadataObj.stringValue != nil {
                print("CODIGO QR?: \(metadataObj.stringValue)")
                registrarCuentaPinBit(mensaje: metadataObj.stringValue)
                captureSession?.stopRunning()//detiene el scan del QR
            }
            
        }
        
    }
    
    
    func registrarCuentaPinBit(mensaje: String)
    {
        //funciones para registrar tarjeta PinBit
        
        let url = urlPinbit + "/link-wallet.json"
        let token = UserDefaults.standard.object(forKey: "token") as! String

        
        let parameters = [
            "token": mensaje,
            "authenticity_token":token] as [String : Any]
        
        print("url: \(url)\n")
        print("parametros: \(parameters)\n")
        request(url,method: .post, parameters: parameters).responseJSON {
            response in
            
            print("RESPUESTA DE PINBIT: \(response.response!.statusCode), \(response.description) \n")
            switch response.result {
            case .success(let data):
                if(response.response!.statusCode == 201)
                {
                    print("datos recibidos: \(data) \n")
                    let json = JSON(data)
                    self.almacenarTarjetaBD(json["card"]["id"].stringValue, mascara: json["card"]["masked_number"].stringValue, alias: json["card"]["alias"].stringValue)
                    
                }
                else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "errorCuentaPinbit"), object: nil)
                    self.dismiss(animated: true, completion: nil)

                }
                
            case .failure(let error):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "errorCuentaPinbit"), object: nil)
                self.dismiss(animated: true, completion: nil)

            }
        }
        
        
        
    }
        
    func almacenarTarjetaBD(_ id:String, mascara: String, alias:String){
            // llama el objeto AppDelegate para poder utilizar las funciones que allí se encuentran
            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            
            //se obtiene el contexto: Es la interfaz para acceder a la base de datos...
            let context = appDel.managedObjectContext
            
            //se crea una nueva tarjeta
            let tarjetaNueva: AnyObject = NSEntityDescription.insertNewObject(forEntityName: "Tarjeta", into: context)
            //Insertar Datos
            tarjetaNueva.setValue(id, forKey: "id")
            tarjetaNueva.setValue(mascara, forKey: "mascara")
            tarjetaNueva.setValue(alias, forKey: "alias")
            tarjetaNueva.setValue("PinBit", forKey: "tipo")
            //guardar arjeta en la bd
            try! context.save()
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CuentaPinbitok"), object: nil)
        self.dismiss(animated: true, completion: nil)

        
        }

    
   
    
    func cerrarCamara(sender: UIButton!){
        print("Cerrar Camara")
        self.dismiss(animated: true, completion: { () -> Void in
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

