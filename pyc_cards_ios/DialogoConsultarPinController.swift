//
//  DialogoRegistroController.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 5/06/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class DialogoConsultarPinController: UIViewController {
    
    //Interfaz
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var viewDialogo: UIView!
    @IBOutlet weak var btnAceptar: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var btnCancelar: UIButton!
  
    
       @IBOutlet weak var btnCheck: UIButton!
    
    
    
    //Mapas
    var didFindMyLocation = false
    
    //Variables
    var latitud: Double!
    var longitud: Double!
    var flag: Bool!
    var vistaPapa: UIView!
    var controllerPapa: UIViewController!
    var desdeQR: Bool!
    var pin: String!
    var check = false
    var acepta = false
    
    var linkPago = "/pagos.json"
    
    //variables necesarias para la utilización de CoreData
    var appDel:AppDelegate!
    var context:NSManagedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Consultar pin \n")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.pagarConPuntos(notification:)), name: NSNotification.Name(rawValue: "pagarConPuntos"), object: nil)
        
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.pagarConPuntos(_:)), name: NSNotification.Name(rawValue: "pagarConPuntos"), object: nil)
        
        // llama el objeto AppDelegate para poder utilizar las funciones que allí se encuentran
        appDel = UIApplication.shared.delegate as! AppDelegate
        //se obtiene el contexto: Es la interfaz para acceder a la base de datos...
        context = appDel.managedObjectContext
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.viewDialogo.layer.cornerRadius = 5
        self.viewDialogo.layer.shadowOpacity = 0.8
        self.viewDialogo.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        flag = false
        //self.desdeQR = false
       
        self.btnCheck.isHidden = true
        self.btnAceptar.isHidden = true
        self.btnCancelar.isHidden = true
        //self.lblCheck.isHidden = true
        self.initPago()
        self.latitud = UserDefaults.standard.object(forKey: "latitud") as! Double!
        self.longitud = UserDefaults.standard.object(forKey: "longitud") as! Double!
        
        print("longitud: \(self.longitud!)\n")

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    required override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //Se encarga de mostrar el dialogo
    func showInView(aView: UIView!, codigo: String!, animated: Bool, controller: UIViewController)
    {
        self.vistaPapa = aView
        self.controllerPapa = controller
        aView.addSubview(self.view)
        self.pin = codigo
        self.desdeQR = false
        UserDefaults.standard.set("false", forKey: "desdeQR")
        print("Pin: \(pin!) \n")
        self.view.frame = aView.frame
        if animated
        {
            self.showAnimate()
            self.getComprobarPin()
        }
    }
    
    //Se encarga de mostrar el dialogo para lectura de código QR
    func showInViewQR(aView: UIView!, codigo: String, animated: Bool, controller: UIViewController)
    {
        self.vistaPapa = aView
       self.controllerPapa = controller
        aView.addSubview(self.view)
        self.pin = codigo
        print("Pin QR: \(pin!) \n")
        self.desdeQR = true
         UserDefaults.standard.set("true", forKey: "desdeQR")
        self.view.frame = aView.frame
        if animated
        {
            self.showAnimate()
            self.getComprobarPin()
        }
    }
    
    //Animación del dialogo
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    //Animación de desaparición del dialogo
    func removeAnimate()
    {
        UIView.animate( withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    //Detecta los toques en la pantalla
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //Inicializar pago
    func initPago(){
        //Primera vez que se muestra dialogo
        self.spinner.startAnimating()
        btnCancelar.setTitle(NSLocalizedString("cancelar",  comment: ""), for: UIControlState())
        self.lblTitulo.text = NSLocalizedString("comprobar_pin", comment: "")
        self.viewDialogo.isUserInteractionEnabled = false
    }
    
    //Muestra el dialogo para pagar un pin
    @IBAction func btnPagar(_ sender: AnyObject) {
        self.spinner.startAnimating()
        //Dialogo para pagar
        //postPagarPin()
    }
    
    //Función de cancelar el dialogo
    @IBAction func btnCancelar(_ sender: AnyObject) {
        if(self.desdeQR == true)
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificacionCierre"), object: self)
            //NotificationCenter.default.post(name: Notification.Name(rawValue: "notificacionCierre"), object: self)
        }
        self.removeAnimate()
    }
    
    //Función que comprueba y retorna la información de un pin
    func getComprobarPin( ) {
        
        let url = urlPinbit + "/pins/\(self.pin!).json"
        
        print("comprobar Pin")
        print("url: \(url)\n")
        request(url).responseJSON {
            response -> Void in
            
            print("Respuesta del servidor Comprobar Pin: \(response.response!.statusCode)")
            
            switch response.result {
            case .success(let data):
                if(response.response?.statusCode == 200)
                {
                    self.btnCheck.isHidden = false
                    self.spinner.stopAnimating()
                    self.spinner.isHidden = true
                    //self.btnAceptar.setTitle(NSLocalizedString("pagar", comment: ""), for: UIControlState())
                    self.btnAceptar.isEnabled = true
                    var json: JSON = JSON(data)
                    //var aux = NSLocalizedString("informacion_pago", comment: "")
                    //self.lblTitulo.text = "\(aux)" + "\n"
                    //aux = NSLocalizedString("establecimiento", comment: "")
                    //self.lblTitulo.text = self.lblTitulo.text! + aux + json["pin"]["establecimiento"].stringValue + "\n"
                    //aux = NSLocalizedString("valor", comment: "")
                    
                    //var val = json["pin"]["valor"].stringValue.replacingOccurrences(of: ".0", with: "")
                    
                    //self.lblTitulo.text = self.lblTitulo.text! + aux + "$" + val + "\n" + NSLocalizedString("coutas_pago", comment: "")
                    //self.flag = true
                    
                    let puntos = NSDecimalNumber(string: UserDefaults.standard.object(forKey: "points") as? String)
                    print("puntos puntos \(puntos)")
                    
                    let valor = NSDecimalNumber(string: json["pin"]["valor"].stringValue)
                    print(" valor: \(valor)")
                    
                    //self.lblCheck.text = NSLocalizedString("check_pago", comment: "")
                     var mensaje = NSLocalizedString("establecimiento", comment: "") + json["pin"]["establecimiento"].stringValue
                    
                    var enviarDatos = true
                    if(puntos >= valor){
                        print("\n se puede pagar con puntos....")
                        
                        let alertController = UIAlertController(title: NSLocalizedString("importante", comment: ""), message:
                            NSLocalizedString("txt_pago_puntos", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
                        
                        let OKAction = UIAlertAction(title: NSLocalizedString("aceptar", comment: ""), style: .default) { (action:UIAlertAction!) in
                            print("\n PAGAR CON PUNTOS.....\n")
                            self.linkPago = "/wallets/pay"
                        }
                        
                        alertController.addAction(OKAction)
                        alertController.addAction(UIAlertAction(title: NSLocalizedString("cancelar", comment: ""), style: UIAlertActionStyle.default,handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                    else{
                        if(UserDefaults.standard.bool(forKey: "sinTarjeta")){
                            enviarDatos  = false
                            print("no tiene puntos ni tarjetas..\n")

                            self.lblTitulo.text = NSLocalizedString("error_pago_sin_tarjeta", comment: "")
                            
                            self.btnCheck.isHidden = true
                           
                                                       self.viewDialogo.layoutIfNeeded()
                            self.spinner.stopAnimating()
                            self.spinner.isHidden = true
                            self.btnAceptar.isHidden = true
                            self.btnCancelar.isHidden = true
                            
                            let button   = UIButton(type: UIButtonType.system)
                            button.frame = CGRect(x: self.btnAceptar.bounds.width + 50,y: self.btnAceptar.bounds.height + 100, width: 100, height: 30)
                            button.setTitle(NSLocalizedString("aceptar", comment: ""), for: UIControlState())
                            //let image: UIImage = UIImage(named: "pattern1.png")!
                            //button.setBackgroundImage(image, forState: .Normal)
                            button.backgroundColor = UIColor(cgColor: CAGradientLayer().UIColorFromHex(rgbValue: 0x7ed8d2, alpha: 1.0).cgColor)
                            
                            button.layer.masksToBounds = true
                            button.layer.cornerRadius = 3.0
                            button.addTarget(self, action: #selector(DialogoConsultarPinController.errorPin(sender:)), for: UIControlEvents.touchUpInside)
                            self.viewDialogo.addSubview(button)

                        }
                    }
                    
                    //llamar al view PagarPinController
                   
                    if(enviarDatos){
                        UserDefaults.standard.set(mensaje, forKey: "mensaje_pin")
                        UserDefaults.standard.set(valor.description, forKey: "valor_pin")
                        UserDefaults.standard.set(self.linkPago, forKey: "link_pin")
                        UserDefaults.standard.set(self.pin, forKey: "codigo_pin")
                        
                        if(self.desdeQR == true){
                            self.controllerPapa!.performSegue(withIdentifier: "pagarPinQR", sender: nil)
                             
                        }else{
                            self.controllerPapa!.performSegue(withIdentifier: "PagarPinInfo", sender: nil)
                            
                        }
                                            
                        self.removeAnimate()
                        
                    }
                    
                }
                else
                {
                    self.lblTitulo.text = NSLocalizedString("pin_invalido", comment: "")
                    self.btnCheck.isHidden = true
                    print("Error comprobando pin 1 \(data)")
                    self.viewDialogo.layoutIfNeeded()
                    self.spinner.stopAnimating()
                    self.spinner.isHidden = true
                    self.btnAceptar.isHidden = true
                    self.btnCancelar.isHidden = true
                    
                    let button   = UIButton(type: UIButtonType.system)
                    button.frame = CGRect(x: self.btnAceptar.bounds.width + 50,y: self.btnAceptar.bounds.height + 100, width: 100, height: 30)
                    button.setTitle(NSLocalizedString("aceptar", comment: ""), for: UIControlState())
                    //let image: UIImage = UIImage(named: "pattern1.png")!
                    //button.setBackgroundImage(image, forState: .Normal)
                    button.backgroundColor = UIColor(cgColor: CAGradientLayer().UIColorFromHex(rgbValue: 0x7ed8d2, alpha: 1.0).cgColor)
                    
                    button.layer.masksToBounds = true
                    button.layer.cornerRadius = 3.0
                    button.addTarget(self, action: #selector(DialogoConsultarPinController.errorPin(sender:)), for: UIControlEvents.touchUpInside)
                    self.viewDialogo.addSubview(button)
                   

                }

            case .failure(let error):
                print("Error comprobando pin \(error)\n")
                
                self.viewDialogo.layoutIfNeeded()
                self.spinner.stopAnimating()
                self.spinner.isHidden = true
                self.btnAceptar.isHidden = true
                self.btnCancelar.isHidden = true
                
                let button   = UIButton(type: UIButtonType.system)
                button.frame = CGRect(x: self.btnAceptar.bounds.width + 50,y: self.btnAceptar.bounds.height + 100, width: 100, height: 30)
                button.setTitle(NSLocalizedString("aceptar", comment: ""), for: UIControlState())
                //let image: UIImage = UIImage(named: "pattern1.png")!
                //button.setBackgroundImage(image, forState: .Normal)
                button.backgroundColor = UIColor(cgColor: CAGradientLayer().UIColorFromHex(rgbValue: 0x7ed8d2, alpha: 1.0).cgColor)
                
                button.layer.masksToBounds = true
                button.layer.cornerRadius = 3.0
                button.addTarget(self, action: #selector(DialogoConsultarPinController.errorPin(sender:)), for: UIControlEvents.touchUpInside)
                self.viewDialogo.addSubview(button)
            }
            
           self.viewDialogo.isUserInteractionEnabled = true

        }
    }
    
    

    func errorPin (sender: UIButton!){
        self.removeAnimate()
        
        if(self.desdeQR == true)
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificacionCierre"), object: self)
        }

    }
  

    
    func traerDatosTarjetaActual(){
        
        let id_tarjeta = UserDefaults.standard.object(forKey: "ultimaTarjeta") as? String
        
        
        //traer los pagos de la bd...
        let consulta = NSFetchRequest<NSFetchRequestResult>(entityName: "tarjeta")
        
        let predicate = NSPredicate(format: "id == %@", id_tarjeta!)
        
        // Set the predicate on the fetch request
        consulta.predicate = predicate
        
        //recorrer el resultado de la consulta
        let res1 = try? self.context.fetch(consulta)
        var alias = ""
        var mascara = ""
        
        //recorriendo la consulta
        for info:AnyObject in res1 as! [NSManagedObject]  {
            alias = (info.value(forKey: "alias") as? String!)!
            mascara = (info.value(forKey:  "mascara") as? String!)!
        }
        
        print("mascara: \(mascara)")

    }
    
    //Formato para fechas
    func formatDate(fecha: String) -> String {
        let parte1 = fecha.substring(to: fecha.index((fecha.characters.indices.min())!, offsetBy: 10)) + " "
        //let parte2 = fecha[fecha.startIndex.advancedBy(11)...fecha.startIndex.advancedBy(18)]
        let parte2 = fecha[fecha.index(fecha.startIndex, offsetBy: 11)...fecha.index(fecha.startIndex, offsetBy: 18)]
        return parte1 + parte2
    }
    
    @objc func pagarConPuntos(notification: NSNotification){
        print("\n PAGAR CON PUNTOS.....\n")
        self.linkPago = "/wallets/pay"
    }
    
    //----FIN
}
