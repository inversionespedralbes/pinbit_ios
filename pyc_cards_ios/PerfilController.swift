//
//  PerfilController.swift
//  pyc_cards_ios
//
//  Created by Sebastian Vivas on 30/06/15.
//  Copyright (c) 2015 Sebastian Vivas. All rights reserved.
//

import UIKit
import Alamofire
import CoreData


class PerfilController: UIViewController, UITableViewDataSource, UITableViewDelegate, CardIOPaymentViewControllerDelegate {



    //Variables de interfaz
    @IBOutlet weak var tabla: UITableView!
    //@IBOutlet weak var lblMiCuenta: UILabel!
    @IBOutlet weak var btnCerrarSesion: UIButton!
    @IBOutlet weak var headerView: UIView!
    
   // @IBOutlet weak var btnPassword: UIButton!
    
    //@IBOutlet weak var btnReferidos: UIButton!
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var lblContrasena: UILabel!
    @IBOutlet weak var lblReferido: UILabel!
    
    @IBOutlet weak var btnAgregarCuenta: UIButton!
    @IBOutlet weak var lblpuntos: UILabel!
    @IBOutlet weak var txtPuntos: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    //Gestos swipe
    var swipeLeft: UISwipeGestureRecognizer!
    
    //Dialogos
    var dialogoTarjeta: DialogoAdicionarTarjetaController!
    var dialogoEliminar: DialogoEliminarTarjetaController!
    var dialogoPass: DialogoCambioPassController!
    var dialogoCambiarTarjeta: DialogoCambiarTarjetasController!
    var dialogoInfo: DialogoSimpleController!
    var dialogoCerrarSesion: DialogoCerrarSesionController!
    
    var dialogoLoader: DialogoCargaController!
    var dialogoTarjetas: DialogoTarjetasController!
    var dialogoRef : DialogoReferidoController!
    
    var tarjetas = [Tarjeta]()
    
    //Gestos swipe
    var swipeRight: UISwipeGestureRecognizer!
    
    var keyPinbit: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.iniTexts()
        tabla.alwaysBounceVertical = false;
        NotificationCenter.default.addObserver(self, selector: #selector(PerfilController.adicionarTarjeta(sender:)), name: NSNotification.Name(rawValue: "addTarjeta"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PerfilController.errorRegistroTarjeta(_:)), name:NSNotification.Name(rawValue: "errorRegistroTarjeta"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(PerfilController.mostrarMensaje(notification:)), name: NSNotification.Name(rawValue: "tarjetaRegistrada"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PerfilController.nuevaTarjeta(notification:)), name: NSNotification.Name(rawValue: "tarjetaRegistrada"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PerfilController.mostrarMensaje(notification:)), name:NSNotification.Name(rawValue: "tarjetaNoRegistrada"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PerfilController.eliminarTarjeta(notification:)), name: NSNotification.Name(rawValue: "eliminarTarjeta"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PerfilController.seleccionarTarjeta(notification:)), name: NSNotification.Name(rawValue: "seleccionarTarjeta"), object: nil)
       // NSNotificationCenter.defaultCenter().addObserver(self, selector: "cambiarPass:", name:"cambiarPass", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PerfilController.cerrarViewPerfil(notification:)), name:NSNotification.Name(rawValue: "cerrarPerfil"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PerfilController.dialogoTarjetas(notification:)), name:NSNotification.Name(rawValue: "verDialogTarjetas"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PerfilController.mostrarErrorPassword(_:)), name: NSNotification.Name(rawValue: "errorPassword"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PerfilController.mostrarCambioPassword(_:)), name:NSNotification.Name(rawValue: "cambioPassword"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(PerfilController.addRefOK(sender:)), name:NSNotification.Name(rawValue: "addRefOK"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(PerfilController.errorRegistroTarjeta(_:)), name: NSNotification.Name(rawValue: "errorCuentaPinbit"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(PerfilController.CuentaPinbitok(sender:)), name: NSNotification.Name(rawValue: "CuentaPinbitok"), object: nil)

        
        
        lblContrasena.text = NSLocalizedString("contrasena", comment: "")
        lblPassword.text = "•••••"
        lblReferido.text = NSLocalizedString("ref_perfil", comment: "")
        lblpuntos.text = NSLocalizedString("puntos", comment:  "")
        btnAgregarCuenta.setTitle(NSLocalizedString("asociar_cuenta", comment: ""), for: .normal)
        
        print("MOSTRAR PUNTOS PERFIL \n")
        
        self.getPoints()
        let puntos = UserDefaults.standard.object(forKey: "points") as? String
        print("puntos perfil: \(puntos)\n")
        if(puntos != nil){
            self.txtPuntos.text = puntos!
        }
        else{
            
        }
        
        
        let codigoRef = UserDefaults.standard.object(forKey: "codigoRef") as! String
        if(codigoRef != nil && !codigoRef.isEmpty){
            print("\n codigo referido: \(codigoRef)");
            dialogoInfo = DialogoSimpleController(nibName: "DialogoSimple", bundle: nil)
            dialogoInfo.showInView(aView: self.view, titulo: NSLocalizedString("titulo_codigo_ref", comment: ""), cuerpo: codigoRef, animated: true)
        }
        
    }
    
    //obtener puntos de referidos
    func getPoints(){
        print("OBTENER PUNTOS \n")
        let url = urlPinbit + "/wallets/check_points.json"
        print("\(url)\n")
        
        request(url).responseJSON {
            response in
            print("getPoints status code: \(response.response!.statusCode) \n")
            switch response.result {
            case .success(let data):
                if(response.response!.statusCode == 200)
                {
                    var json: JSON = JSON(data)
                    print("\n Puntos: \(json)")
                    UserDefaults.standard.setValue(json["points"].stringValue, forKey: "points")
                    self.txtPuntos.text = json["points"].stringValue
                }
                else
                {
                    UserDefaults.standard.setValue("0.0", forKey: "points")
                    self.txtPuntos.text = "0.0"
                }
            case .failure(let error):
                print(error)
                self.txtPuntos.text = "0.0"
            }
            
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        // CAGradientLayer().fondoDegrade(self.headerView, color1: CAGradientLayer().UIColorFromHex(0x071b6, alpha: 1), color2: CAGradientLayer().UIColorFromHex(0x090ef, alpha: 1))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath as IndexPath) as! TarjetasAdaptador
        cell2.layoutIfNeeded()
        
        return cell2
    }
    
    
    @objc func mostrarMensaje(notification: NSNotification){
        
        if(notification.name.rawValue == "tarjetaRegistrada"){
            print("tarjetaRegistrada")
        }
        else{
            print("tarjeta NO Registrada")
        }
    }
    
    @objc func eliminarTarjeta(notification: NSNotification){
        print("notificacion elminar tarjeta Perfil....\n")
        if(InternetConnection.isConnectedToNetwork()){//verifica si el dispositivo tiene conexion a internet
            let diccionario = notification.userInfo as! Dictionary<String,String?>
            dialogoEliminar = DialogoEliminarTarjetaController(nibName: "DialogoEliminarTarjeta", bundle: nil)
            dialogoEliminar.showInView(aView: self.view, animated: true, numero: diccionario["numero"]!, alias: diccionario["alias"]!, indice: diccionario["indice"]!, id: diccionario["id"]!)
        }
        else{
            self.view.makeToast(NSLocalizedString("no_internet", comment: ""))
        }
        
    }
    
    
    @objc func seleccionarTarjeta(notification: NSNotification){
        print("Notificacion seleccionarTarjeta\n")
        self.dialogoLoader = DialogoCargaController(nibName: "DialogoCarga", bundle: nil)
        self.dialogoLoader.showInView(aView: self.view, titulo: NSLocalizedString("procesando", comment: ""), animated: true)
        if(InternetConnection.isConnectedToNetwork()){//verifica si el dispositivo tiene conexion a internet
            let diccionario = notification.userInfo as! Dictionary<String,String?>
            postTarjeta(idTarjeta: diccionario["id"]!!, mascara: diccionario["numero"]!!)
        }
        else{
            self.view.makeToast(NSLocalizedString("no_internet", comment: ""))
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func postTarjeta(idTarjeta: String, mascara: String)
    {
        self.dialogoLoader.showAnimate()
        print("SELECCIONAR TARJETA PERFIL \n")
        let url = urlPinbit + "/seleccionar_tarjetas.json"
        print("\(url)\n")
        let token = UserDefaults.standard.object(forKey: "token") as! String
        let parameters = [
            "tarjeta": [
                "id":idTarjeta
            ], "authenticity_token":token ] as [String : Any]
        
        
        request(url, method: .post, parameters: parameters).responseJSON {
            response in
            print("Respuesta del servidor: \(response.response!.statusCode)\n")
            
            switch response.result {
            case .success:
                if(response.response!.statusCode == 200)
                {
                    print("tarjeta seleccionada perfil \n")
                    //se almacena la ultima tarjeta seleccionada
                    UserDefaults.standard.setValue(idTarjeta, forKey: "ultimaTarjeta")
                    
                    UserDefaults.standard.setValue(mascara, forKey: "numero_enmascarado")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cambieTarjeta"), object: nil)
                   //NotificationCenter.default.post(name: Notification.Name(rawValue: "cambieTarjeta"), object: nil)
                    self.dialogoLoader.removeAnimate()
                    //Cierra el view....
                    self.dismiss(animated: true, completion: nil)
                }
                else
                {
                    //Error de respuesta servidor
                    print("Error seleccion tarjeta \n")
                }
            case .failure(let error):
                print(error)
            }
            self.dialogoLoader.removeAnimate()
           
        }
    }
    
    
    @objc func dialogoTarjetas(notification: NSNotification){
        print("DIALOGO DE TARJETAS PERFIL...")
        
        let tarjetasDatos = UserDefaults.standard.object(forKey: "tarjetas") as? NSData
        
        if let tarjetasDatos = tarjetasDatos {
            let listaTarjetas = NSKeyedUnarchiver.unarchiveObject(with: tarjetasDatos as Data) as? [Tarjeta]
            
            if let listaTarjetas = listaTarjetas {
                if(listaTarjetas.count > 1)
                {
//                    dialogoCambiarTarjeta = DialogoCambiarTarjetasController(nibName: "DialogoCambiarTarjetasController", bundle: nil)
//                    dialogoCambiarTarjeta.showInView(self.view, animated: true, tarjetas: listaTarjetas)
                    
                    dialogoTarjetas = DialogoTarjetasController(nibName: "DialogoTarjetasController",bundle: nil)
                    dialogoTarjetas.showInView(aView: self.view, animated: true, tarjetas: listaTarjetas, dialogo: self.dialogoLoader)
                }
                else
                {
                    dialogoInfo = DialogoSimpleController(nibName: "DialogoSimple", bundle: nil)
                    dialogoInfo.showInView(aView: self.view, titulo: NSLocalizedString("cambiar_tarjeta", comment: ""), cuerpo: NSLocalizedString("cuerpo_dialogo_unica", comment: ""), animated: true)
                }
                
            }
            
        }
    
    }

    

    //recibe la notificación del cierre de sesión en el servidor y cierra la aplicación
    @objc func cerrarViewPerfil(notification: NSNotification){
        print("Intenta cerrar Perfil \n")
        self.dismiss(animated: true, completion: nil)
    }
    
    //recibe la notificación de la tarjeta creada...
    @objc func nuevaTarjeta(notification: NSNotification){
        print("Notificacion: nuevaTarjeta \n")
       //self.tabBarController!.selectedIndex = 0
        
        self.performSegue(withIdentifier: "PerfilPagos", sender: nil)
        //ºself.view.dismissViewControllerAnimated(true, completion: nil)
    }

    //Inicialización de textos
    func iniTexts(){
        //btnCerrarSesion.setTitle(NSLocalizedString("cerrar_sesion", comment: ""), forState: .Normal)
        //lblMiCuenta.text = (NSLocalizedString("tus_datos", comment: ""))
        lblTitulo.text = NSLocalizedString("perfil", comment: "")
    }
    
    /*
    ......
    ......
    
        REGISTRO DE TARJETAS
    
    ......
    ......
    */
    
    
    //Atiende el llamado del boton registrar tarjetas
    @objc func adicionarTarjeta(sender: AnyObject){
        
        print("Llama a Card.io")
        //llama a Card.io para realizar
        // la captura de la tarjeta
        //self.dialogoLoader.removeAnimate()
        let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
        cardIOVC?.modalPresentationStyle = .formSheet
        cardIOVC?.suppressScannedCardImage = true
        
        
        print(" Muestra CardIO \n")
        self.present(cardIOVC!, animated: true, completion: nil)
        
    }
    

    /// This method will be called when there is a successful scan (or manual entry). You MUST dismiss paymentViewController.
    /// @param cardInfo The results of the scan.
    /// @param paymentViewController The active CardIOPaymentViewController.
    public func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        if let info = cardInfo {
            var tipoTarjeta = info.cardType.hashValue.description
            print("tipoTarjeta: \(tipoTarjeta)")
            var typeCard = ""
            
            switch tipoTarjeta {
            case "3":
                typeCard = "AmEx"
                break
            case "4":
                typeCard = "VISA"
                break
            case "5":
                typeCard = "MasterCard"
            break
            case "6":
                typeCard = "DinersClub"
                break
            default:
                typeCard = ""
            }
            print("TIPO TARJETA:::::: \(typeCard)\n")
            paymentViewController?.dismiss(animated: true, completion: {
                //MUestra los datos capturados en un dialogo para continuar con el registro de la tarjeta
                self.dialogoTarjeta = DialogoAdicionarTarjetaController(nibName: "DialogoAdicionarTarjetaController", bundle: nil)
                self.dialogoTarjeta.showInView(self.view, numero: info.cardNumber, mes: String(info.expiryMonth), anio: String(info.expiryYear), cvv: String(info.cvv), tipo: typeCard, animated: true)
            })
            
        }
        
    }
    
    
    
    
    /// This method will be called if the user cancels the scan. You MUST dismiss paymentViewController.
    /// @param paymentViewController The active CardIOPaymentViewController.
    public func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        print("\n Se canceló el scan de la tarjeta... \n")
        paymentViewController?.dismiss(animated: true, completion: nil)
        let tarjeta = UserDefaults.standard.bool(forKey: "sinTarjeta")
        
    }
    

    
    @objc func errorRegistroTarjeta(_ sender: NSNotification){
        print("errorRegistroTarjeta")
        self.view.makeToast(NSLocalizedString("error_registro_tarjeta", comment: ""))
    }
    
    
    /*
    ......
    ......
    
    FIN REGISTRO TARJETAS
    
    ......
    ......
    */
  
    
    /*
    ......
    ......
    
    CAMBIO DE CONTRASEÑA....
    
    ......
    ......
    */
    
    @IBAction func cambiarPass(_ sender: AnyObject) {
        dialogoPass = DialogoCambioPassController(nibName: "DialogoCambioPass", bundle: nil)
        dialogoPass.showInView(aView: self.view, animated: true)
        
    }
    
    
    @IBAction func mostrarCambioPassword(_ sender: AnyObject) {
        print("Mostrando dialogo al cambiar contraseña...")
        self.view.makeToast(NSLocalizedString("cambio_password_correcto", comment: ""))
    }
    
    @IBAction func mostrarErrorPassword(_ sender: AnyObject) {
        print("Mostrando dialogo de error al cambiar contraseña...")
        self.view.makeToast(NSLocalizedString("error_cambio_contrasena", comment: ""))
    }
    /*
    ......
    ......
    
    FIN CAMBIO DE CONTRASEÑA
    
    ......
    ......
    */
    
    
    
    @IBAction func btnAtras(_ sender: AnyObject) {
        //cierra el view....
        let tarjeta = UserDefaults.standard.bool(forKey: "sinTarjeta")
        //UserDefaults.standard.bool(forKey: "sinTarjeta")
       
        if(!tarjeta)
        {
            print("Hay tarjetas")
        }
        else{
           // self.view.makeToast("No hay tarjetas")
            print("no hay tarjetas... \n")
        }
        
        self.performSegue(withIdentifier: "PerfilPagos", sender: nil)
        //self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func agregarReferido(_ sender: AnyObject) {
        print("\n boton agregar referidos")
        dialogoRef = DialogoReferidoController(nibName: "DialogoReferido", bundle: nil)
        dialogoRef.showInView(aView: self.view, animated: true)
    }
    
    @objc func addRefOK(sender: AnyObject){
        self.view.makeToast(NSLocalizedString("ref_ok", comment: ""))
    }
    
    @objc func errorCuentaPinbit(sender: AnyObject){
        self.view.makeToast(NSLocalizedString("error_registro", comment: ""))
        self.tabla.reloadData()
    }
    @objc func CuentaPinbitok(sender: AnyObject){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "nuevaTarjeta"), object: nil)
        self.view.makeToast(NSLocalizedString("cuenta_pinbit_ok", comment: ""))
        
    }
    
    
    @IBAction func agregarCuenta(_ sender: UIButton) {
        print("entra a asociar cuenta \n")
        let alertController = UIAlertController(title: NSLocalizedString("importante", comment: ""), message:
            NSLocalizedString("solo_store", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        
        let OKAction = UIAlertAction(title: NSLocalizedString("aceptar", comment: ""), style: .default) { (action:UIAlertAction!) in
            print("\n Mostrar QR para asociar cuenta\n")
            self.scanQR()
        }
        
        alertController.addAction(OKAction)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("cancelar", comment: ""), style: UIAlertActionStyle.default,handler: nil))
        self.present(alertController, animated: true, completion: nil)

    }
    
    func scanQR(){
       self.performSegue(withIdentifier: "scanQRPerfil", sender: nil)
    }

}
