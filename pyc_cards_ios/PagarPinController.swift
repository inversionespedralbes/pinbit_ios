//
//  PagarPinController.swift
//  pyc_cards_ios
//
//  Created by deisy melo on 9/12/16.
//  Copyright © 2016 PinBit. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class PagarPinController: UIViewController {
    
    @IBOutlet weak var tituloValor: UILabel!
    @IBOutlet weak var infoPin: UILabel!
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var lblValor: UILabel!
    @IBOutlet weak var tituloCuotas: UILabel!
    @IBOutlet weak var cuota: UILabel!
    @IBOutlet weak var cuotaMenos: UILabel!
    @IBOutlet weak var cuotaMAs: UILabel!
    
    @IBOutlet weak var btnCancelar: UIButton!
    
    
    
    var loader: DialogoCargaController!
    var numCuotas = 1
    var latitud: Double!
    var longitud: Double!
    var linkPago = ""
    
    var pin = ""
    var mensaje = ""
    var url = ""
    var total = ""
    //variables necesarias para la utilización de CoreData
    var appDel:AppDelegate!
    var context:NSManagedObjectContext!
    
    var desdeQR = ""
    
    var alias = ""
    var mascara = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // llama el objeto AppDelegate para poder utilizar las funciones que allí se encuentran
        appDel = UIApplication.shared.delegate as! AppDelegate
        //se obtiene el contexto: Es la interfaz para acceder a la base de datos...
        context = appDel.managedObjectContext
        
        print("PagarPinController \n")
        titulo.text = NSLocalizedString("informacion_pago", comment: "")
        tituloValor.text = NSLocalizedString("valor", comment: "")
        tituloCuotas.text = NSLocalizedString("cuotas_pago", comment: "")
        btnCancelar.setTitle(NSLocalizedString("cancelar", comment: ""), for: .normal)
        
        mensaje = (UserDefaults.standard.object(forKey: "mensaje_pin") as! String?)!
        total = (UserDefaults.standard.object(forKey: "valor_pin") as! String?)!
        self.linkPago =  UserDefaults.standard.object(forKey: "link_pin") as! String
        self.pin = UserDefaults.standard.object(forKey: "codigo_pin") as! String

        print("mensaje: \(mensaje)\n")
        print("valor: \(total)\n")
        print("pin : \(pin)\n")
        print("link pago : \(linkPago)\n")
        
        
        cuota.text = "1";
        cuotaMenos.text = "";
        cuotaMAs.text = "2";
        
        self.latitud = UserDefaults.standard.object(forKey: "latitud") as! Double!
        self.longitud = UserDefaults.standard.object(forKey: "longitud") as! Double!
        
        print("longitud: \(self.longitud!)\n")
        
        lblValor.text = total.description
        infoPin.text = mensaje
        desdeQR = UserDefaults.standard.object(forKey: "desdeQR") as! String!
        print("desdeQR?? \(desdeQR)")
       
        print("Esperando.........")
    }
    
    
    
    
    @IBAction func BtnMenos(_ sender: UIButton) {
        if(numCuotas > 1){
            var total = numCuotas - 1
            
            if(total == 1){cuotaMenos.text = ""}else{ cuotaMenos.text = String(total-1) }
            cuota.text = String(total)
            cuotaMAs.text = String(total+1)
            numCuotas = total
        }
        print("numCuotas: \(numCuotas)\n")
    }
    
   
    func BotonMas(_ sender: UIButton) {
        
        var total = numCuotas + 1
        cuotaMenos.text = String(total-1)
        cuota.text = String(total)
        cuotaMAs.text = String(total+1)
        numCuotas = total
        
        print("numCuotas: \(numCuotas)\n")
        
    }
    
    
    func PagarPin(_ sender: UIButton) {
        let alertController = UIAlertController(title: NSLocalizedString("titulo_check_pago", comment: ""), message:
            NSLocalizedString("check_pago", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        
        let OKAction = UIAlertAction(title: NSLocalizedString("aceptar", comment: ""), style: .default) { (action:UIAlertAction!) in
            print("\n Acepta.....\n")
            self.postPagarPin()
            //self.linkPago = "/wallets/pay"
        }
        
        alertController.addAction(OKAction)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("cancelar", comment: ""), style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    //Post para pagar un PIN
    func postPagarPin(){
       
            print("\n POST PAGAR PIN \n")
            loader = DialogoCargaController(nibName: "DialogoCarga", bundle: nil)
            loader.showInView(aView: self.view, titulo: NSLocalizedString("procesando", comment: ""), animated: true)
        
            let token = UserDefaults.standard.object(forKey: "token") as! String
            let deviceID = UserDefaults.standard.object(forKey: "device_id") as! String
            
            //traer los datos de la tarjeta desde la bd para verlos en la lista de pagos...
            let dato = NSFetchRequest<NSFetchRequestResult>(entityName: "Tarjeta")
            var tarjeta = UserDefaults.standard.value( forKey: "ultimaTarjeta") as! String
            
            //Se consulta especificamente el id de tarjeta que queremos
            dato.predicate = NSPredicate(format: "id = %@", tarjeta)
            //recorrer el resultado de la consulta....
            let consultaTarjea = try? context.fetch(dato)
            
        
            
            for tarj in consultaTarjea as! [NSManagedObject]  {
                alias = tarj.value(forKey: "alias") as! String
                mascara = tarj.value(forKey: "mascara") as! String
            }
            
            print("Prueba de postPagar: \(mascara)")
            
            let parameters = [
                "sistema_operativo": "iOS",
                "latitud":"\(self.latitud!)",
                "longitud":"\(self.longitud!)",
                "cantidad_cuotas": self.numCuotas,
                "authenticity_token": token,
                "pin":"\(self.pin)",
                "device_id":"\(deviceID)"] as [String : Any]
            
            let url = urlPinbit + self.linkPago
            
            print("\n \(url)")
            print("params: \(parameters)")
            
            request(url,method: .post, parameters: parameters).responseJSON {
                response in
                print("Respuesta del Pago: \(response.response!.statusCode)")
                
                switch response.result {
                case .success(let data):
                     print("Datos del pago: \n \(data)")
                    if(response.response!.statusCode == 201){
                        
                        let json = JSON(data)
                        print("JSON pagar pin: \(json)")
                        
                        if(self.linkPago.contains("/wallets/pay")){
                            let points = json["points"].stringValue
                            UserDefaults.standard.setValue(points, forKey: "points")
                        }
                        else{
                            self.savePay(json: json, tarjeta: tarjeta)
                        }
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pagoPin"), object: nil)
                        if(self.desdeQR == "true"){
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificacionCierre"), object: self)
                        }

                        self.dismiss(animated: true, completion: nil)
                    }
                    else{
                        //Error de respuesta servidor
                        if(response.response!.statusCode == 422){
                            print("Falló el pago....")
                            if(self.desdeQR == "true"){
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificacionCierre"), object: self)
                            }

                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "falloPago"), object: nil)
                            self.dismiss(animated: true, completion: nil)
                        }
                        else{
                            
                            if(self.desdeQR == "true"){
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificacionCierre"), object: self)
                                
                            }

                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "falloPago"), object: nil)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                case .failure(let error):
                    print("Faloo pago: \(error) \n")
                    if(self.desdeQR == "true"){
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificacionCierre"), object: self)
                        
                    }

                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "falloPago"), object: nil)
                    self.dismiss(animated: true, completion: nil)
                }
                
            }
            loader.dismiss(animated: true, completion: nil)
        }
    
  func savePay(json:JSON, tarjeta:String) {
        
        let movimientoNuevo: AnyObject = NSEntityDescription.insertNewObject(forEntityName: "Movimiento", into: self.context)
        
        var date = json["fecha"].stringValue
        
        movimientoNuevo.setValue(json["establecimiento"].stringValue, forKey: "establecimiento")
        movimientoNuevo.setValue(self.formatDate(fecha: date), forKey: "fecha_pago")
        //movimientoNuevo.setValue(formatDate(date), forKey: "fecha_pago")
        movimientoNuevo.setValue(json["latitud_cobro"].stringValue, forKey: "latitud_cobro")
        movimientoNuevo.setValue(json["longitud_cobro"].stringValue, forKey: "longitud_cobro")
        movimientoNuevo.setValue(json["latitud_pago"].stringValue, forKey: "latitud_pago")
        movimientoNuevo.setValue(json["longitud_pago"].stringValue, forKey: "longitud_pago")
        movimientoNuevo.setValue(json["valor"].stringValue, forKey: "valor")
        
        print("moviento_valor: \(json["valor"].stringValue)\n")
        //datos de la tarjeta
        
        movimientoNuevo.setValue(tarjeta, forKey: "id_tarjeta")
        movimientoNuevo.setValue(self.alias, forKey: "alias_tarjeta")
        movimientoNuevo.setValue(self.mascara, forKey: "mascara_tarjeta")
        //guardar tarjeta en la bd
        try! self.context.save()
    }
    
    func traerDatosTarjetaActual(){
        
        let id_tarjeta = UserDefaults.standard.object(forKey: "ultimaTarjeta") as? String
        
        
        //traer los pagos de la bd...
        let consulta = NSFetchRequest<NSFetchRequestResult>(entityName: "tarjeta")
        
        let predicate = NSPredicate(format: "id == %@", id_tarjeta!)
        
        // Set the predicate on the fetch request
        consulta.predicate = predicate
        
        //recorrer el resultado de la consulta
        let res1 = try? self.context.fetch(consulta)
        var alias = ""
        var mascara = ""
        
        //recorriendo la consulta
        for info:AnyObject in res1 as! [NSManagedObject]  {
            alias = (info.value(forKey: "alias") as? String!)!
            mascara = (info.value(forKey:  "mascara") as? String!)!
        }
        
        print("mascara: \(mascara)")
        
    }

    
    //Formato para fechas
    func formatDate(fecha: String) -> String {
        let parte1 = fecha.substring(to: fecha.index((fecha.characters.indices.min())!, offsetBy: 10)) + " "
        //let parte2 = fecha[fecha.startIndex.advancedBy(11)...fecha.startIndex.advancedBy(18)]
        let parte2 = fecha[fecha.index(fecha.startIndex, offsetBy: 11)...fecha.index(fecha.startIndex, offsetBy: 18)]
        return parte1 + parte2
        
    }
    
    
    @IBAction func Cancelar(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
